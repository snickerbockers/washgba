/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <string.h>

#include "SDL.h"
#include "SDL_opengl.h"

#include "gba.h"
#include "sched.h"
#include "gbafw.h"
#include "gbaram.h"
#include "gbavram.h"
#include "gbaoam.h"
#include "palram.h"
#include "gba_onchip_ram.h"
#include "gbaio.h"
#include "gbaintc.h"
#include "compiler_bullshit.h"
#include "hostfile.h"
#include "log.h"
#include "memory_map.h"
#include "arm7.h"
#include "thumb.h"
#include "atomics.h"
#include "emu.h"
#include "error.h"
#include "gamepack.h"
#include "gbavid.h"
#include "wash_glptr.h"
#include "gui.h"
#include "renderer.h"
#include "mat.h"

#ifdef HAVE_CAPSTONE
#include "capstone/capstone.h"
#endif

#ifdef ENABLE_DEBUGGER
#include "washdbg_tcp.h"
#endif

#ifdef IO_THREAD
#include "io_thread.h"
#endif

static jmp_buf error_point;

static washdc_atomic_int is_running = WASHDC_ATOMIC_INT_INIT(1);
static enum wash_state wash_state = WASH_STATE_NOT_RUNNING;
static unsigned n_frames;

WASH_NORETURN void wash_raise_error(enum error_type err_tp) {
    error_signal(err_tp);
    longjmp(error_point, err_tp);
}

void wash_kill(void) {
    LOG_INFO("%s called - washGBA will exit soon\n", __func__);
    int oldval = 1;
    washdc_atomic_int_compare_exchange(&is_running, &oldval, 0);
}

void wash_inject_irq(char const *id) {
    LOG_INFO("injecting IRQ %s\n", id);
    // TODO: this
    LOG_ERROR("FAILURE TO INJECT IRQ \"%s\"\n", id);
}

void wash_state_transition(enum wash_state state_new,
                           enum wash_state state_old) {
    if (state_old != wash_state)
        CRITICAL_ERROR(ERROR_INTEGRITY);
    wash_state = state_new;
}

bool washgba_is_running(void) {
    return washdc_atomic_int_load(&is_running);
}

static washdc_hostfile file_stdio_open(char const *path,
                                       enum washdc_hostfile_mode mode) {
    char modestr[4] = { 0 };
    int top = 0;
    if (mode & WASHDC_HOSTFILE_WRITE)
        modestr[top++] = 'w';
    else if (mode & WASHDC_HOSTFILE_READ)
        modestr[top++] = 'r';
    else
        return WASHDC_HOSTFILE_INVALID;

    if (mode & WASHDC_HOSTFILE_BINARY)
        modestr[top++] = 'b';
    if (mode & WASHDC_HOSTFILE_DONT_OVERWRITE)
        modestr[top++] = 'x';

    return fopen(path, modestr);
}

static void file_stdio_close(washdc_hostfile file) {
    fclose((FILE*)file);
}

static int file_stdio_seek(washdc_hostfile file, long disp,
                           enum washdc_hostfile_seek_origin origin) {
    int whence;
    switch (origin) {
    case WASHDC_HOSTFILE_SEEK_BEG:
        whence = SEEK_SET;
        break;
    case WASHDC_HOSTFILE_SEEK_CUR:
        whence = SEEK_CUR;
        break;
    case WASHDC_HOSTFILE_SEEK_END:
        whence = SEEK_END;
        break;
    default:
        return -1;
    }
    return fseek((FILE*)file, disp, whence);
}

static long file_stdio_tell(washdc_hostfile file) {
    return ftell((FILE*)file);
}

static size_t file_stdio_read(washdc_hostfile file, void *outp, size_t len) {
    return fread(outp, 1, len, (FILE*)file);
}

static size_t file_stdio_write(washdc_hostfile file, void const *inp, size_t len) {
    return fwrite(inp, 1, len, (FILE*)file);
}

static int file_stdio_flush(washdc_hostfile file) {
    return fflush((FILE*)file);
}

static struct washdc_hostfile_api hostfile_stdio = {
    .open = file_stdio_open,
    .close = file_stdio_close,
    .seek = file_stdio_seek,
    .tell = file_stdio_tell,
    .read = file_stdio_read,
    .write = file_stdio_write,
    .flush = file_stdio_flush
};

static int gba_init(struct gba *gba, char const *fw_path, char const *rompath) {

    washdc_log_info("==== initializing gba instance %p ====\n", gba);

    gba->inst_counter = 0;

    dc_clock_init(&gba->clock);

    washdc_log_info("loading firmware from \"%s\"\n", fw_path);

    FILE *fw_rom = fopen(fw_path, "rb");
    if (!fw_rom) {
        washdc_log_error("failure to open \"%s\"\n", fw_path);
        return -1;
    }

    gbafw_init_from_file(&gba->fw, fw_rom);
    fclose(fw_rom);

    washdc_hostfile rom = WASHDC_HOSTFILE_INVALID;
    if (rompath) {
        rom = washdc_hostfile_open(rompath,
                                   WASHDC_HOSTFILE_BINARY |
                                   WASHDC_HOSTFILE_READ);
        if (!rom) {
            washdc_log_error("ERROR - UNABLE TO OPEN %s\n", rompath);
            exit(1);
        } else {
            washdc_log_info("successfully loaded %s\n", rompath);
        }
    } else {
        washdc_log_info("booting firmware with no gamepack inserted.\n");
    }

    washdc_log_info("firmware initialized.\n");

    gbaram_init(&gba->ram);
    gba_onchip_ram_init(&gba->onchip_ram);
    gbavram_init(&gba->vram);
    gbaoam_init(&gba->oam);
    palram_init(&gba->pal);
    gbaio_init(&gba->io);
    gamepack_init(&gba->game, rom);

    if (rom != WASHDC_HOSTFILE_INVALID)
        washdc_hostfile_close(rom);

    memory_map_init(&gba->map);
    memory_map_add(&gba->map, 0, 0x3fff, ~0, MEMORY_MAP_REGION_UNKNOWN,
                   &gbafw_memory_interface, &gba->fw);
    memory_map_add(&gba->map, 0x02000000, 0x0203ffff, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gbaram_memory_interface,
                   &gba->ram);
    memory_map_add(&gba->map, GBAIO_FIRST, GBAIO_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gbaio_memory_interface,
                   gba);
    memory_map_add(&gba->map, 0x03000000, 0x03ffffff, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gba_onchip_ram_memory_interface,
                   &gba->onchip_ram);
    memory_map_add(&gba->map, GBAVRAM_FIRST, GBAVRAM_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gbavram_memory_interface,
                   &gba->vram);
    memory_map_add(&gba->map, GBAOAM_FIRST, GBAOAM_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gbaoam_memory_interface,
                   &gba->oam);
    memory_map_add(&gba->map, PALRAM_FIRST, PALRAM_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &palram_memory_interface,
                   &gba->pal);
    memory_map_add(&gba->map, GAMEPACK_WAITSTATE0_FIRST, GAMEPACK_WAITSTATE0_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gamepack_memory_interface,
                   &gba->game);
    memory_map_add(&gba->map, GAMEPACK_WAITSTATE1_FIRST, GAMEPACK_WAITSTATE1_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gamepack_memory_interface,
                   &gba->game);
    memory_map_add(&gba->map, GAMEPACK_WAITSTATE2_FIRST, GAMEPACK_WAITSTATE2_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gamepack_memory_interface,
                   &gba->game);
    memory_map_add(&gba->map, GAMEPACK_SRAM_FIRST, GAMEPACK_SRAM_LAST, ~0,
                   MEMORY_MAP_REGION_UNKNOWN, &gamepack_sram_memory_interface,
                   &gba->game);

    arm7_init(&gba->cpu, &gba->clock);
    arm7_set_mem_map(&gba->cpu, &gba->map);
    gbaintc_init(&gba->intc, &gba->cpu);
    gbadmac_init(&gba->dmac);
    gbavid_init(&gba->vid, &gba->intc, &gba->clock, gba);

    washdc_log_info("==== done initializing gba instance %p ====\n", gba);

    return 0;
}

static void gba_cleanup(struct gba *gba) {
    washdc_log_info("==== releasing resources for GBA instance %p ====\n", gba);
    gbavid_cleanup(&gba->vid);
    gbadmac_cleanup(&gba->dmac);
    gamepack_cleanup(&gba->game);
    gbaintc_cleanup(&gba->intc);
    arm7_cleanup(&gba->cpu);
    memory_map_cleanup(&gba->map);
    gbaio_cleanup(&gba->io);
    palram_cleanup(&gba->pal);
    gbaoam_cleanup(&gba->oam);
    gbavram_cleanup(&gba->vram);
    gba_onchip_ram_cleanup(&gba->onchip_ram);
    gbaram_cleanup(&gba->ram);
    gbafw_cleanup(&gba->fw);
    dc_clock_cleanup(&gba->clock);
    washdc_log_info("==== finished releasing resources for GBA instance %p ====\n", gba);
}

// print usage and exit with error.  THIS FUNCTION DOES NOT RETURN.  EVER.
WASH_NORETURN void printusage(char const *cmdstr) {
    fprintf(stderr, "usage: %s -f <path to firmware image> [-g washdbg]\n", cmdstr);
    exit(1);
}

static struct gba gameboy;
static struct gui gui;
static struct renderer renderer;

static bool run_to_next_arm7_event(void *ctxt) {
    struct gba *gba = ctxt;
    cycle_stamp tgt_stamp = clock_target_stamp(&gba->clock);

    if (clock_countdown(&gba->clock) == 0)
        return false;

    cycle_stamp cycles_after;
    for (;;) {
        cycle_stamp cycles_adv = arm7_exec_inst(&gba->cpu);
        gba->inst_counter++;

        if (cycles_adv >= clock_countdown(&gba->clock)) {
            cycles_after = clock_target_stamp(&gba->clock);
            break;
        }

        clock_countdown_sub(&gba->clock, cycles_adv);
    }
    clock_set_cycle_stamp(&gba->clock, cycles_after);

    return false;
}

#ifdef ENABLE_DEBUGGER
static bool check_debugger(void) {
    /*
     * If the debugger is enabled, make sure we have its permission to
     * single-step; if we don't then  block until something interresting
     * happens, and then skip the rest of the loop.
     */
    debug_notify_inst();
    bool is_running;

    if ((is_running = washgba_is_running()) &&
        wash_state == WASH_STATE_DEBUG) {
        printf("wash_state is WASH_STATE_DEBUG\n");
        do {
            // call debug_run_once 100 times per second
            /* win_check_events(); */ // TODO: this
            debug_run_once();
            /* washdc_sleep_ms(1000 / 100); */ // TODO: this
        } while (wash_state == WASH_STATE_DEBUG &&
                 (is_running = washgba_is_running()));
    }
    return !is_running;
}

static bool run_to_next_arm7_event_debugger(void *ctxt) {
    struct gba *gba = ctxt;
    cycle_stamp tgt_stamp = clock_target_stamp(&gba->clock);

    debug_set_context(DEBUG_CONTEXT_ARM7);
    cycle_stamp cycles_after;
    bool exit_now;

    if ((exit_now = check_debugger()))
        return exit_now;

    while (!(exit_now = check_debugger())) {
        int extra_cycles;
        arm7_inst inst = arm7_fetch_inst(&gba->cpu, &extra_cycles);
        arm7_op_fn handler;
        if (arm7_thumb_mode(&gba->cpu))
            handler = arm7_thumb_decode(&gba->cpu, &inst);
        else
            handler = arm7_decode(&gba->cpu, inst);

        // TODO: more accurate instruction cycle counting
        unsigned inst_cycles = 1;
        handler(&gba->cpu, inst);

        gba->inst_counter++;
        cycle_stamp cycles_adv =
            (inst_cycles + extra_cycles) * ARM7_CLOCK_SCALE;

        if (cycles_adv >= clock_countdown(&gba->clock)) {
            cycles_after = clock_target_stamp(&gba->clock);
            break;
        }

        clock_countdown_sub(&gba->clock, cycles_adv);
#ifdef ENABLE_DBG_COND
        debug_check_conditions(DEBUG_CONTEXT_ARM7);
#endif
    }
    if (!exit_now)
        clock_set_cycle_stamp(&gba->clock, cycles_after);

    return false;
}
#endif

static bool wash_strieq(char const *str1, char const *str2) {
    do {
        if (toupper(*str1++) != toupper(*str2++))
            return false;
    } while (*str1 || *str2);
    return true;
}

#ifdef HAVE_CAPSTONE
static void print_cs_insn(cs_insn const *insn, void (*log_fn)(char const*,...), char const *prologue, char const *epilogue) {
    unsigned n_bytes = insn->size;
    uint8_t const *next_byte = insn->bytes;

    switch (n_bytes) {
    case 0:
        log_fn("%s%8s %-16s; %s\n", prologue,
               insn->mnemonic, insn->op_str, epilogue);
        break;
    case 1:
        log_fn("%s%8s %-16s ; %02x - %s\n", prologue,
               insn->mnemonic, insn->op_str, (unsigned)*next_byte, epilogue);
        break;
    case 2:
        log_fn("%s%8s %-16s ; %02x%02x - %s\n", prologue,
               insn->mnemonic, insn->op_str,
               (unsigned)next_byte[1], (unsigned)next_byte[0], epilogue);
        break;
    case 3:
        log_fn("%s%8s %-16s ; %02x%02x%02x - %s\n", prologue,
               insn->mnemonic, insn->op_str,
               (unsigned)next_byte[2], (unsigned)next_byte[1],
               (unsigned)next_byte[0], epilogue);
        break;
    default:
        next_byte = insn->bytes + (n_bytes - 4);
        do {
            log_fn("%s%8s %-16s ; %02x%02x%02x%02x - %s\n", prologue,
                   insn->mnemonic, insn->op_str,
                   (unsigned)next_byte[1], (unsigned)next_byte[0],
                   (unsigned)next_byte[3], (unsigned)next_byte[2],
                   epilogue);
            n_bytes -= 4;
            next_byte -= 4;
        } while (n_bytes >= 4);
        switch (n_bytes) {
        case 1:
            log_fn("%s%8s %-16s ; %02x - %s\n", prologue,
                   insn->mnemonic, insn->op_str, (unsigned)*next_byte, epilogue);
            break;
        case 2:
            log_fn("%s%8s %-16s ; %02x%02x - %s\n", prologue,
                   insn->mnemonic, insn->op_str,
                   (unsigned)next_byte[1], (unsigned)next_byte[0], epilogue);
            break;
        case 3:
            log_fn("%s%8s %-16s ; %02x%02x%02x - %s\n", prologue,
                   insn->mnemonic, insn->op_str,
                   (unsigned)next_byte[2], (unsigned)next_byte[1],
                   (unsigned)next_byte[0], epilogue);
            break;
        }
    }
}
#endif

static size_t do_disasm_thumb(csh handle, uint16_t *inst_buf,
                              uint32_t *first_inst, uint32_t *last_inst,
                              cs_insn **insnpp) {
    uint32_t n_inst = (*last_inst - *first_inst) / 2 + 1;
    size_t count = cs_disasm(handle, inst_buf, n_inst * sizeof(uint16_t),
                             *first_inst, 0, insnpp);

    if (count >= 1 || *first_inst >= *last_inst)
        return count;

    count = cs_disasm(handle, inst_buf + 1, (n_inst - 1) * sizeof(uint16_t),
                      *first_inst + 2, 0, insnpp);
    if (count >= 1)
        *first_inst += 2;
    return count;
}

#define WIN_WIDTH 640
#define WIN_HEIGHT 480

static void redraw_win(SDL_Window *win) {
    SDL_GL_SwapWindow(win);
}

#define MAX_TILES 512

#define WINDOW_BORDER 8.0f
#define WIN_TITLE_HEIGHT 24.0f

// upper-left corner of tile-view window
static GLfloat tile_view_pos_win_ul[2] = { 0.0f, 0.0f };

static void draw_tile_view(void) {
#define TILE_VIEW_N_TILES_X 16
    uint16_t obj_pal[256];
    memcpy(obj_pal, gameboy.pal.mem + 256, sizeof(obj_pal));

    char *obj_tiles = malloc(0x8000);
    if (!obj_tiles) {
        LOG_ERROR("%s - failure to allocate object buffer\n", __func__);
        return;
    }

    memcpy(obj_tiles, gameboy.vram.vram + 0x10000 / 2, 0x8000);

    GLuint *tile_tex = malloc(sizeof(GLuint) * MAX_TILES);
    if (!tile_tex) {
        LOG_ERROR("%s - failure to allocate tile buffer\n", __func__);
        free(obj_tiles);
        return;
    }

    GLfloat const win_color[4] = {
        0x88 / 255.0f,
        0x88 / 255.0f,
        0x88 / 255.0f,
        1.0f };
    GLfloat tile_disp_sz[2] = {
        TILE_VIEW_N_TILES_X * 8.0f,
        8.0f * (MAX_TILES / TILE_VIEW_N_TILES_X)
    };
    GLfloat tile_disp_win_sz[2] = {
        tile_disp_sz[0] + WINDOW_BORDER * 2,
        tile_disp_sz[1] + WINDOW_BORDER * 2 + WIN_TITLE_HEIGHT
    };

    GLfloat border_pos[2] = {
        tile_view_pos_win_ul[0],
        tile_view_pos_win_ul[1]
    };

    gui_draw_untextured_quad(&gui, border_pos, tile_disp_win_sz, win_color);

    pglGenTextures(MAX_TILES, tile_tex);

    unsigned tilno;
    for (tilno = 0; tilno < MAX_TILES; tilno++) {
        char *tile = obj_tiles + 8 * 8 * tilno;

        unsigned char pix[8 * 8 * 3];
        for (unsigned pixno = 0; pixno < 64; pixno++) {
            uint16_t pix16 = obj_pal[tile[pixno]];

            unsigned red = (pix16 & 0x1f) << 3;
            unsigned green = ((pix16 >> 5) & 0x1f) << 3;
            unsigned blue = ((pix16 >> 10) & 0x1f) << 3;

            pix[pixno * 3] = red;
            pix[pixno * 3 + 1] = green;
            pix[pixno * 3 + 2] = blue;
        }

        pglBindTexture(GL_TEXTURE_2D, tile_tex[tilno]);
        pglTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 8, 8, 0, GL_RGB, GL_UNSIGNED_BYTE, pix);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }

    GLfloat tile_disp_left_edge = tile_view_pos_win_ul[0] + WINDOW_BORDER;
    GLfloat x_pos = tile_disp_left_edge;
    GLfloat y_pos = tile_view_pos_win_ul[1] + (WIN_TITLE_HEIGHT + WINDOW_BORDER);

    gui_draw_string(&gui, "tiles",
                    tile_view_pos_win_ul[0] + WINDOW_BORDER,
                    tile_view_pos_win_ul[1] + WINDOW_BORDER, WIN_TITLE_HEIGHT);

    for (tilno = 0; tilno < MAX_TILES; tilno++) {
        GLfloat pos[2] = { x_pos, y_pos };
        GLfloat sz[2] = { 8.0f, 8.0f };
        GLfloat uv_pos[2] = { 0.0f, 0.0f };
        GLfloat uv_sz[2] = { 1.0f, 1.0f };
        gui_draw_quad(&gui, tile_tex[tilno], pos, sz, uv_pos, uv_sz);
        x_pos += 8.0f;
        if ((1 + tilno) % TILE_VIEW_N_TILES_X == 0) {
            x_pos = tile_disp_left_edge;
            y_pos += 8.0f;
        }
    }
    pglDeleteTextures(MAX_TILES, tile_tex);

    gbaoam_parse(&gameboy.oam);

    free(tile_tex);
    free(obj_tiles);
}

static void render_frame(SDL_Window *win) {
    // notice that there is an intentional y-invert
    GLfloat proj_mat[16];
    GLfloat min[2] = { 0.0f, 160.0f };
    GLfloat max[2] = { 240.0f, 0.0f };
    mat_ortho2d(proj_mat, min, max);

    pglClearColor(1.0, 1.0, 1.0, 1.0);
    pglClear(GL_COLOR_BUFFER_BIT);

    renderer_draw_frame(&renderer, &gameboy, proj_mat);

    // disable the tile view for now
    /* draw_tile_view(); */

    redraw_win(win);
}

int main(int argc, char **argv) {
    char const *fwpath = NULL;
    char const *debugger = NULL;
    char const *cmdstr = argv[0];
    bool en_washdbg = false;
    bool log_stdout = false;
    bool log_verbose = false;
    int argidx;

    for (argidx = 1; argidx < argc; argidx++) {
        int subidx = 0;
        if (argv[argidx][0] == '-') {
            bool done_with_this_arg = false;
            while (argv[argidx][++subidx] && !done_with_this_arg) {
                switch (argv[argidx][subidx]) {
                case 'f':
                    if (fwpath || argv[argidx][subidx + 1] || argidx + 1 >= argc)
                        printusage(cmdstr);
                    fwpath = argv[++argidx];
                    done_with_this_arg = true;
                    break;
                case 'g':
                    if (debugger || argv[argidx][subidx + 1] || argidx + 1 >= argc)
                        printusage(cmdstr);
                    debugger = argv[++argidx];
                    if (wash_strieq(debugger, "washdbg"))
                        en_washdbg = true;
                    else
                        printusage(cmdstr);
                    done_with_this_arg = true;
                    break;
                case 'l':
                    log_stdout = true;
                    break;
                case 'v':
                    log_verbose = true;
                    break;
                }
            }
        } else {
            break;
        }
    }

    if (!fwpath)
        printusage(cmdstr);

    argc -= argidx;
    argv += argidx;

    char const *rompath = NULL;

    if (argc > 0)
        rompath = argv[0];

    SDL_Init(SDL_INIT_VIDEO);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_Window *win = SDL_CreateWindow("washGBA",
                                       SDL_WINDOWPOS_UNDEFINED,
                                       SDL_WINDOWPOS_UNDEFINED,
                                       WIN_WIDTH, WIN_HEIGHT,
                                       SDL_WINDOW_OPENGL);
    SDL_GLContext ctxt = SDL_GL_CreateContext(win);

    washdc_hostfile_init(&hostfile_stdio);
    log_init(log_stdout, log_verbose);

    washdc_log_info("Welcome to washGBA!\n");

#ifdef IO_THREAD
    io_init();
#endif

    if (gba_init(&gameboy, fwpath, rompath) != 0) {
        washdc_log_error("unable to initialize virtual GBA.\n");
        exit(1);
    }

    int is_running_val = 0;
    washdc_atomic_int_compare_exchange(&is_running, &is_running_val, 1);

    wash_state_transition(WASH_STATE_RUNNING, WASH_STATE_NOT_RUNNING);

#ifdef ENABLE_DEBUGGER
    if (en_washdbg) {
        debug_init();
        debug_init_context(DEBUG_CONTEXT_ARM7, &gameboy.cpu, &gameboy.map);
    }
#endif

#ifdef ENABLE_DEBUGGER
    if (en_washdbg) {
        debug_attach(&washdbg_frontend);
        gameboy.clock.dispatch = run_to_next_arm7_event_debugger;
    } else {
#endif
        gameboy.clock.dispatch = run_to_next_arm7_event;
#ifdef ENABLE_DEBUGGER
    }
#endif
    gameboy.clock.dispatch_ctxt = &gameboy;

    washdc_log_info("initialization complete.  have fun!\n");

    wash_load_gl();

    gui_init(&gui, WIN_WIDTH, WIN_HEIGHT);
    renderer_init(&renderer);

    pglClear(GL_COLOR_BUFFER_BIT);
    redraw_win(win);

    int err_tp;
    if ((err_tp = setjmp(error_point)) == 0) {
        // main loop
        while (washgba_is_running()) {
            SDL_Event evt;
            gba_btn_state this_frame_btns = 0;
            while (SDL_PollEvent(&evt)) {
                switch (evt.type) {
                case SDL_QUIT:
                    washdc_log_info("exiting due to SDL_QUIT event\n");
                    wash_kill();
                    break;
                case SDL_KEYDOWN:
                    switch (evt.key.keysym.scancode) {
                    case SDL_SCANCODE_W:
                        this_frame_btns |= GBA_BTN_UP;
                        break;
                    case SDL_SCANCODE_S:
                        this_frame_btns |= GBA_BTN_DOWN;
                        break;
                    case SDL_SCANCODE_A:
                        this_frame_btns |= GBA_BTN_LEFT;
                        break;
                    case SDL_SCANCODE_D:
                        this_frame_btns |= GBA_BTN_RIGHT;
                        break;
                    case SDL_SCANCODE_KP_4:
                        this_frame_btns |= GBA_BTN_B;
                        break;
                    case SDL_SCANCODE_KP_5:
                        this_frame_btns |= GBA_BTN_A;
                        break;
                    case SDL_SCANCODE_KP_7:
                        this_frame_btns |= GBA_BTN_L;
                        break;
                    case SDL_SCANCODE_KP_8:
                        this_frame_btns |= GBA_BTN_R;
                        break;
                    case SDL_SCANCODE_Z:
                        this_frame_btns |= GBA_BTN_START;
                        break;
                    case SDL_SCANCODE_X:
                        this_frame_btns |= GBA_BTN_SELECT;
                        break;
                    }
                }
            }

            gameboy.btns = this_frame_btns;
            dc_clock_run_frame(&gameboy.clock);
            n_frames++;
            render_frame(win);
        }
    } else {
        washdc_log_error("====================================================================\n");
        washdc_log_error("==\n");
        washdc_log_error("== EMULATION ERROR REPORT\n");
        washdc_log_error("==\n");
        washdc_log_error("====================================================================\n");
        washdc_log_error("emulation error handled with grace and repose\n");
        error_print();
        error_clear();

        washdc_log_error("====================================================================\n");
        washdc_log_error("ARM CPU state:\n");

        struct arm7 *cpu = &gameboy.cpu;
        char const *curmode;
        switch (cpu->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK) {
        case ARM7_MODE_USER:
            curmode = "user";
            break;
        case ARM7_MODE_FIQ:
            curmode = "fiq";
            break;
        case ARM7_MODE_IRQ:
            curmode = "irq";
            break;
        case ARM7_MODE_SVC:
            curmode = "svc";
            break;
        case ARM7_MODE_ABT:
            curmode = "abt";
            break;
        case ARM7_MODE_UND:
            curmode = "und";
            break;
        case ARM7_MODE_SYS:
            curmode = "sys";
            break;
        default:
            curmode = "ERROR/INVALID";
        }
        washdc_log_error("\tcurrent CPU mode: %s\n", curmode);
        washdc_log_error("\tcurrent CPU instruction set: %s\n",
                         arm7_thumb_mode(cpu) ? "thumb" : "ARM");

        washdc_log_error("\tR0:  %08x R1:  %08x R2:  %08x R3:  %08x\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 0, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 1, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 2, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 3, ARM7_MODE_USER));
        washdc_log_error("\tR4:  %08x R5:  %08x R6:  %08x R7:  %08x\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 4, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 5, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 6, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 7, ARM7_MODE_USER));
        washdc_log_error("\tR8:  %08x R9:  %08x R10: %08x R11: %08x\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 8, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 9, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 10, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 11, ARM7_MODE_USER));
        washdc_log_error("\tR12: %08x R13: %08x R14: %08x R15: %08x\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 12, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_USER),
                         (unsigned)*arm7_gen_reg_bank(cpu, 15, ARM7_MODE_USER));


        washdc_log_error("\tCPSR: %08x\n", (unsigned)cpu->reg[ARM7_REG_CPSR]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tR8_fiq:   %08X R9_fiq:  %08X\n"
                         "\tR10_fiq:  %08X R11_fiq: %08X\n"
                         "\tR12_fiq:  %08X R13_fiq: %08X\n"
                         "\tR14_fiq:  %08X\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 8, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 9, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 10, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 11, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 12, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_FIQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_FIQ));


        washdc_log_error("\tSPSR_fiq: %08x\n", (unsigned)cpu->reg[ARM7_REG_SPSR_FIQ]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tR8_irq:   %08x R9_irq:  %08x\n"
                         "\tR10_irq:  %08x R11_irq: %08x\n"
                         "\tR12_irq:  %08x R13_irq: %08x\n"
                         "\tR14_irq:  %08X\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 8, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 9, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 10, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 11, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 12, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_IRQ),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_IRQ));

        washdc_log_error("\tSPSR_irq: %08x\n", (unsigned)cpu->reg[ARM7_REG_SPSR_IRQ]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tR13_svc:  %08x R14_svc: %08X\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_SVC),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_SVC));

        washdc_log_error("\tSPSR_svc: %08x\n", (unsigned)cpu->reg[ARM7_REG_SPSR_SVC]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tR13_abt:  %08x R14_abt: %08X\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_ABT),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_ABT));

        washdc_log_error("\tSPSR_abt: %08x\n", (unsigned)cpu->reg[ARM7_REG_SPSR_ABT]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tR13_und:  %08x R14_und: %08X\n",
                         (unsigned)*arm7_gen_reg_bank(cpu, 13, ARM7_MODE_UND),
                         (unsigned)*arm7_gen_reg_bank(cpu, 14, ARM7_MODE_UND));

        washdc_log_error("\tSPSR_und: %08x\n", (unsigned)cpu->reg[ARM7_REG_SPSR_UND]);

        washdc_log_error("\t------------------------------------------------------------\n");

        washdc_log_error("\tpipeline status:\n");

#ifdef HAVE_CAPSTONE
        csh capstone_handle;
        bool capstone_avail;
        cs_insn *insn;
        cs_err cs_err_val = cs_open(CS_ARCH_ARM,
                                    arm7_thumb_mode(cpu) ?
                                    CS_MODE_THUMB : CS_MODE_ARM,
                                    &capstone_handle);
        capstone_avail = (cs_err_val == CS_ERR_OK);
#endif

        size_t count;
        if (arm7_thumb_mode(cpu)) {
#ifdef HAVE_CAPSTONE
            uint16_t pipeline_ordered[3] = {
                cpu->pipeline[2],
                cpu->pipeline[1],
                cpu->pipeline[0]
            };

            if (capstone_avail &&
                (count = cs_disasm(capstone_handle, pipeline_ordered,
                                   sizeof(pipeline_ordered),
                                   cpu->pipeline_pc[2], 3, &insn))) {
                char prologue[16];
                snprintf(prologue, sizeof(prologue), "\t%08x:",
                         (unsigned)cpu->pipeline_pc[2]);
                print_cs_insn(insn, washdc_log_error, prologue, "EXECUTE");
                if (count >= 2) {
                    snprintf(prologue, sizeof(prologue), "\t%08x:",
                             (unsigned)cpu->pipeline_pc[1]);
                    print_cs_insn(insn + 1, washdc_log_error,
                                  prologue, "DECODE");
                }
                if (count >= 3) {
                    snprintf(prologue, sizeof(prologue), "\t%08x:",
                             (unsigned)cpu->pipeline_pc[0]);
                    print_cs_insn(insn + 2, washdc_log_error,
                                  prologue, "FETCH");
                }
                cs_free(insn, count);
            } else {
#endif
                washdc_log_error("\texecute: %08x: %04x\n",
                                 (unsigned)cpu->pipeline_pc[2],
                                 (unsigned)cpu->pipeline[2]);
                washdc_log_error("\tdecode:  %08x: %04x\n",
                                 (unsigned)cpu->pipeline_pc[1],
                                 (unsigned)cpu->pipeline[1]);
                washdc_log_error("\tfetch:   %08x: %04x\n",
                                 (unsigned)cpu->pipeline_pc[0],
                                 (unsigned)cpu->pipeline[0]);
#ifdef HAVE_CAPSTONE
            }
#endif
        } else {
#ifdef HAVE_CAPSTONE
            uint32_t pipeline_ordered[3] = {
                cpu->pipeline[2],
                cpu->pipeline[1],
                cpu->pipeline[0]
            };

            if (capstone_avail &&
                (count = cs_disasm(capstone_handle, pipeline_ordered,
                                   sizeof(pipeline_ordered),
                                   cpu->pipeline_pc[2], 3, &insn))) {
                char prologue[16];
                snprintf(prologue, sizeof(prologue), "\t%08x:",
                         (unsigned)cpu->pipeline_pc[2]);
                print_cs_insn(insn, washdc_log_error, prologue, "EXECUTE");
                if (count >= 2) {
                    snprintf(prologue, sizeof(prologue), "\t%08x:",
                             (unsigned)cpu->pipeline_pc[1]);
                    print_cs_insn(insn + 1, washdc_log_error, prologue, "DECODE");
                }
                if (count >= 3) {
                    snprintf(prologue, sizeof(prologue), "\t%08x:",
                             (unsigned)cpu->pipeline_pc[0]);
                    print_cs_insn(insn + 2, washdc_log_error, prologue, "FETCH");
                }
                cs_free(insn, count);
            } else {
#endif
                washdc_log_error("\texecute: %08x: %08x\n",
                                 (unsigned)cpu->pipeline_pc[2],
                                 (unsigned)cpu->pipeline[2]);
                washdc_log_error("\tdecode:  %08x: %08x\n",
                                 (unsigned)cpu->pipeline_pc[1],
                                 (unsigned)cpu->pipeline[1]);
                washdc_log_error("\tfetch:   %08x: %08x\n",
                                 (unsigned)cpu->pipeline_pc[0],
                                 (unsigned)cpu->pipeline[0]);
#ifdef HAVE_CAPSTONE
            }
#endif
        }

        washdc_log_error("====================================================================\n");
        washdc_log_error("\tinstruction dump:\n");

#define INST_BEFORE_DISASM 32
#define INST_AFTER_DISASM 32
        uint32_t first_inst, last_inst;

        size_t bytes_per_inst = arm7_thumb_mode(cpu) ? 2 : 4;

        if (cpu->pipeline_pc[2] >= bytes_per_inst * INST_BEFORE_DISASM) {
            first_inst = cpu->pipeline_pc[2] -
                bytes_per_inst * INST_BEFORE_DISASM;
        } else {
            first_inst = 0;
        }

        if (cpu->pipeline_pc[2] <= UINT32_MAX -
            bytes_per_inst * INST_AFTER_DISASM) {
            last_inst = cpu->pipeline_pc[2] +
                bytes_per_inst * INST_AFTER_DISASM;
        } else {
            last_inst = UINT32_MAX - INST_AFTER_DISASM;
        }

        if (bytes_per_inst == 4) {
            unsigned n_inst = (last_inst - first_inst + 4) / 4;
            uint32_t *inst_buf = calloc(n_inst, 4);
            bool *inst_valid_buf = malloc(n_inst * sizeof(bool));
            if (inst_buf && inst_valid_buf) {
                unsigned inst_no;
                for (inst_no = 0; inst_no < n_inst; inst_no++) {
                    uint32_t addr = inst_no * 4 + first_inst;
                    inst_valid_buf[inst_no] =
                        memory_map_try_read_32(&gameboy.map, addr,
                                               inst_buf + inst_no) ==
                        MEM_ACCESS_SUCCESS;
                }
#ifdef HAVE_CAPSTONE
                if (capstone_avail && (count = cs_disasm(capstone_handle, inst_buf,
                                                         n_inst * 4, first_inst, 0, &insn)) >= 1) {
                    uint32_t addr = first_inst;
                    for (inst_no = 0; inst_no < count; inst_no++) {
                        char const *annotation;
                        if (addr == cpu->pipeline_pc[2])
                            annotation = " <- failure occurred at this instruction";
                        else if (addr == cpu->reg[ARM7_REG_PC])
                            annotation = " <- PC";
                        else
                            annotation = "";
                        char addr_str[11];
                        snprintf(addr_str, sizeof(addr_str),
                                 "%08x: ", (unsigned)addr);
                        print_cs_insn(insn + inst_no, washdc_log_error,
                                      addr_str, annotation);
                        addr += insn[inst_no].size;
                    }
                } else {
#endif
                    for (inst_no = 0; inst_no < n_inst; inst_no++) {
                        uint32_t addr = inst_no * 4 + first_inst;
                        char const *annotation;
                        if (addr == cpu->pipeline_pc[2])
                            annotation = " <- failure occurred at this instruction";
                        else if (addr == cpu->reg[ARM7_REG_PC])
                            annotation = " <- PC";
                        else
                            annotation = "";
                        if (inst_valid_buf[inst_no]) {
                            if (strlen(annotation)) {
                                washdc_log_error("%08x: 0x%08x; %s\n", (unsigned)addr,
                                                 (unsigned)inst_buf[inst_no], annotation);
                            } else {
                                washdc_log_error("%08x: 0x%08x\n", (unsigned)addr,
                                                 (unsigned)inst_buf[inst_no]);
                            }
                        } else {
                            if (strlen(annotation)) {
                                washdc_log_error("%08x: <error reading address> ; %s\n",
                                                 (unsigned)addr, annotation);
                            } else {
                                washdc_log_error("%08x: <error reading address>\n", (unsigned)addr);
                            }
                        }
                    }
#ifdef HAVE_CAPSTONE
                }
#endif
            } else {
                washdc_log_error("unable to dump instructions due to failed "
                                 "memory allocation\n");
            }
            free(inst_buf);
            free(inst_valid_buf);
        } else {
            unsigned n_inst = (last_inst - first_inst + 2) / 2;
            uint16_t *inst_buf = calloc(n_inst, 2);
            bool *inst_valid_buf = malloc(n_inst * sizeof(bool));
            if (inst_buf && inst_valid_buf) {
                unsigned inst_no;
                for (inst_no = 0; inst_no < n_inst; inst_no++) {
                    uint32_t addr = inst_no * 2 + first_inst;
                    inst_valid_buf[inst_no] =
                        memory_map_try_read_16(&gameboy.map, addr,
                                               inst_buf + inst_no) ==
                        MEM_ACCESS_SUCCESS;
                }
#ifdef HAVE_CAPSTONE
                if (capstone_avail && (count = do_disasm_thumb(capstone_handle, inst_buf, &first_inst,
                                                               &last_inst, &insn)) >= 1) {
                    uint32_t addr = first_inst;
                    for (inst_no = 0; inst_no < count; inst_no++) {
                        char const *annotation;
                        if (addr == cpu->pipeline_pc[2])
                            annotation = " <- failure occurred at this instruction";
                        else if (addr == cpu->reg[ARM7_REG_PC])
                            annotation = " <- PC";
                        else
                            annotation = "";
                        char addr_str[11];
                        snprintf(addr_str, sizeof(addr_str),
                                 "%08x: ", (unsigned)addr);
                        print_cs_insn(insn + inst_no, washdc_log_error,
                                      addr_str, annotation);
                        addr += insn[inst_no].size;
                    }
                } else {
#endif
                    for (inst_no = 0; inst_no < n_inst; inst_no++) {
                        uint32_t addr = inst_no * 2 + first_inst;
                        char const *annotation;
                        if (addr == cpu->pipeline_pc[2])
                            annotation = " <- failure occurred at this instruction";
                        else if (addr == cpu->reg[ARM7_REG_PC])
                            annotation = " <- PC";
                        else
                            annotation = "";
                        if (inst_valid_buf[inst_no]) {
                            if (strlen(annotation)) {
                                washdc_log_error("%08x: %04x ; %s\n", addr,
                                                 (unsigned)inst_buf[inst_no],
                                                 annotation);
                            } else {
                                washdc_log_error("%08x: %04x\n", addr,
                                                 (unsigned)inst_buf[inst_no]);
                            }
                        } else {
                            if (strlen(annotation)) {
                                washdc_log_error("%08x: <error reading address> ; %s\n",
                                                 addr, annotation);
                            } else {
                                washdc_log_error("%08x: <error reading address>\n", addr);
                            }
                        }
                    }
#if HAVE_CAPSTONE
                }
#endif
            } else {
                washdc_log_error("unable to dump instructions due to failed "
                                 "memory allocation\n");
            }
            free(inst_buf);
            free(inst_valid_buf);
        }

#ifdef HAVE_CAPSTONE
        if (capstone_avail)
            cs_close(&capstone_handle);
#endif

#define STACK_DUMP_N_DWORDS 128
#define STACK_DUMP_N_BYTES (STACK_DUMP_N_DWORDS * 4)

        uint32_t first_stack_addr, last_stack_addr;
        uint32_t stack_ptr = (*arm7_gen_reg(cpu, 13) / 16) * 16;

        if (stack_ptr >= STACK_DUMP_N_BYTES / 2)
            first_stack_addr = stack_ptr - STACK_DUMP_N_BYTES / 2;
        else
            first_stack_addr = 0;

        if (first_stack_addr <= UINT32_MAX - (STACK_DUMP_N_BYTES - 16))
            last_stack_addr = first_stack_addr + (STACK_DUMP_N_BYTES - 16);
        else
            last_stack_addr = UINT32_MAX;

        washdc_log_error("====================================================================\n");
        washdc_log_error("\tStack dump of %u bytes from %08x (stack pointer is %08x)\n",
                         (unsigned)(last_stack_addr - first_stack_addr + 16),
                         (unsigned)first_stack_addr,
                         (unsigned)*arm7_gen_reg(cpu, 13));

        washdc_log_error("           0 1 2 3  4 5 6 7  8 9 a b  c d e f\n");

        uint32_t addr;
        for (addr = first_stack_addr; addr != (last_stack_addr + 16); addr += 16) {
            bool valid[4] = { false, false, false, false };
            uint32_t vals[4];
            if (memory_map_try_read_32(&gameboy.map, addr, vals) == MEM_ACCESS_SUCCESS)
                valid[0] = true;
            if (memory_map_try_read_32(&gameboy.map, addr + 4, vals + 1) == MEM_ACCESS_SUCCESS)
                valid[1] = true;
            if (memory_map_try_read_32(&gameboy.map, addr + 8, vals + 2) == MEM_ACCESS_SUCCESS)
                valid[2] = true;
            if (memory_map_try_read_32(&gameboy.map, addr + 12, vals + 3) == MEM_ACCESS_SUCCESS)
                valid[3] = true;
            washdc_log_error("%08x:", (unsigned)addr);
            if (valid[0])
                washdc_log_error(" %08x", (unsigned)vals[0]);
            else
                washdc_log_error(" <error> ");
            if (valid[1])
                washdc_log_error(" %08x", (unsigned)vals[1]);
            else
                washdc_log_error(" <error> ");
            if (valid[2])
                washdc_log_error(" %08x", (unsigned)vals[2]);
            else
                washdc_log_error(" <error> ");
            if (valid[3])
                washdc_log_error(" %08x\n", (unsigned)vals[3]);
            else
                washdc_log_error(" <error>\n");
        }

        washdc_log_error("====================================================================\n");
        washdc_log_error("==\n");
        washdc_log_error("== END OF EMULATION ERROR REPORT\n");
        washdc_log_error("==\n");
        washdc_log_error("====================================================================\n");

        wash_kill();
    }

    washdc_log_info("the emulation core is no longer executing; "
                    "cleanup will commence.\n");

    washdc_log_info("total number of instructions executed: %llu\n",
                    gameboy.inst_counter);
    washdc_log_info("total number of frames emulated: %u\n", n_frames);

#ifdef ENABLE_DEBUGGER
    if (en_washdbg)
        debug_cleanup();
#endif

    /* wash_state_transition(WASH_STATE_NOT_RUNNING, WASH_STATE_RUNNING); */

    renderer_cleanup(&renderer);
    gui_cleanup(&gui);

    gba_cleanup(&gameboy);

#ifdef IO_THREAD
    washdc_log_info("killing io_thread...\n");
    io_kick();
    io_cleanup();
    washdc_log_info("io_thread has been terminated.\n");
#endif

    log_cleanup();
    washdc_hostfile_cleanup();

    SDL_GL_DeleteContext(ctxt);
    SDL_DestroyWindow(win);
    SDL_Quit();

    return 0;
}
