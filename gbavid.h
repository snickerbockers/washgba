/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBAVID_H_
#define GBAVID_H_

#include <stdbool.h>
#include <stdint.h>

#include "sched.h"
#include "gbaintc.h"

struct gba;

struct gbavid {
    struct dc_clock *clock;
    struct gbaintc *intc;
    struct gba *gba;
    unsigned xpos, ypos;
    cycle_stamp last_sync;
    uint32_t dispcnt;
    uint32_t dispstat;

    bool vblank_event_scheduled;
    struct SchedEvent vblank_event;

    bool hblank_event_scheduled;
    struct SchedEvent hblank_event;
};

void gbavid_init(struct gbavid *vid, struct gbaintc *intc,
                 struct dc_clock *clock, struct gba *gba);
void gbavid_cleanup(struct gbavid *vid);

uint32_t gbavid_vcount(struct gbavid *vid);
uint32_t gbavid_hcount(struct gbavid *vid);

bool gbavid_irq(struct gbavid *vid);

void gbavid_sync(struct gbavid *vid);

uint32_t gbavid_get_dispcnt(struct gbavid const *gbavid);
void gbavid_set_dispcnt(struct gbavid *gbavid, uint32_t dispcnt);

uint32_t gbavid_get_dispstat(struct gbavid *gbavid);
void gbavid_set_dispstat(struct gbavid *gbavid, uint32_t dispstat);

#endif
