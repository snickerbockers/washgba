/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "sched.h"

#include "error.h"
#include "gbavid.h"
#include "gbaintc.h"
#include "gbadmac.h"

#define CYCLES_PER_PIX 4

/*
 * width and height of the screen.  These include the hblank and vblank pixels,
 * so it's not the width and height of the actual display/framebuffer; it's the
 * width and height that's used for video timing.
 */
#define VID_WIDTH 308
#define VID_HEIGHT 228

void gbavid_sync(struct gbavid *vid);

static bool gbavid_vblank_event_handler(struct SchedEvent *event);
static void sched_vblank_event(struct gbavid *vid);

static bool gbavid_hblank_event_handler(struct SchedEvent *event);
static void sched_hblank_event(struct gbavid *vid);

void gbavid_init(struct gbavid *vid, struct gbaintc *intc,
                 struct dc_clock *clock, struct gba *gba) {
    memset(vid, 0, sizeof(*vid));
    vid->clock = clock;
    vid->intc = intc;
    vid->gba = gba;

    vid->vblank_event.arg_ptr = vid;
    vid->vblank_event.handler = gbavid_vblank_event_handler;

    vid->hblank_event.arg_ptr = vid;
    vid->hblank_event.handler = gbavid_hblank_event_handler;

    sched_hblank_event(vid);
    sched_vblank_event(vid);
}

void gbavid_cleanup(struct gbavid *vid) {
}

static void sched_vblank_event(struct gbavid *vid) {
    if (vid->vblank_event_scheduled)
        CRITICAL_ERROR(ERROR_INTEGRITY);

    unsigned lines_until_vblank;
    if (vid->ypos < 160)
        lines_until_vblank = 160 - 1 - vid->ypos;
    else
        lines_until_vblank = VID_HEIGHT - 1 - vid->ypos + 160;

    unsigned rem_pix_this_line = VID_WIDTH - vid->xpos;
    unsigned rem_pix = rem_pix_this_line + lines_until_vblank * VID_WIDTH;
    cycle_stamp rem_cycles = (cycle_stamp)rem_pix * CYCLES_PER_PIX;

    vid->vblank_event.when =
        (clock_cycle_stamp(vid->clock) / CYCLES_PER_PIX) * CYCLES_PER_PIX +
        rem_cycles;
    sched_event(vid->clock, &vid->vblank_event);
    vid->vblank_event_scheduled = true;
}

static DEF_ERROR_INT_ATTR(hcount)
static DEF_ERROR_INT_ATTR(vcount)

static bool gbavid_vblank_event_handler(struct SchedEvent *event) {
    struct gbavid *vid = event->arg_ptr;

    if (!vid->vblank_event_scheduled)
        CRITICAL_ERROR(ERROR_INTEGRITY);

    vid->vblank_event_scheduled = false;

    gbavid_sync(vid);

    if (vid->ypos != 160 || vid->xpos != 0) {
        error_set_hcount(vid->xpos);
        error_set_vcount(vid->ypos);
        error_set_current_dc_cycle_stamp(clock_cycle_stamp(vid->clock));
        EMU_ERROR(ERROR_TIMING);
    }

    if (vid->dispstat & (1 << 3)) {
        gbaintc_raise_irq_outside_cpu_context(vid->intc, GBAINTC_VBLANK_MASK);
        LOG_DBG("%s - JUST RAISED A VBLANK INTERRUPT\n", __func__);
    }

    gba_dmac_notify_vblank(vid->gba);

    sched_vblank_event(vid);

    return true; // signal end of frame
}

static void sched_hblank_event(struct gbavid *vid) {
    if (vid->hblank_event_scheduled)
        CRITICAL_ERROR(ERROR_INTEGRITY);

    unsigned rem_pix;
    if (vid->xpos < 240)
        rem_pix = 240 - vid->xpos;
    else
        rem_pix = VID_WIDTH - vid->xpos + 240;

    cycle_stamp rem_cycles = (cycle_stamp)rem_pix * CYCLES_PER_PIX;

    vid->hblank_event.when =
        (clock_cycle_stamp(vid->clock) / CYCLES_PER_PIX) * CYCLES_PER_PIX +
        rem_cycles;
    sched_event(vid->clock, &vid->hblank_event);
    vid->hblank_event_scheduled = true;
}

static bool gbavid_hblank_event_handler(struct SchedEvent *event) {
    struct gbavid *vid = event->arg_ptr;

    if (!vid->hblank_event_scheduled)
        CRITICAL_ERROR(ERROR_INTEGRITY);

    vid->hblank_event_scheduled = false;

    gbavid_sync(vid);

    if (vid->xpos != 240) {
        error_set_hcount(vid->xpos);
        error_set_vcount(vid->ypos);
        error_set_current_dc_cycle_stamp(clock_cycle_stamp(vid->clock));
        EMU_ERROR(ERROR_TIMING);
    }

    if (vid->dispstat & (1 << 4)) {
        gbaintc_raise_irq_outside_cpu_context(vid->intc, GBAINTC_HBLANK_MASK);
        LOG_DBG("%s - JUST RAISED AN HBLANK INTERRUPT\n", __func__);
    }

    gba_dmac_notify_hblank(vid->gba);

    sched_hblank_event(vid);

    return false; // do not signal end of frame
}

void gbavid_sync(struct gbavid *vid) {
    cycle_stamp new_stamp = clock_cycle_stamp(vid->clock);
    cycle_stamp vid_ticks = (new_stamp - vid->last_sync) / CYCLES_PER_PIX;

    if (vid_ticks > 0) {
        vid->last_sync = (new_stamp / CYCLES_PER_PIX) * CYCLES_PER_PIX;
        vid->xpos = vid->xpos + vid_ticks;
        vid->ypos = (vid->ypos + vid->xpos / VID_WIDTH) % VID_HEIGHT;
        vid->xpos %= VID_WIDTH;
    }
}

uint32_t gbavid_vcount(struct gbavid *vid) {
    gbavid_sync(vid);
    return vid->ypos;
}

uint32_t gbavid_hcount(struct gbavid *vid) {
    gbavid_sync(vid);
    return vid->xpos;
}

uint32_t gbavid_get_dispcnt(struct gbavid const *gbavid) {
    return gbavid->dispcnt;
}

void gbavid_set_dispcnt(struct gbavid *gbavid, uint32_t dispcnt) {
    LOG_DBG("GBAIO: write %04x to DISPCNT\n", (unsigned)dispcnt);
    gbavid->dispcnt = dispcnt;
}

uint32_t gbavid_get_dispstat(struct gbavid *gbavid) {
    // general lcd status
    uint32_t vcount = gbavid_vcount(gbavid);
    uint32_t hcount = gbavid_hcount(gbavid);
    gbavid->dispstat &= ~0x47;
    if (vcount >= 160 && vcount <= 226)
        gbavid->dispstat |= 1;
    if (hcount >= 240)
        gbavid->dispstat |= 2;
    return gbavid->dispstat;
}

void gbavid_set_dispstat(struct gbavid *gbavid, uint32_t dispstat) {
    LOG_DBG("GBAIO: write %04x to DISPSTAT\n", (unsigned)dispstat);
    if (dispstat & ((1 << 5))) { // vcounter is not implemented yet
        error_set_value(dispstat);
        EMU_ERROR(ERROR_UNIMPLEMENTED); // h-blank and v-counter
    }
    gbavid->dispstat = dispstat;
}
