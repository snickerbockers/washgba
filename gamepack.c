/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "log.h"
#include "mem_code.h"

#include "gamepack.h"

static int gamepack_try_read8(uint32_t addr, uint8_t *val, void *ctxt);
static int gamepack_try_read16(uint32_t addr, uint16_t *val, void *ctxt);
static int gamepack_try_read32(uint32_t addr, uint32_t *val, void *ctxt);

static int gamepack_try_write32(uint32_t addr, uint32_t val, void *ctxt);

static int gamepack_sram_try_read8(uint32_t addr, uint8_t *val, void *ctxt);
static int gamepack_sram_try_write8(uint32_t addr, uint8_t val, void *ctxt);

struct memory_interface const gamepack_memory_interface = {
    .try_read8 = gamepack_try_read8,
    .try_read16 = gamepack_try_read16,
    .try_read32 = gamepack_try_read32,

    .try_write32 = gamepack_try_write32
};

struct memory_interface const gamepack_sram_memory_interface = {
    .try_read8 = gamepack_sram_try_read8,
    .try_write8 = gamepack_sram_try_write8
};

void gamepack_init(struct gamepack *pack, washdc_hostfile rom) {
    pack->dat = calloc(MAX_ROM_SIZE, 1);

    for (unsigned idx = 0; idx < MAX_ROM_SIZE; idx += 2) {
        unsigned openbus = idx & 0xffff;
        pack->dat[idx] = openbus & 0xff;
        pack->dat[idx + 1] = openbus >> 8;
    }

    if (rom != WASHDC_HOSTFILE_INVALID) {
        unsigned idx = 0;
        while (idx < MAX_ROM_SIZE) {
            int ch_in = washdc_hostfile_getc(rom);
            if (ch_in == WASHDC_HOSTFILE_EOF)
                break;
            else
                pack->dat[idx++] = ch_in;
        }
    }

    // TODO: support saving/loading SRAM to files
    pack->sram = calloc(GAMEPACK_SRAM_LEN, 1);
}

void gamepack_cleanup(struct gamepack *pack) {
    free(pack->dat);
    free(pack->sram);
}

static int gamepack_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    struct gamepack *pack = ctxt;
    if (addr < MAX_ROM_SIZE) {
        *val = pack->dat[addr];
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gamepack_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    struct gamepack *pack = ctxt;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (addr < MAX_ROM_SIZE) {
        memcpy(val, pack->dat + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gamepack_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    struct gamepack *pack = ctxt;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (addr < MAX_ROM_SIZE) {
        memcpy(val, pack->dat + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gamepack_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    if (addr & 3)
        return MEM_ACCESS_BADALIGN;
    LOG_WARN("%s - ignore 4-byte write of %08x to %08x\n",
             __func__, (unsigned)val, (unsigned)addr);
    return MEM_ACCESS_SUCCESS;
}

static int gamepack_sram_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    struct gamepack *pack = ctxt;
    if (addr < GAMEPACK_SRAM_LEN) {
        *val = pack->sram[addr];
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gamepack_sram_try_write8(uint32_t addr, uint8_t val, void *ctxt) {
    struct gamepack *pack = ctxt;
    if (addr < GAMEPACK_SRAM_LEN) {
        pack->sram[addr] = val;
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}
