/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBADMAC_H_
#define GBADMAC_H_

#include "sched.h"
#include "intmath.h"

#define GBA_DMAC_ENABLE_BIT (1 << 15)

#define GBA_DMAC_REPEAT_BIT (1 << 9)

#define GBA_DMAC_START_TIMING_BITS BIT_RANGE(12, 13)
#define GBA_DMAC_START_TIMING_IMMED 0
#define GBA_DMAC_START_TIMING_VBLANK (1 << 12)
#define GBA_DMAC_START_TIMING_HBLANK (2 << 12)
#define GBA_DMAC_START_TIMING_SPECIAL (3 << 12)

#define GBA_DMAC_CHAN_COUNT 4

struct gba;

struct gbadmac_chan {
    uint32_t saddr, daddr;
    uint16_t cnt_l, cnt_h;

    uint32_t daddr_internal;
};

struct gbadmac {
    struct gbadmac_chan chans[GBA_DMAC_CHAN_COUNT];
    struct SchedEvent events[4];
};

void gbadmac_init(struct gbadmac *dmac);
void gbadmac_cleanup(struct gbadmac *dmac);

void gba_dmac_set_saddr(struct gba *gba, unsigned chan, uint32_t saddr);
uint32_t gba_dmac_get_saddr(struct gba *gba, unsigned chan);

void gba_dmac_set_daddr(struct gba *gba, unsigned chan, uint32_t daddr);
uint32_t gba_dmac_get_daddr(struct gba *gba, unsigned chan);

void gba_dmac_set_cnt_l(struct gba *gba, unsigned chan, uint16_t cnt_l);
uint16_t gba_dmac_get_cnt_l(struct gba *gba, unsigned chan);

void gba_dmac_set_cnt_h(struct gba *gba, unsigned chan, uint16_t cnt_h);
uint16_t gba_dmac_get_cnt_h(struct gba *gba, unsigned chan);

void gba_dmac_notify_hblank(struct gba *gba);
void gba_dmac_notify_vblank(struct gba *gba);

#endif
