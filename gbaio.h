/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBAIO_H_
#define GBAIO_H_

#include <stdint.h>

#include "memory_map.h"

#define GBAIO_FIRST 0x04000000
#define GBAIO_LAST 0x040003ff

#define GBAIO_LEN ((GBAIO_LAST - GBAIO_FIRST + 1) / 2)

struct gbaio {
    uint16_t buf[GBAIO_LEN];
};

void gbaio_init(struct gbaio *gbaio);
void gbaio_cleanup(struct gbaio *gbaio);

extern struct memory_interface const gbaio_memory_interface;

#endif
