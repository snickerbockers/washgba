/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBA_H_
#define GBA_H_

#include "gbafw.h"
#include "gbaram.h"
#include "gbavram.h"
#include "gbaoam.h"
#include "palram.h"
#include "gba_onchip_ram.h"
#include "gbaio.h"
#include "gbaintc.h"
#include "memory_map.h"
#include "arm7.h"
#include "sched.h"
#include "gamepack.h"
#include "gbadmac.h"
#include "gbavid.h"

typedef unsigned gba_btn_state;

#define GBA_BTN_A (1 << 0)
#define GBA_BTN_B (1 << 1)
#define GBA_BTN_SELECT (1 << 2)
#define GBA_BTN_START (1 << 3)
#define GBA_BTN_RIGHT (1 << 4)
#define GBA_BTN_LEFT (1 << 5)
#define GBA_BTN_UP (1 << 6)
#define GBA_BTN_DOWN (1 << 7)
#define GBA_BTN_R (1 << 8)
#define GBA_BTN_L (1 << 9)

struct gba {
    struct gbafw fw;
    struct gbaram ram;
    struct gba_onchip_ram onchip_ram;
    struct gbavram vram;
    struct gbaoam oam;
    struct palram pal;
    struct gbaio io;
    struct gbaintc intc;
    struct memory_map map;
    struct arm7 cpu;
    struct dc_clock clock;
    struct gamepack game;
    struct gbadmac dmac;
    struct gbavid vid;

    // counts number of instructions executed
    unsigned long long inst_counter;

    gba_btn_state btns;
};

#endif
