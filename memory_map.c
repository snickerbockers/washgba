/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2016-2019, 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stddef.h>
#include <string.h>

#include "error.h"
#include "mem_code.h"

#include "memory_map.h"

void memory_map_init(struct memory_map *map) {
    memset(map, 0, sizeof(*map));
}

void memory_map_cleanup(struct memory_map *map) {
    memset(map, 0, sizeof(*map));
}

#define MEMORY_MAP_TRY_READ_TMPL(type, type_postfix)                    \
    int memory_map_try_read_##type_postfix(struct memory_map *map,      \
                                           uint32_t addr, type *val) {  \
        uint32_t first_addr = addr;                                     \
        uint32_t last_addr = sizeof(type) - 1 + first_addr;             \
                                                                        \
        unsigned region_no;                                             \
        for (region_no = 0; region_no < map->n_regions; region_no++) {  \
            struct memory_map_region *reg = map->regions + region_no;   \
            uint32_t range_mask = reg->range_mask;                      \
            if ((first_addr & range_mask) >= reg->first_addr &&         \
                (last_addr & range_mask) <= reg->last_addr) {           \
                struct memory_interface const *intf = reg->intf;        \
                uint32_t first = reg->first_addr;                       \
                void *ctxt = reg->ctxt;                                 \
                if (intf->try_read##type_postfix) {                     \
                    return intf->try_read##type_postfix(addr - first,   \
                                                        val, ctxt);     \
                } else {                                                \
                    return MEM_ACCESS_BADSIZE;                          \
                }                                                       \
            }                                                           \
        }                                                               \
                                                                        \
        return MEM_ACCESS_FAILURE;                                      \
    }

MEMORY_MAP_TRY_READ_TMPL(uint8_t, 8)
MEMORY_MAP_TRY_READ_TMPL(uint16_t, 16)
MEMORY_MAP_TRY_READ_TMPL(uint32_t, 32)

#define MEM_MAP_TRY_WRITE_TMPL(type, type_postfix)                      \
    int memory_map_try_write_##type_postfix(struct memory_map *map,     \
                                            uint32_t addr, type val) {  \
        uint32_t first_addr = addr;                                     \
        uint32_t last_addr = sizeof(type) - 1 + first_addr;             \
                                                                        \
        unsigned region_no;                                             \
        for (region_no = 0; region_no < map->n_regions; region_no++) {  \
            struct memory_map_region *reg = map->regions + region_no;   \
            uint32_t range_mask = reg->range_mask;                      \
            if ((first_addr & range_mask) >= reg->first_addr &&         \
                (last_addr & range_mask) <= reg->last_addr) {           \
                struct memory_interface const *intf = reg->intf;        \
                uint32_t first = reg->first_addr;                       \
                void *ctxt = reg->ctxt;                                 \
                if (intf->try_write##type_postfix) {                    \
                    return intf->try_write##type_postfix(addr - first,  \
                                                         val, ctxt);    \
                } else {                                                \
                    return MEM_ACCESS_BADSIZE;                          \
                }                                                       \
            }                                                           \
        }                                                               \
        return MEM_ACCESS_FAILURE;                                      \
    }

MEM_MAP_TRY_WRITE_TMPL(uint8_t, 8)
MEM_MAP_TRY_WRITE_TMPL(uint16_t, 16)
MEM_MAP_TRY_WRITE_TMPL(uint32_t, 32)

void
memory_map_add(struct memory_map *map,
               uint32_t addr_first,
               uint32_t addr_last,
               uint32_t range_mask,
               enum memory_map_region_id id,
               struct memory_interface const *intf,
               void *ctxt) {
    if (map->n_regions >= MAX_MEM_MAP_REGIONS)
        CRITICAL_ERROR(ERROR_OVERFLOW);

    struct memory_map_region *reg = map->regions + map->n_regions++;

    reg->first_addr = addr_first;
    reg->last_addr = addr_last;
    reg->range_mask = range_mask;
    reg->id = id;
    reg->intf = intf;
    reg->ctxt = ctxt;
}
