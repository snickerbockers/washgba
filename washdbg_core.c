/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2018-2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include "capstone/capstone.h"

#include "log.h"
#include "debugger.h"
#include "arm7_reg_idx.h"
#include "error.h"
#include "emu.h"
#include "washdbg_tcp.h"
#include "compiler_bullshit.h"
#include "mem_code.h"
#include "intmath.h"

#include "washdbg_core.h"

#ifndef USE_LIBEVENT
#error this file should not be built with USE_LIBEVENT disabled!
#endif
#ifndef ENABLE_DEBUGGER
#error this file whould not be built with ENABLE_DEBUGGER disabled!
#endif

#define BUF_LEN 1024

static char in_buf[BUF_LEN];
static unsigned in_buf_pos;

static csh capstone_handle_arm, capstone_handle_thumb;
static bool capstone_avail;

struct washdbg_txt_state {
    char const *txt;
    unsigned pos;
};

/*
 * map bp index to address
 */
static struct washdbg_bp_stat {
    uint32_t addr;
    bool enabled;
    bool valid;
} washdbg_bp_stat[NUM_DEBUG_CONTEXTS][DEBUG_N_BREAKPOINTS];

static struct name_map {
    char const *str;
    int idx;
} const arm7_reg_map[] = {
    { "r0", ARM7_REG_R0 },
    { "r1", ARM7_REG_R1 },
    { "r2", ARM7_REG_R2 },
    { "r3", ARM7_REG_R3 },
    { "r4", ARM7_REG_R4 },
    { "r5", ARM7_REG_R5 },
    { "r6", ARM7_REG_R6 },
    { "r7", ARM7_REG_R7 },
    { "r8", ARM7_REG_R8 },
    { "r9", ARM7_REG_R9 },
    { "r10", ARM7_REG_R10 },
    { "r11", ARM7_REG_R11 },
    { "r12", ARM7_REG_R12 },
    { "r13", ARM7_REG_R13 },
    { "r14", ARM7_REG_R14 },
    { "r15", ARM7_REG_R15 },

    { "sb", ARM7_REG_R9 },
    { "sl", ARM7_REG_R10 },
    { "fp", ARM7_REG_R11 },
    { "ip", ARM7_REG_R12 },
    { "sp", ARM7_REG_R13 },
    { "lr", ARM7_REG_R14 },
    { "pc", ARM7_REG_PC },

    { "r8_fiq", ARM7_REG_R8_FIQ },
    { "r9_fiq", ARM7_REG_R9_FIQ },
    { "r10_fiq", ARM7_REG_R10_FIQ },
    { "r11_fiq", ARM7_REG_R11_FIQ },
    { "r12_fiq", ARM7_REG_R12_FIQ },
    { "r13_fiq", ARM7_REG_R13_FIQ },
    { "r14_fiq", ARM7_REG_R14_FIQ },
    { "r13_svc", ARM7_REG_R13_SVC },
    { "r14_svc", ARM7_REG_R14_SVC },
    { "r13_abt", ARM7_REG_R13_ABT },
    { "r14_abt", ARM7_REG_R14_ABT },
    { "r13_irq", ARM7_REG_R13_IRQ },
    { "r14_irq", ARM7_REG_R14_IRQ },
    { "r13_und", ARM7_REG_R13_UND },
    { "r14_und", ARM7_REG_R14_UND },

    { "cpsr", ARM7_REG_CPSR },

    { "spsr_fiq", ARM7_REG_SPSR_FIQ },
    { "spsr_svc", ARM7_REG_SPSR_SVC },
    { "spsr_abt", ARM7_REG_SPSR_ABT },
    { "spsr_irq", ARM7_REG_SPSR_IRQ },
    { "spsr_und", ARM7_REG_SPSR_UND },

    { NULL }
};

static char *washdbg_strdup(char const *input) {
    size_t len = strlen(input) + 1;
    char *ret = malloc(len);
    strcpy(ret, input);
    return ret;
}

static void washdbg_process_input(void);
static int washdbg_puts(char const *txt);

static unsigned washdbg_print_buffer(struct washdbg_txt_state *state);

static void washdbg_print_banner(void);

static void washdbg_echo(int argc, char **argv);

static void washdbg_state_echo_process(void);

static int
eval_expression(char const *expr, enum dbg_context_id *ctx_id, unsigned *out);

static unsigned washdbg_print_x(void);

static int reg_idx_arm7(char const *reg_name);

static bool is_dec_str(char const *str);
static unsigned parse_dec_str(char const *str);
static bool is_hex_str(char const *str);
static unsigned parse_hex_str(char const *str);

#if defined(ENABLE_DBG_COND) || defined(ENABLE_MMU)
static int parse_int_str(char const *valstr, uint32_t *out);
#endif

static char const *
washdbg_disas_single_arm7(uint32_t addr, uint32_t val, uint32_t *next_addr);
static char const *
washdbg_disas_single_thumb(uint32_t addr, uint32_t *next_addr);

enum washdbg_byte_count {
    WASHDBG_1_BYTE = 1,
    WASHDBG_2_BYTE = 2,
    WASHDBG_4_BYTE = 4,
    WASHDBG_INST   = 5,
    WASHDBG_INST_THUMB = 6
};

static int
parse_fmt_string(char const *str, enum washdbg_byte_count *byte_count_out,
                 unsigned *count_out);

enum washdbg_state {
    WASHDBG_STATE_BANNER,
    WASHDBG_STATE_PROMPT,
    WASHDBG_STATE_NORMAL,
    WASHDBG_STATE_BAD_INPUT,
    WASHDBG_STATE_CMD_CONTINUE,
    WASHDBG_STATE_RUNNING,
    WASHDBG_STATE_HELP,
    WASHDBG_STATE_CONTEXT_INFO,
    WASHDBG_STATE_PRINT_ERROR,
    WASHDBG_STATE_ECHO,
    WASHDBG_STATE_X,
    WASHDBG_STATE_CMD_BPSET,
    WASHDBG_STATE_CMD_BPLIST,
    WASHDBG_STATE_CMD_PRINT,
    WASHDBG_STATE_CMD_TRANS_ITLB,
    WASHDBG_STATE_CMD_TRANS_UTLB,
    WASHDBG_STATE_CMD_ASID,
    WASHDBG_STAT_CMD_AT_MODE,
    WASHDBG_STATE_CMD_AT_MODE,
    WASHDBG_STATE_CMD_DUMP,
    WASHDBG_STATE_CMD_REGS,
    WASHDBG_STATE_INJECT_IRQ_USAGE_STRING,

    // permanently stop accepting commands because we're about to disconnect.
    WASHDBG_STATE_CMD_EXIT
} cur_state;

void washdbg_init(void) {
    fprintf(stderr, "%s called\n", __func__);
    cs_err cs_err_val = cs_open(CS_ARCH_ARM, CS_MODE_ARM,
                                &capstone_handle_arm);
    if (cs_err_val != CS_ERR_OK) {
        // disable disassembly for ARM7
        fprintf(stderr, "cs_open returned error code %d\n", (int)cs_err_val);
        capstone_avail = false;
    } else {
        capstone_avail = true;
    }

    cs_err_val = cs_open(CS_ARCH_ARM, CS_MODE_THUMB,
                         &capstone_handle_thumb);
    if (cs_err_val != CS_ERR_OK) {
        // disable disassembly for ARM7
        fprintf(stderr, "cs_open returned error code %d\n", (int)cs_err_val);
        capstone_avail = false;
        cs_close(&capstone_handle_arm);
    } else {
        capstone_avail = true;
    }

    washdbg_print_banner();
    memset(washdbg_bp_stat, 0, sizeof(washdbg_bp_stat));
}

void washdbg_cleanup(void* argp) {
    if (capstone_avail) {
        cs_close(&capstone_handle_thumb);
        cs_close(&capstone_handle_arm);
    }
    capstone_avail = false;
}

static struct continue_state {
    struct washdbg_txt_state txt;
} continue_state;

void washdbg_do_continue(int argc, char **argv) {
    continue_state.txt.txt = "Continuing execution\n";
    continue_state.txt.pos = 0;

    cur_state = WASHDBG_STATE_CMD_CONTINUE;
}

static bool washdbg_is_continue_cmd(char const *cmd) {
    return strcmp(cmd, "c") == 0 ||
        strcmp(cmd, "continue") == 0;
}

void washdbg_do_exit(int argc, char **argv) {
    printf("User requested exit via WashDbg\n");
    wash_kill();
    cur_state = WASHDBG_STATE_CMD_EXIT;
}

static bool washdbg_is_exit_cmd(char const *cmd) {
    return strcmp(cmd, "exit") == 0;
}

void washdbg_input_ch(char ch) {
    if (ch == '\r')
        return;

    // in_buf[1023] will always be \0
    if (in_buf_pos <= (BUF_LEN - 2))
        in_buf[in_buf_pos++] = ch;
}

static bool washdbg_is_step_cmd(char const *cmd) {
    return strcmp(cmd, "s") == 0 ||
        strcmp(cmd, "step") == 0;
}

static void washdbg_do_step(int argc, char **argv) {
    printf("WashDbg single-step requested\n");
    cur_state = WASHDBG_STATE_RUNNING;
    debug_request_single_step();
}

struct print_banner_state {
    struct washdbg_txt_state txt;
} print_banner_state;

static void washdbg_print_banner(void) {
    // this gets printed to the dev console every time somebody connects to the debugger
    static char const *login_banner =
        "Welcome to WashDbg!\n"
        "WashingtonDC Copyright (C) 2016-2021 snickerbockers\n"
        "This program comes with ABSOLUTELY NO WARRANTY;\n"
        "This is free software, and you are welcome to redistribute it\n"
        "under the terms of the GNU GPL version 3.\n\n";

    print_banner_state.txt.txt = login_banner;
    print_banner_state.txt.pos = 0;

    cur_state = WASHDBG_STATE_BANNER;
}

static struct help_state {
    struct washdbg_txt_state txt;
} help_state;

void washdbg_do_help(int argc, char **argv) {
    static char const *help_msg =
        "WashDbg command list\n"
        "\n"
        "asid         - print current address space\n"
#ifdef ENABLE_MMU
        "at-mode      - allow automatic address translation based on MMU\n"
#endif
        "bpdel        - delete a breakpoint\n"
        "bpdis        - disable a breakpoint\n"
        "bpen         - enable a breakpoint\n"
        "bplist       - list all breakpoints\n"
        "bpset <addr> - set a breakpoint\n"
        "continue     - continue execution when suspended.\n"
        "crash        - crash the emulator.\n"
        "dump         - dump memory to disk\n"
        "echo         - echo back text\n"
        "exit         - exit the debugger and close WashingtonDC\n"
        "help         - display this message\n"
        "inject_irq   - artificially inject an IRQ\n"
#ifdef ENABLE_DBG_COND
        "memwatch     - watch a specific memory address for a specific value\n"
#endif
        "print        - print a value\n"
        "regs         - print all registers\n"
#ifdef ENABLE_DBG_COND
        "regwatch     - watch for a register to be set to a given value\n"
#endif
        "step         - single-step\n"
#ifdef ENABLE_MMU
        "trans_itlb   - translate pointer using ITLB or UTLB\n"
        "trans_utlb   - translate pointer using UTLB only\n"
#endif
        "x            - eXamine memory address\n"
        "\n"
        "to asynchronously suspend execution and return to this prompt, send a "
        "line break\n";

    help_state.txt.txt = help_msg;
    help_state.txt.pos = 0;
    cur_state = WASHDBG_STATE_HELP;
}

static bool washdbg_is_help_cmd(char const *cmd) {
    return strcmp(cmd, "help") == 0;
}

struct context_info_state {
    char msg[128];
    struct washdbg_txt_state txt;
} context_info_state;

/*
 * Display info about the current context before showing a new prompt
 */
void washdbg_print_context_info(void) {
    uint32_t pc_next;
    uint32_t inst32;
    uint16_t inst16;
    char const *disas = NULL;
    char const *isa;
    char const *curctx;

    switch (debug_current_context()) {
    case DEBUG_CONTEXT_ARM7:
        pc_next = debug_pc_next(DEBUG_CONTEXT_ARM7);
        isa = debug_get_isa(DEBUG_CONTEXT_ARM7);
        if (strcmp(WASHDBG_ISA_ARM, isa) == 0) {
            curctx = "ARM7";
            if (debug_read_mem(DEBUG_CONTEXT_ARM7, &inst32,
                               pc_next, sizeof(inst32)) == 0) {
                disas = washdbg_disas_single_arm7(pc_next, inst32, NULL);
            }
        } else if (strcmp(WASHDBG_ISA_THUMB, isa) == 0) {
            curctx = "ARM7/THUMB";
	    disas = washdbg_disas_single_thumb(pc_next, NULL);
        }
        snprintf(context_info_state.msg, sizeof(context_info_state.msg),
                 "Current debug context is %s\n"
                 "PC is 0x%08x\n"
                 "next_inst:\n\t0x%08x: %s\n",
                 curctx,
                 (unsigned)debug_get_reg(DEBUG_CONTEXT_ARM7, ARM7_REG_PC),
                 (unsigned)pc_next, disas ? disas : "");
        break;
    default:
        snprintf(context_info_state.msg, sizeof(context_info_state.msg),
                 "Current debug context is <unknown/error>\n");
    }
    context_info_state.msg[sizeof(context_info_state.msg) - 1] = '\0';
    context_info_state.txt.txt = context_info_state.msg;
    context_info_state.txt.pos = 0;
    cur_state = WASHDBG_STATE_CONTEXT_INFO;
}

struct print_prompt_state {
    struct washdbg_txt_state txt;
} print_prompt_state;

void washdbg_print_prompt(void) {
    static char const *prompt = "(WashDbg): ";

    print_prompt_state.txt.txt = prompt;
    print_prompt_state.txt.pos = 0;
    cur_state = WASHDBG_STATE_PROMPT;
}

static struct bad_input_state {
    struct washdbg_txt_state txt;
    char bad_input_line[BUF_LEN];
} bad_input_state;

static void washdbg_bad_input(char const *bad_cmd) {
    snprintf(bad_input_state.bad_input_line,
             sizeof(bad_input_state.bad_input_line),
             "Unrecognized input \"%s\"\n", bad_cmd);
    bad_input_state.bad_input_line[BUF_LEN - 1] = '\0';

    bad_input_state.txt.txt = bad_input_state.bad_input_line;
    bad_input_state.txt.pos = 0;
    cur_state = WASHDBG_STATE_BAD_INPUT;
}

static struct print_error_state {
    struct washdbg_txt_state txt;
} print_error_state;

#define MAX_ERROR_LEN 1024
static void washdbg_print_error(char const *error, ...) {
    static char txt_buf[MAX_ERROR_LEN];
    va_list args;
    va_start(args, error);
    vsnprintf(txt_buf, sizeof(txt_buf), error, args);
    va_end(args);

    print_error_state.txt.txt = txt_buf;
    print_error_state.txt.pos = 0;
    cur_state = WASHDBG_STATE_PRINT_ERROR;
}

static struct echo_state {
    int argc;
    char **argv;
    int cur_arg;
    unsigned cur_arg_pos;
    bool print_space;
} echo_state;

static void washdbg_echo(int argc, char **argv) {
    int arg_no;

    if (argc <= 1) {
        washdbg_print_prompt();
        return;
    }

    echo_state.cur_arg = 1;
    echo_state.cur_arg_pos = 0;
    echo_state.print_space = false;
    echo_state.argc = argc;
    echo_state.argv = (char**)calloc(sizeof(char*), argc);
    if (!echo_state.argv) {
        washdbg_print_error("Failed allocation.\n");
        goto cleanup_args;
    }

    for (arg_no = 0; arg_no < argc; arg_no++) {
        echo_state.argv[arg_no] = washdbg_strdup(argv[arg_no]);
        if (!echo_state.argv[arg_no]) {
            washdbg_print_error("Failed allocation.\n");
            goto cleanup_args;
        }
    }

    cur_state = WASHDBG_STATE_ECHO;

    return;

cleanup_args:
    for (arg_no = 0; arg_no < argc; arg_no++)
        free(echo_state.argv[arg_no]);
    free(echo_state.argv);
}

static bool washdbg_is_echo_cmd(char const *cmd) {
    return strcmp(cmd, "echo") == 0;
}

#define WASHDBG_X_STATE_STR_LEN 128

enum x_state_disas_mode {
    X_STATE_DISAS_DISABLED,
    X_STATE_DISAS_ARM7,
    X_STATE_DISAS_THUMB
};

static struct x_state {
    char str[WASHDBG_X_STATE_STR_LEN];
    size_t str_pos;

    void *dat;
    enum washdbg_byte_count byte_count;
    unsigned count;
    unsigned idx;
    enum x_state_disas_mode disas_mode;

    uint32_t next_addr;
} x_state;

static void washdbg_x_set_string(void) {
    uint32_t val32;
    uint16_t val16;
    uint8_t val8;

    if (x_state.disas_mode == X_STATE_DISAS_ARM7) {
        val32 = ((uint32_t*)x_state.dat)[x_state.idx];

        uint32_t next_next_addr;
        snprintf(x_state.str, sizeof(x_state.str), "0x%08x: %s\n",
                 x_state.next_addr,
                 washdbg_disas_single_arm7(x_state.next_addr, val32,
                                           &next_next_addr));
        x_state.next_addr = next_next_addr;
        x_state.str[sizeof(x_state.str) - 1] = '\0';
        return;
    } else if (x_state.disas_mode == X_STATE_DISAS_THUMB) {
        val16 = ((uint16_t*)x_state.dat)[x_state.idx];

        uint32_t next_next_addr;
        snprintf(x_state.str, sizeof(x_state.str), "0x%08x: %s\n",
                 x_state.next_addr,
                 washdbg_disas_single_thumb(x_state.next_addr,
                                            &next_next_addr));
        x_state.next_addr = next_next_addr;
        x_state.str[sizeof(x_state.str) - 1] = '\0';
        return;
    }
    switch (x_state.byte_count) {
    case WASHDBG_4_BYTE:
        val32 = ((uint32_t*)x_state.dat)[x_state.idx];
        snprintf(x_state.str, sizeof(x_state.str), "0x%08x: 0x%08x\n",
                 (unsigned)x_state.next_addr, (unsigned)val32);
        x_state.next_addr += 4;
        break;
    case WASHDBG_2_BYTE:
        val16 = ((uint16_t*)x_state.dat)[x_state.idx];
        snprintf(x_state.str, sizeof(x_state.str), "0x%08x: 0x%04x\n",
                 (unsigned)x_state.next_addr, (unsigned)val16);
        x_state.next_addr += 2;
        break;
    case WASHDBG_1_BYTE:
        val8 = ((uint8_t*)x_state.dat)[x_state.idx];
        snprintf(x_state.str, sizeof(x_state.str), "0x%08x: 0x%02x\n",
                 (unsigned)x_state.next_addr, (unsigned)val8);
        x_state.next_addr += 1;
        break;
    default:
        strncpy(x_state.str, "<ERROR>\n", sizeof(x_state.str));
        x_state.str[WASHDBG_X_STATE_STR_LEN - 1] = '\0';
    }
}

static void washdbg_x(int argc, char **argv) {
    unsigned addr;
    enum dbg_context_id ctx;
    char *fmt_str;

    if (argc != 2) {
        washdbg_print_error("only a single argument is supported for the x "
                            "command.\n");
        return;
    }

    fmt_str = strchr(argv[0], '/');
    if (fmt_str) {
        fmt_str++;
        printf("The format string is \"%s\"\n", fmt_str);
    }

    memset(&x_state, 0, sizeof(x_state));

    if (parse_fmt_string(fmt_str, &x_state.byte_count, &x_state.count) < 0) {
        washdbg_print_error("failed to parse x-command format string.\n");
        return;
    }

    if (eval_expression(argv[1], &ctx, &addr) != 0)
        return;

    if (x_state.byte_count == WASHDBG_INST) {
        switch (ctx) {
        case DEBUG_CONTEXT_ARM7:
            x_state.byte_count = WASHDBG_4_BYTE;
            if (capstone_avail) {
                x_state.disas_mode = X_STATE_DISAS_ARM7;
            } else {
                fprintf(stderr, "capstone_avail is false\n");
                x_state.disas_mode = X_STATE_DISAS_DISABLED;
            }
            break;
        default:
            washdbg_print_error("unknown context ???\n");
            return;
        }
    } else if (x_state.byte_count == WASHDBG_INST_THUMB) {
        switch (ctx) {
        case DEBUG_CONTEXT_ARM7:
            x_state.byte_count = WASHDBG_2_BYTE;
            if (capstone_avail) {
                x_state.disas_mode = X_STATE_DISAS_THUMB;
            } else {
                fprintf(stderr, "capstone_avail is false\n");
                x_state.disas_mode = X_STATE_DISAS_DISABLED;
            }
            break;
        default:
            washdbg_print_error("unknown context ???\n");
            return;
        }
    } else if (x_state.byte_count != 1 &&
               x_state.byte_count != 2 &&
               x_state.byte_count != 4) {
        washdbg_print_error("invalid byte count\n");
        return;
    }

    x_state.next_addr = addr;

    if (x_state.count > 1024 * 32) {
        washdbg_print_error("too much data\n");
        return;
    }

    x_state.dat = calloc(x_state.byte_count, x_state.count);
    if (!x_state.dat) {
        washdbg_print_error("failed allocation\n");
        return;
    }

    // now do the memory lookup here
    int err_code = debug_read_mem(ctx, x_state.dat, addr,
                                  x_state.byte_count * x_state.count);
    if (err_code != MEM_ACCESS_SUCCESS) {
        washdbg_print_error("%s\n", washdc_mem_code_error(err_code));
        return;
    }

    washdbg_x_set_string();
    x_state.idx = 1;

    cur_state = WASHDBG_STATE_X;
}

static unsigned washdbg_print_x(void) {
    char const *start = x_state.str + x_state.str_pos;
    size_t len = strlen(x_state.str);
    unsigned rem_chars = len - x_state.str_pos;
    if (rem_chars) {
        unsigned n_chars = washdbg_puts(start);
        if (n_chars == rem_chars)
            goto reload;
        else
            x_state.str_pos += n_chars;
    } else {
        goto reload;
    }
    return 1;

reload:
    if (x_state.idx == x_state.count)
        return 0;
    washdbg_x_set_string();
    x_state.idx++;
    return 1;
}

static bool washdbg_is_x_cmd(char const *cmd) {
    // TODO: implement formatted versions, like x/w
    return strcmp(cmd, "x") == 0 ||
        (strlen(cmd) >= 3 && cmd[0] == 'x' && cmd[1] == '/');
}

#define WASHDBG_BPSET_STR_LEN 64

static struct bpset_state {
    struct washdbg_txt_state txt;
    char str[WASHDBG_BPSET_STR_LEN];
} bpset_state;

static void washdbg_bpset(int argc, char **argv) {
    if (argc != 2) {
        washdbg_print_error("only a single argument is supported for the bpset "
                            "command.\n");
        return;
    }

    unsigned addr;
    enum dbg_context_id ctx;
    if (eval_expression(argv[1], &ctx, &addr) != 0)
        return;

    unsigned bp_idx;
    for (bp_idx = 0; bp_idx < DEBUG_N_BREAKPOINTS; bp_idx++)
        if (!washdbg_bp_stat[ctx][bp_idx].valid) {
            washdbg_bp_stat[ctx][bp_idx].addr = addr;
            washdbg_bp_stat[ctx][bp_idx].valid = true;
            washdbg_bp_stat[ctx][bp_idx].enabled = true;
            break;
        }

    if (bp_idx >= DEBUG_N_BREAKPOINTS ||
        debug_add_break(ctx, addr) != 0) {
        washdbg_print_error("failed to add breakpoint\n");
        return;
    }

    memset(bpset_state.str, 0, sizeof(bpset_state.str));

    snprintf(bpset_state.str, sizeof(bpset_state.str),
             "breakpoint %u added successfully.\n", bp_idx);

    bpset_state.txt.txt = bpset_state.str;
    bpset_state.txt.pos = 0;

    cur_state = WASHDBG_STATE_CMD_BPSET;
}

static bool washdbg_is_bpset_cmd(char const *cmd) {
    return strcmp(cmd, "bpset") == 0;
}

#define WASHDBG_BPLIST_STR_LEN 64

static struct bplist_state {
    char str[WASHDBG_BPLIST_STR_LEN];
    unsigned str_idx;
    unsigned bp_next;
    enum dbg_context_id ctx_next;
    struct washdbg_txt_state txt;
} bplist_state;

static int washdbg_bplist_load_bp(void) {
    if (bplist_state.ctx_next >= NUM_DEBUG_CONTEXTS)
        return -1;

    struct washdbg_bp_stat *chosen = NULL;
    unsigned idx_cur;
    int ctx_cur;
    for (ctx_cur = bplist_state.ctx_next, idx_cur = bplist_state.bp_next;
         ctx_cur < NUM_DEBUG_CONTEXTS; ctx_cur++) {
        for (; idx_cur < DEBUG_N_BREAKPOINTS;
             idx_cur++) {
            struct washdbg_bp_stat *bp = &washdbg_bp_stat[ctx_cur][idx_cur];
            if (bp->valid) {
                chosen = bp;

                bplist_state.bp_next = idx_cur + 1;
                bplist_state.ctx_next = (enum dbg_context_id)ctx_cur;
                if (bplist_state.bp_next >= DEBUG_N_BREAKPOINTS) {
                    bplist_state.bp_next = 0;
                    bplist_state.ctx_next =
                        (enum dbg_context_id)(1 + bplist_state.ctx_next);
                }
                goto have_chosen;
            }
        }
        idx_cur = 0;
    }

have_chosen:

    if (!chosen)
        return -1;

    memset(bplist_state.str, 0, sizeof(bplist_state.str));

    char const *ctx_name = 
        (ctx_cur == DEBUG_CONTEXT_ARM7 ? "arm7" : "unknown");
    snprintf(bplist_state.str, sizeof(bplist_state.str),
             "%s breakpoint %u: 0x%08x (%s)\n",
             ctx_name, idx_cur, (unsigned)chosen->addr,
             chosen->enabled ? "enabled" : "disabled");
    bplist_state.str[WASHDBG_BPLIST_STR_LEN - 1] = '\0';
    bplist_state.txt.pos = 0;
    return 0;
}

static void washdbg_bplist_run(void) {
    if (washdbg_print_buffer(&bplist_state.txt) != 0)
        return;
    if (washdbg_bplist_load_bp() != 0)
        washdbg_print_prompt();
}

static void washdbg_do_bplist(int argc, char **argv) {
    if (argc != 1) {
        washdbg_print_error("bplist takes no arguments!\n");
        return;
    }
    memset(&bplist_state, 0, sizeof(bplist_state));
    bplist_state.txt.txt = bplist_state.str;
    cur_state = WASHDBG_STATE_CMD_BPLIST;
}

static bool washdbg_is_bplist_cmd(char const *cmd) {
    return strcmp(cmd, "bplist") == 0;
}

static void washdbg_do_bpdis(int argc, char **argv) {
    if (argc != 2) {
        washdbg_print_error("need to provide breakpoint id\n");
        return;
    }

    enum dbg_context_id ctx;
    unsigned idx;
    if (eval_expression(argv[1], &ctx, &idx) != 0)
        return;

    if ((ctx != DEBUG_CONTEXT_ARM7) ||
        (idx >= DEBUG_N_BREAKPOINTS)) {
        washdbg_print_error("bad breakpoint idx\n");
        return;
    }

    struct washdbg_bp_stat *bp = &washdbg_bp_stat[ctx][idx];
    if (!bp->valid) {
        washdbg_print_error("breakpoint is not set\n");
        return;
    }

    if (debug_remove_break(ctx, bp->addr) != 0) {
        washdbg_print_error("failed to remove breakpoint\n");
        return;
    }
    bp->enabled = false;
    washdbg_print_prompt();
}


static bool washdbg_is_bpdis_cmd(char const *str) {
    return strcmp(str, "bpdis") == 0;
}

static void washdbg_do_bpen(int argc, char **argv) {
    if (argc != 2) {
        washdbg_print_error("need to provide breakpoint id\n");
        return;
    }

    enum dbg_context_id ctx;
    unsigned idx;
    if (eval_expression(argv[1], &ctx, &idx) != 0)
        return;

    if ((ctx != DEBUG_CONTEXT_ARM7) ||
        (idx >= DEBUG_N_BREAKPOINTS)) {
        washdbg_print_error("bad breakpoint idx\n");
        return;
    }

    struct washdbg_bp_stat *bp = &washdbg_bp_stat[ctx][idx];
    if (!bp->valid) {
        washdbg_print_error("breakpoint is not set\n");
        return;
    }

    if (debug_add_break(ctx, bp->addr) != 0) {
        washdbg_print_error("failed to re-add breakpoint\n");
        return;
    }
    bp->enabled = true;
    washdbg_print_prompt();
}

static bool washdbg_is_bpen_cmd(char const *str) {
    return strcmp(str, "bpen") == 0;
}

static void washdbg_do_bpdel(int argc, char **argv) {
    if (argc != 2) {
        washdbg_print_error("need to provide breakpoint id\n");
        return;
    }

    enum dbg_context_id ctx;
    unsigned idx;
    if (eval_expression(argv[1], &ctx, &idx) != 0)
        return;

    if ((ctx != DEBUG_CONTEXT_ARM7) ||
        (idx >= DEBUG_N_BREAKPOINTS)) {
        washdbg_print_error("bad breakpoint idx\n");
        return;
    }

    struct washdbg_bp_stat *bp = &washdbg_bp_stat[ctx][idx];
    if (!bp->valid) {
        washdbg_print_error("breakpoint is not set\n");
        return;
    }

    if (debug_remove_break(ctx, bp->addr) != 0) {
        washdbg_print_error("failed to remove breakpoint\n");
        return;
    }

    memset(bp, 0, sizeof(*bp));
    washdbg_print_prompt();
}

static bool washdbg_is_bpdel_cmd(char const *str) {
    return strcmp(str, "bpdel") == 0;
}

#define WASHDBG_PRINT_STATE_STR_LEN 128

static struct washdbg_print_state {
    char str[WASHDBG_PRINT_STATE_STR_LEN];
    struct washdbg_txt_state txt;
} print_state;

static void washdbg_print(int argc, char **argv) {
    unsigned val;
    enum dbg_context_id ctx;

    if (argc != 2) {
        washdbg_print_error("only a single argument is supported for the print "
                            "command.\n");
        return;
    }

    memset(&print_state, 0, sizeof(print_state));
    print_state.txt.txt = print_state.str;

    if (eval_expression(argv[1], &ctx, &val) != 0)
        return;

    snprintf(print_state.str, sizeof(print_state.str), "0x%08x\n", val);
    print_state.str[WASHDBG_PRINT_STATE_STR_LEN - 1] = '\0';

    cur_state = WASHDBG_STATE_CMD_PRINT;
}

static bool washdbg_is_print_cmd(char const *str) {
    return strcmp(str, "print") == 0 ||
        strcmp(str, "p") == 0;
}

static bool washdbg_is_regwatch_cmd(char const *str) {
    return strcmp(str, "regwatch") == 0;
}

static void washdbg_regwatch(int argc, char **argv) {
#ifdef ENABLE_DBG_COND
    if (argc != 4) {
        washdbg_print_error("usage: regwatch context register value\n");
        return;
    }

    enum dbg_context_id ctx;
    if (strcmp(argv[1], "arm7") == 0) {
        ctx = DEBUG_CONTEXT_ARM7;
    } else {
        washdbg_print_error("unrecognized context string.\n");
        return;
    }

    int reg_idx;
    if (ctx == DEBUG_CONTEXT_ARM7) {
        reg_idx = reg_idx_arm7(argv[2]);
    }

    if (reg_idx < 0) {
        washdbg_print_error("unrecognized register.\n");
        return;
    }

    uint32_t value;
    if (parse_int_str(argv[3], &value) != 0)
        return;

    if (!debug_reg_cond(ctx, reg_idx, value))
        washdbg_print_error("failed to insert condition\n");
    else
        washdbg_print_prompt();

#else
    washdbg_print_error("regwatch command not available; rebuild WashingtonDC "
                        "with -DENABLE_DBG_COND=On.\n");
#endif
}

static bool washdbg_is_memwatch_cmd(char const *str) {
    return strcmp(str, "memwatch") == 0;
}

static void washdbg_memwatch(int argc, char **argv) {
#ifdef ENABLE_DBG_COND
    uint32_t size, addr, val;

    if (argc != 5) {
        washdbg_print_error("usage: memwatch context size addr value\n");
        return;
    }

    enum dbg_context_id ctx;
    if (strcmp(argv[1], "arm7") == 0) {
        ctx = DEBUG_CONTEXT_ARM7;
    } else {
        washdbg_print_error("unrecognized context string.\n");
        return;
    }

    if (parse_int_str(argv[2], &size) != 0)
        return;
    if (parse_int_str(argv[3], &addr) != 0)
        return;
    if (parse_int_str(argv[4], &val) != 0)
        return;

    if (!debug_mem_cond(ctx, addr, val, size))
        washdbg_print_error("failed to insert condition\n");
    else
        washdbg_print_prompt();

#else
    washdbg_print_error("memwatch command not available; rebuild WashingtonDC "
                        "with -DENABLE_DBG_COND=On.\n");
#endif
}

#define WASHDBG_TRANS_ITLB_STR_LEN 64

static struct trans_itlb_state {
    char msg[WASHDBG_TRANS_ITLB_STR_LEN];
    struct washdbg_txt_state txt;
} trans_itlb_state;

static bool washdbg_is_trans_itlb_cmd(char const *str) {
    return strcmp(str, "trans_itlb") == 0;
}

static void washdbg_trans_itlb(int argc, char **argv) {
    washdbg_print_error("the trans_itlb command is not available; rebuild "
                        "WashingtonDC with -DENABLE_MMU=On.\n");
}

#define WASHDBG_TRANS_UTLB_STR_LEN 64

static struct trans_utlb_state {
    char msg[WASHDBG_TRANS_UTLB_STR_LEN];
    struct washdbg_txt_state txt;
} trans_utlb_state;

static bool washdbg_is_trans_utlb_cmd(char const *str) {
    return strcmp(str, "trans_utlb") == 0;
}

static void washdbg_trans_utlb(int argc, char **argv) {
    washdbg_print_error("the trans_utlb command is not available; rebuild "
                        "WashingtonDC with -DENABLE_MMU=On.\n");
}

#define WASHDBG_ASID_STR_LEN 64

static struct asid_state {
    char msg[WASHDBG_ASID_STR_LEN];
    struct washdbg_txt_state txt;
} asid_state;

static bool washdbg_is_asid_cmd(char const *str) {
    return strcmp(str, "asid") == 0;
}

static void washdbg_asid(int argc, char **argv) {
    washdbg_print_error("unable to read from PTEH\n");
}

#define WASHDBG_AT_MODE_STR_LEN 64

static struct at_mode_state {
    char msg[WASHDBG_AT_MODE_STR_LEN];
    struct washdbg_txt_state txt;
} at_mode_state;

static bool washdbg_is_at_mode_cmd(char const *str) {
    return strcmp(str, "at-mode") == 0;
}

static void washdbg_at_mode(int argc, char **argv) {
    washdbg_print_error("this command is not available in non-mmu builds.  "
                        "Reconfigure cmake with -DENABLE_MMU=On and then "
                        "rebuild WashingtonDC.\n");
}

#define WASHDBG_DUMP_MODE_STR_LEN 64

static struct dump_state {
    char msg[WASHDBG_DUMP_MODE_STR_LEN];
    struct washdbg_txt_state txt;
} dump_state;

static bool washdbg_is_dump_cmd(char const *str) {
    return strcmp(str, "dump") == 0;
}

static void washdbg_dump(int argc, char **argv) {
    enum dbg_context_id ctx_id;
    unsigned first, last;

    if (argc != 4 ||
        eval_expression(argv[1], &ctx_id, &first) != 0 ||
        eval_expression(argv[2], &ctx_id, &last) != 0 ||
        first > last) {
        washdbg_print_error("usage: %s <first> <last> <filename>\n", argv[0]);
        return;
    }

    /*
     * enforce sane limits, this is safe to raise if necessary but really it
     * should never be necessary on a gameboy advance.
     */
    unsigned n_bytes = last - first + 1;
    if (n_bytes > (1024 * 1024 * 32)) {
        washdbg_print_error("range cannot exceed 32MB\n");
        return;
    }

    char const *path = argv[3];

    washdc_log_info("washdbg: user requested dump of address range %08x - %08x "
                    "to %s\n", first, last, path);

    void *bufp = calloc(1, n_bytes);
    if (!bufp) {
        washdc_log_error("ERROR: washdbg: failed allocation of %u bytes\n",
                         n_bytes);
        washdbg_print_error("ERROR: failed allocation of %u bytes\n", n_bytes);
        return;
    }

    if (debug_read_mem(DEBUG_CONTEXT_ARM7, bufp, first, n_bytes) == 0) {
        FILE *outfile = fopen(path, "wb");
        if (outfile) {
            if (fwrite(bufp, n_bytes, 1, outfile) != 1) {
                washdbg_print_error("ERROR: failure to write to \"%s\"\n", path);
                washdc_log_error("ERROR: washdbg: failure to write to "
                                 "\"%s\"\n", path);
            } else {
                // success
                snprintf(dump_state.msg, sizeof(dump_state.msg),
                         "%u bytes dumped to \"%s\"\n",
                         n_bytes, path);
                dump_state.txt.txt = dump_state.msg;
                dump_state.txt.pos = 0;
                cur_state = WASHDBG_STATE_CMD_DUMP;
            }
            fclose(outfile);
        } else {
            washdbg_print_error("ERROR: failed to open \"%s\"\n", path);
            washdc_log_error("ERROR: washdbg: failed to open \"%s\"\n", path);
        }
    } else {
        // failure
        washdbg_print_error("ERROR: failed to read memory range from "
                            "%08x through %08x\n", first, last);
        washdc_log_error("ERROR: washdbg: failed to read memory range from "
                         "%08x through %08x\n", first, last);
    }

    free(bufp);
}

static bool washdbg_is_crash_cmd(char const *str) {
    return strcmp(str, "crash") == 0;
}

static void washdbg_crash(int argc, char **argv) {
    CRITICAL_ERROR(ERROR_DEBUG);
}

#define WASHDBG_REGS_STATE_STR_LEN 128

static struct regs_state {
    uint32_t arm7_regs[ARM7_REGISTER_COUNT];

    char cur_str[WASHDBG_REGS_STATE_STR_LEN];
    enum dbg_context_id cur_ctx;
    int cur_reg;
    // unsigned cur_char;

    struct washdbg_txt_state txt;
} regs_state;

// may return NULL if there is no name
static char const *washdbg_arm7_reg_name(int reg_idx) {
    struct name_map const *curs = arm7_reg_map;
    while (curs->str) {
        if (curs->idx == reg_idx)
            return curs->str;
        curs++;
    }
    return NULL;
}

static int washdbg_regs_fill_str_arm7(void) {
    if (regs_state.cur_ctx != DEBUG_CONTEXT_ARM7)
        CRITICAL_ERROR(ERROR_INTEGRITY);

        while (regs_state.cur_reg < ARM7_REGISTER_COUNT) {
            char const *name = washdbg_arm7_reg_name(regs_state.cur_reg);
            if (name) {
                snprintf(regs_state.cur_str, sizeof(regs_state.cur_str),
                         "arm7:$%s\t%08x\n", name,
                         (unsigned)regs_state.arm7_regs[regs_state.cur_reg]);
                regs_state.txt.pos = 0;
                regs_state.txt.txt = regs_state.cur_str;
                return 0;
            }
            regs_state.cur_reg++;
        }
        return -1;
}

static int washdbg_regs_fill_str(void) {
    if (regs_state.cur_ctx != DEBUG_CONTEXT_ARM7)
        CRITICAL_ERROR(ERROR_INTEGRITY);

    return washdbg_regs_fill_str_arm7();
}

static bool washdbg_is_regs_cmd(char const *str) {
    return strcmp(str, "regs") == 0;
}

static void washdbg_regs(int argc, char **argv) {
    if (argc != 1) {
        washdbg_print_error("usage: regs\n");
        return;
    }

    debug_get_all_regs(DEBUG_CONTEXT_ARM7, regs_state.arm7_regs,
                       sizeof(regs_state.arm7_regs));

    regs_state.cur_ctx = DEBUG_CONTEXT_ARM7;
    regs_state.cur_reg = 0;

    if (washdbg_regs_fill_str() != 0)
        CRITICAL_ERROR(ERROR_INTEGRITY); // should be impossible

    cur_state = WASHDBG_STATE_CMD_REGS;
}

/*
 * maps washdbg IRQ strings to the ones accepted by
 * washdc_gameconsole_inject_irq
 */
static struct washdbg_irqstr_translation {
    char const *washdbg_str; // this is case-insensitive
    char const *emu_str;     // this is case-sensitive
} const irqstr_translations[] = {
    { NULL, NULL }
};

#define WASHDBG_IRQSTR_MAX_LEN 64

static struct inject_irq_state {
    char msg[WASHDBG_IRQSTR_MAX_LEN];

    // state used for printing a list of the allowed irqs in the usage string
    struct washdbg_irqstr_translation const *next_irqstr;
    struct washdbg_txt_state txt;
} inject_irq_state;

static bool washdbg_is_inject_irq_cmd(char const *str) {
    return strcmp(str, "inject_irq") == 0;
}

static char const *washdbg_translate_irqstr(char const *input) {
    struct washdbg_irqstr_translation const *cursor = irqstr_translations;
    while (cursor->washdbg_str) {
        if (strlen(input) == strlen(cursor->washdbg_str)) {
            char const *cmp1 = input;
            char const *cmp2 = cursor->washdbg_str;
            while (*cmp1 && *cmp2) {
                if (toupper(*cmp1) != toupper(*cmp2))
                    break;
                cmp1++;
                cmp2++;
            }
            if (!*cmp1 && !*cmp2)
                return cursor->emu_str;
        }
        cursor++;
    }
    return NULL;
}

static void washdbg_inject_irq(int argc, char **argv) {
    char const *irqstr;
    if (argc != 2 || !(irqstr = washdbg_translate_irqstr(argv[1]))) {
        inject_irq_state.txt.pos = 0;
        inject_irq_state.txt.txt = "usage: inject_irq <irq>\nallowed irqs:\n";
        inject_irq_state.next_irqstr = irqstr_translations;
        cur_state = WASHDBG_STATE_INJECT_IRQ_USAGE_STRING;
        return;
    }
    debug_inject_irq(irqstr);
    washdbg_print_prompt();
}

void washdbg_core_run_once(void) {
    switch (cur_state) {
    case WASHDBG_STATE_BANNER:
        if (washdbg_print_buffer(&print_banner_state.txt) == 0)
            washdbg_print_context_info();
        break;
    case WASHDBG_STATE_PROMPT:
        if (washdbg_print_buffer(&print_prompt_state.txt) == 0)
            cur_state = WASHDBG_STATE_NORMAL;
        break;
    case WASHDBG_STATE_CMD_CONTINUE:
        if (washdbg_print_buffer(&continue_state.txt) == 0) {
            debug_request_continue();
            cur_state = WASHDBG_STATE_RUNNING;
        }
        break;
    case WASHDBG_STATE_NORMAL:
        washdbg_process_input();
        break;
    case WASHDBG_STATE_BAD_INPUT:
        if (washdbg_print_buffer(&bad_input_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_HELP:
        if (washdbg_print_buffer(&help_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CONTEXT_INFO:
        if (washdbg_print_buffer(&context_info_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_PRINT_ERROR:
        if (washdbg_print_buffer(&print_error_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_ECHO:
        washdbg_state_echo_process();
        break;
    case WASHDBG_STATE_X:
        if (washdbg_print_x() == 0) {
            free(x_state.dat);
            x_state.dat = NULL;
            washdbg_print_prompt();
        }
        break;
    case WASHDBG_STATE_CMD_BPSET:
        if (washdbg_print_buffer(&bpset_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_BPLIST:
        washdbg_bplist_run();
        break;
    case WASHDBG_STATE_CMD_PRINT:
        if (washdbg_print_buffer(&print_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_TRANS_ITLB:
        if (washdbg_print_buffer(&trans_itlb_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_TRANS_UTLB:
        if (washdbg_print_buffer(&trans_utlb_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_ASID:
        if (washdbg_print_buffer(&asid_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_AT_MODE:
        if (washdbg_print_buffer(&at_mode_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_DUMP:
        if (washdbg_print_buffer(&dump_state.txt) == 0)
            washdbg_print_prompt();
        break;
    case WASHDBG_STATE_CMD_REGS:
        if (washdbg_print_buffer(&regs_state.txt) == 0) {
            regs_state.cur_reg++;
            if (washdbg_regs_fill_str() != 0)
                washdbg_print_prompt();
        }
        break;
    case WASHDBG_STATE_INJECT_IRQ_USAGE_STRING:
        if (washdbg_print_buffer(&inject_irq_state.txt) == 0) {
            if (inject_irq_state.next_irqstr->washdbg_str) {
                inject_irq_state.txt.txt = inject_irq_state.msg;
                snprintf(inject_irq_state.msg, sizeof(inject_irq_state.msg),
                         "\t%s\n", inject_irq_state.next_irqstr->washdbg_str);
                inject_irq_state.msg[sizeof(inject_irq_state.msg) - 1] = '\0';
                inject_irq_state.txt.pos = 0;
                inject_irq_state.next_irqstr++;
            } else {
                washdbg_print_prompt();
            }
        }
    default:
        break;
    }
}

void washdbg_core_on_break(enum dbg_context_id id, void *argptr) {
    if (cur_state != WASHDBG_STATE_RUNNING)
        CRITICAL_ERROR(ERROR_INTEGRITY);
    washdbg_print_context_info();
}

// maximum length of a single argument
#define SINGLE_ARG_MAX 128

// maximum number of arguments
#define MAX_ARG_COUNT 256

static void washdbg_process_input(void) {
    static char cur_line[BUF_LEN];
    int argc = 0;
    char **argv = NULL;
    int arg_no;

    char const *newline_ptr = strchr(in_buf, '\n');
    if (newline_ptr) {
        unsigned newline_idx = newline_ptr - in_buf;

        memset(cur_line, 0, sizeof(cur_line));
        memcpy(cur_line, in_buf, newline_idx);

        if (newline_idx < (BUF_LEN - 1)) {
            size_t chars_to_move = BUF_LEN - newline_idx - 1;
            memmove(in_buf, newline_ptr + 1, chars_to_move);
            in_buf_pos = 0;
        }

        // Now separate the current line out into arguments
        char *token = strtok(cur_line, " \t");
        while (token) {
            if (argc + 1 > MAX_ARG_COUNT) {
                washdbg_print_error("too many arguments\n");
                goto cleanup_args;
            }

            // the + 1 is to add in space for the \0
            size_t tok_len = strlen(token) + 1;

            if (tok_len > SINGLE_ARG_MAX) {
                washdbg_print_error("argument exceeded maximum length.\n");
                goto cleanup_args;
            }

            char *new_arg = (char*)malloc(tok_len * sizeof(char));
            if (!new_arg) {
                washdbg_print_error("Failed allocation.\n");
                goto cleanup_args;
            }

            memcpy(new_arg, token, tok_len * sizeof(char));

            char **new_argv = (char**)realloc(argv, sizeof(char*) * (argc + 1));
            if (!new_argv) {
                washdbg_print_error("Failed allocation.\n");
                goto cleanup_args;
            }

            argv = new_argv;
            argv[argc] = new_arg;
            argc++;

            token = strtok(NULL, " \t");
        }

        char const *cmd;
        if (argc)
            cmd = argv[0];
        else
            cmd = "";

        if (strlen(cmd)) {
            if (washdbg_is_continue_cmd(cmd)) {
                washdbg_do_continue(argc, argv);
            } else if (washdbg_is_exit_cmd(cmd)) {
                washdbg_do_exit(argc, argv);
            } else if (washdbg_is_help_cmd(cmd)) {
                washdbg_do_help(argc, argv);
            } else if (washdbg_is_echo_cmd(cmd)) {
                washdbg_echo(argc, argv);
            } else if (washdbg_is_x_cmd(cmd)) {
                washdbg_x(argc, argv);
            } else if (washdbg_is_step_cmd(cmd)) {
                washdbg_do_step(argc, argv);
            } else if (washdbg_is_bpset_cmd(cmd)) {
                washdbg_bpset(argc, argv);
            } else if (washdbg_is_bplist_cmd(cmd)) {
                washdbg_do_bplist(argc, argv);
            } else if (washdbg_is_bpdis_cmd(cmd)) {
                washdbg_do_bpdis(argc, argv);
            } else if (washdbg_is_bpen_cmd(cmd)) {
                washdbg_do_bpen(argc, argv);
            } else if (washdbg_is_bpdel_cmd(cmd)) {
                washdbg_do_bpdel(argc, argv);
            } else if (washdbg_is_print_cmd(cmd)) {
                washdbg_print(argc, argv);
            } else if (washdbg_is_regwatch_cmd(cmd)) {
                washdbg_regwatch(argc, argv);
            } else if (washdbg_is_memwatch_cmd(cmd)) {
                washdbg_memwatch(argc, argv);
            } else if (washdbg_is_trans_itlb_cmd(cmd)) {
                washdbg_trans_itlb(argc, argv);
            } else if (washdbg_is_trans_utlb_cmd(cmd)) {
                washdbg_trans_utlb(argc, argv);
            } else if (washdbg_is_asid_cmd(cmd)) {
                washdbg_asid(argc, argv);
            } else if (washdbg_is_at_mode_cmd(cmd)) {
                washdbg_at_mode(argc, argv);
            } else if (washdbg_is_dump_cmd(cmd)) {
                washdbg_dump(argc, argv);
            } else if (washdbg_is_crash_cmd(cmd)) {
                washdbg_crash(argc, argv);
            } else if (washdbg_is_regs_cmd(cmd)) {
                washdbg_regs(argc, argv);
            } else if (washdbg_is_inject_irq_cmd(cmd)) {
                washdbg_inject_irq(argc, argv);
            } else {
                washdbg_bad_input(cmd);
            }
        } else {
            washdbg_print_prompt();
        }
    }

cleanup_args:
    for (arg_no = 0; arg_no < argc; arg_no++)
        free(argv[arg_no]);
    free(argv);
}

static int washdbg_puts(char const *txt) {
    return washdbg_tcp_puts(txt);
}

static void washdbg_state_echo_process(void) {
    if (echo_state.cur_arg >= echo_state.argc) {
        if (echo_state.print_space) {
            if (washdbg_puts("\n"))
                echo_state.print_space = false;
            else
                return;
        }
        washdbg_print_prompt();
        int arg_no;
        for (arg_no = 0; arg_no < echo_state.argc; arg_no++)
            free(echo_state.argv[arg_no]);
        free(echo_state.argv);
        memset(&echo_state, 0, sizeof(echo_state));
        return;
    }

    for (;;) {
        if (echo_state.print_space == true) {
            if (washdbg_puts(" "))
                echo_state.print_space = false;
            else
                return;
        }

        char *arg = echo_state.argv[echo_state.cur_arg];
        unsigned arg_len = strlen(arg);
        unsigned arg_pos = echo_state.cur_arg_pos;
        unsigned rem_chars = arg_len - arg_pos;

        if (rem_chars) {
            unsigned n_chars = washdbg_puts(arg + arg_pos);
            if (n_chars == rem_chars) {
                echo_state.cur_arg_pos = 0;
                echo_state.cur_arg++;
                echo_state.print_space = true;
                if (echo_state.cur_arg >= echo_state.argc)
                    return;
            } else {
                echo_state.cur_arg_pos += n_chars;
                return;
            }
        }
    }
}

static unsigned washdbg_print_buffer(struct washdbg_txt_state *state) {
    char const *start = state->txt + state->pos;
    unsigned rem_chars = strlen(state->txt) - state->pos;
    if (rem_chars) {
        unsigned n_chars = washdbg_puts(start);
        if (n_chars == rem_chars)
            return 0;
        else
            state->pos += n_chars;
    } else {
        return 0;
    }
    return strlen(state->txt) - state->pos;
}

static bool is_hex_str(char const *str) {
    if (*str == 0)
        return false; // empty string
    while (*str)
        switch (*str++) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'a':
        case 'A':
        case 'b':
        case 'B':
        case 'c':
        case 'C':
        case 'd':
        case 'D':
        case 'e':
        case 'E':
        case 'f':
        case 'F':
            break;
        default:
            return false;
        }
    return true;
}

static bool is_dec_str(char const *str) {
    if (*str == 0)
        return false; // empty string
    while (*str)
        if (!isdigit(*str++))
            return false;
    return true;
}

static unsigned parse_dec_str(char const *str) {
    size_t len = strlen(str);
    if (!len)
        return 0; // error condition; just ignore it for now
    size_t idx = len - 1;
    unsigned total = 0;
    unsigned scale = 1;
    do {
        unsigned weight = str[idx] - '0';
        total += scale * weight;
        scale *= 10;
    } while (idx--);
    return total;
}

static unsigned parse_hex_str(char const *str) {
    size_t len = strlen(str);
    if (!len)
        return 0; // error condition; just ignore it for now
    size_t idx = len - 1;
    unsigned total = 0;
    unsigned scale = 1;
    do {
        /* unsigned weight = str[idx] - '0'; */
        unsigned weight;
        if (str[idx] >= '0' && str[idx] <= '9')
            weight = str[idx] - '0';
        else if (str[idx] >= 'a' && str[idx] <= 'f')
            weight = str[idx] - 'a' + 10;
        else if (str[idx] >= 'A' && str[idx] <= 'F')
            weight = str[idx] - 'A' + 10;
        else
            weight = 0; // error condition; just ignore it for now
        total += scale * weight;
        scale *= 16;
    } while (idx--);
    printf("%s is %u\n", str, total);
    return total;
}

static int reg_idx_arm7(char const *reg_name) {
    struct name_map const *cursor = arm7_reg_map;

    while (cursor->str) {
        if (strcmp(reg_name, cursor->str) == 0)
            return cursor->idx;
        cursor++;
    }
    return -1;
}

/*
 * expression format:
 * <ctx>:0xhex_val
 * OR
 * <ctx>:dec_val
 * OR
 * <ctx>:$reg_name
 *
 * ctx can be arm7 or sh4.  If it is not provided, it defaults to the current
 * context.  If the command interprets the value as being a pointer, then ctx
 * indicates whether it points to arm7's memory space or sh4's memory space.
 *
 * If the command does not interpret the value as a pointer, then ctx only
 * matters for the $reg_name form.  However, ctx can still be speciified even
 * though it is useless.
 */
static int eval_expression(char const *expr, enum dbg_context_id *ctx_id, unsigned *out) {
    enum dbg_context_id ctx = debug_current_context();

    char const *first_colon = strchr(expr, ':');
    if (first_colon != NULL) {
        unsigned n_chars = first_colon - expr;
        if (n_chars == 4 && toupper(expr[0]) == 'A' &&
            toupper(expr[1]) == 'R' && toupper(expr[2]) == 'M' &&
            toupper(expr[3]) == '7') {
            ctx = DEBUG_CONTEXT_ARM7;
        } else {
            // unrecognized context
            washdbg_print_error("Unknown context\n");
            return -1;
        }

        expr = first_colon + 1;
    }

    *ctx_id = ctx;

    if (strlen(expr) == 0) {
        washdbg_print_error("empty expression\n");
        return -1;
    }

    if (expr[0] == '$') {
        // register
        if (ctx == DEBUG_CONTEXT_ARM7) {
            int reg_idx = reg_idx_arm7(expr + 1);
            if (reg_idx >= 0) {
                *out = debug_get_reg(DEBUG_CONTEXT_ARM7, reg_idx);
                return 0;
            }
            washdbg_print_error("unknown arm7 register\n");
            return -1;
        } else {
            washdbg_print_error("register expressions are not implemented yet\n");
            return -1;
        }
    } else if (expr[0] == '0' && toupper(expr[1]) == 'X' &&
               is_hex_str(expr + 2)) {
        // hex
        *out = parse_hex_str(expr);
        return 0;
    } else if (is_dec_str(expr)) {
        // decimal
        *out = parse_dec_str(expr);
        return 0;
    } else {
        // error
        washdbg_print_error("unknown expression class\n");
        return -1;
    }
}

static int
parse_fmt_string(char const *str, enum washdbg_byte_count *byte_count_out,
                 unsigned *count_out) {
    bool have_count = false;
    bool have_byte_count = false;
    unsigned byte_count = 4;
    unsigned count = 1;

    bool parsing_digits = false;

    char const *digit_start = NULL;

    if (!str)
        goto the_end;

    while (*str || parsing_digits) {
        if (parsing_digits) {
            if (*str < '0' || *str > '9') {
                parsing_digits = false;
                unsigned n_chars = str - digit_start + 1;
                if (n_chars >= 32)
                    return -1;
                char tmp[32];
                strncpy(tmp, digit_start, sizeof(tmp));
                tmp[31] = '\0';
                if (have_count)
                    return -1;
                have_count = true;
                count = atoi(tmp);

                continue;
            }
        } else {
            switch (*str) {
            case 'w':
                if (have_byte_count)
                    return -1;
                byte_count = WASHDBG_4_BYTE;
                have_byte_count = true;
                break;
            case 'h':
                if (have_byte_count)
                    return -1;
                byte_count = WASHDBG_2_BYTE;
                have_byte_count = true;
                break;
            case 'b':
                if (have_byte_count)
                    return -1;
                byte_count = WASHDBG_1_BYTE;
                have_byte_count = true;
                break;
            case 'i':
                if (have_byte_count)
                    return -1;
                byte_count = strcmp(debug_get_isa(debug_current_context()), WASHDBG_ISA_THUMB) ?
                    WASHDBG_INST : WASHDBG_INST_THUMB;
                have_byte_count = true;
                break;
            case 'a':
                if (have_byte_count)
                    return -1;
                byte_count = WASHDBG_INST;
                have_byte_count = true;
                break;
            case 't':
                if (have_byte_count)
                    return -1;
                byte_count = WASHDBG_INST_THUMB;
                have_byte_count = true;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                parsing_digits = true;
                digit_start = str;
                continue;
            default:
                return -1;
            }
        }
        str++;
    }

    /*
     * This limit is arbitrary, you can increase or decrease it as you'd like.
     * I just put this in there to keep things sane.
     */
    if (count >= 2048) {
        washdbg_print_error("too much data\n");
        return -1;
    }

the_end:

    *count_out = count;
    *byte_count_out = (enum washdbg_byte_count)byte_count;

    return 0;
}

#define DISAS_LINE_LEN 128

static char const *
washdbg_disas_single_arm7(uint32_t addr, uint32_t val, uint32_t *next_addr) {
    cs_insn *insn;
    static char buf[DISAS_LINE_LEN];

    size_t count = cs_disasm(capstone_handle_arm, (uint8_t*)&val,
                             sizeof(val), addr, 1, &insn);

    if (count == 1) {
        snprintf(buf, sizeof(buf), "%s %s", insn->mnemonic, insn->op_str);

        if (next_addr)
            *next_addr = addr + insn->size;
    } else {
        fprintf(stderr, "cs_disasm returned %u cs_errno is %d\n",
                (unsigned)count, (int)cs_errno(capstone_handle_arm));
        snprintf(buf, sizeof(buf), "0x%08x", (unsigned)val);

        if (next_addr)
            *next_addr = addr + 4;
    }

    if (count)
        cs_free(insn, count);

    buf[sizeof(buf) - 1] = '\0';

    return buf;
}

static char const *
washdbg_disas_single_thumb(uint32_t addr, uint32_t *next_addr) {
    cs_insn *insn;
    static char buf[DISAS_LINE_LEN];
    size_t count;
    uint16_t inst16[2];

    if (debug_read_mem(DEBUG_CONTEXT_ARM7, inst16,
		       addr, sizeof(inst16[0])) != 0)
	return NULL;

    switch (inst16[0] >> 11) {
    case 0x1e:
    case 0x1f:
    case 0x1d:
	if (debug_read_mem(DEBUG_CONTEXT_ARM7, inst16 + 1,
			   addr + 2, sizeof(inst16[1])) == 0) {
	    count = cs_disasm(capstone_handle_thumb, (uint8_t*)inst16,
			      sizeof(inst16), addr, 1, &insn);
	    if (count == 1) {
		snprintf(buf, sizeof(buf), "%s %s", insn->mnemonic, insn->op_str);

		if (next_addr)
		    *next_addr = addr + insn->size;
	    } else {
		switch (inst16[0] >> 11) {
		case 0x1e:
		    snprintf(buf, sizeof(buf), "bl_part1 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		    if (next_addr)
			*next_addr = addr + 2;
		    break;
		case 0x1f:
		    snprintf(buf, sizeof(buf), "bl_part2 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		    if (next_addr)
			*next_addr = addr + 2;
		    break;
		case 0x1d:
		    snprintf(buf, sizeof(buf), "blx_part2 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		    if (next_addr)
			*next_addr = addr + 2;
		    break;
		}
		if (count)
		    cs_free(insn, count);
		return buf;
	    }
	} else {
	    switch (inst16[0] >> 11) {
	    case 0x1e:
		snprintf(buf, sizeof(buf), "bl_part1 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		if (next_addr)
		    *next_addr = addr + 2;
		break;
	    case 0x1f:
		snprintf(buf, sizeof(buf), "bl_part2 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		if (next_addr)
		    *next_addr = addr + 2;
		break;
	    case 0x1d:
		snprintf(buf, sizeof(buf), "blx_part2 #0x%03x", inst16[0] & BIT_RANGE(0, 10));
		if (next_addr)
		    *next_addr = addr + 2;
		break;
	    }
	    if (count)
		cs_free(insn, count);
	    return buf;
	}
	break;
    default:
        count = cs_disasm(capstone_handle_thumb, (uint8_t*)inst16,
                          sizeof(inst16[0]), addr, 1, &insn);

        if (count == 1) {
            snprintf(buf, sizeof(buf), "%s %s", insn->mnemonic, insn->op_str);

            if (next_addr)
                *next_addr = addr + insn->size;
        } else {
            fprintf(stderr, "cs_disasm returned %u cs_errno is %d\n",
                    (unsigned)count, (int)cs_errno(capstone_handle_arm));
            snprintf(buf, sizeof(buf), "0x%04x", (unsigned)inst16[0]);

            if (next_addr)
                *next_addr = addr + 2;
        }

        if (count)
            cs_free(insn, count);
    }

    buf[sizeof(buf) - 1] = '\0';
    return buf;
}

#if defined(ENABLE_DBG_COND) || defined(ENABLE_MMU)
static int parse_int_str(char const *valstr, uint32_t *out) {
    if (is_dec_str(valstr)) {
        *out = parse_dec_str(valstr);
        return 0;
    } else if (strlen(valstr) > 2 && valstr[0] == '0' && valstr[1] == 'x' &&
               is_hex_str(valstr + 2)) {
        *out = parse_hex_str(valstr + 2);
        return 0;
    } else {
        fprintf(stderr, "valstr is %s\n", valstr);
        washdbg_print_error("unable to parse value.\n");
    }
    return - 1;
}
#endif
