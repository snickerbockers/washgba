/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef __MAT_H_
#define __MAT_H_

#include <string.h>

#include "SDL_opengl.h" // for GLfloat

static inline void mat_ident(GLfloat mat[16]) {
    mat[0] = 1.0f;
    mat[1] = 0.0f;
    mat[2] = 0.0f;
    mat[3] = 0.0f;

    mat[4] = 0.0f;
    mat[5] = 1.0f;
    mat[6] = 0.0f;
    mat[7] = 0.0f;

    mat[8] = 0.0f;
    mat[9] = 0.0f;
    mat[10] = 1.0f;
    mat[11] = 0.0f;

    mat[12] = 0.0f;
    mat[13] = 0.0f;
    mat[14] = 0.0f;
    mat[15] = 1.0f;
}

static inline void
mat_scale(GLfloat mat[16], GLfloat scale_x, GLfloat scale_y,
          GLfloat scale_z, GLfloat scale_w) {
    mat[0] = scale_x;
    mat[1] = 0.0f;
    mat[2] = 0.0f;
    mat[3] = 0.0f;

    mat[4] = 0.0f;
    mat[5] = scale_y;
    mat[6] = 0.0f;
    mat[7] = 0.0f;

    mat[8] = 0.0f;
    mat[9] = 0.0f;
    mat[10] = scale_z;
    mat[11] = 0.0f;

    mat[12] = 0.0f;
    mat[13] = 0.0f;
    mat[14] = 0.0f;
    mat[15] = scale_w;
}

static inline void mat_translation(GLfloat mat[16], GLfloat x, GLfloat y, GLfloat z) {
    mat[0] = 1.0f;
    mat[1] = 0.0f;
    mat[2] = 0.0f;
    mat[3] = x;

    mat[4] = 0.0f;
    mat[5] = 1.0f;
    mat[6] = 0.0f;
    mat[7] = y;

    mat[8] = 0.0f;
    mat[9] = 0.0f;
    mat[10] = 1.0f;
    mat[11] = z;

    mat[12] = 0.0f;
    mat[13] = 0.0f;
    mat[14] = 0.0f;
    mat[15] = 1.0f;
}

static inline
void mat_ortho2d(GLfloat mat[16], GLfloat const min[2], GLfloat const max[2]) {
    mat[0] = 2.0f / (max[0] - min[0]);
    mat[1] = 0.0f;
    mat[2] = 0.0f;
    mat[3] = -2.0f * min[0] / (max[0] - min[0]) - 1.0f;

    mat[4] = 0.0f;
    mat[5] = 2.0f / (max[1] - min[1]);
    mat[6] = 0.0f;
    mat[7] = -2.0f * min[1] / (max[1] - min[1]) - 1.0f;

    mat[8] = 0.0f;
    mat[9] = 0.0f;
    mat[10] = 1.0f;
    mat[11] = 0.0f;

    mat[12] = 0.0f;
    mat[13] = 0.0f;
    mat[14] = 0.0f;
    mat[15] = 1.0f;
}

static inline void
mat_mult(GLfloat dst[16], GLfloat const lhs[16], GLfloat const rhs[16]) {
    // temporary buffer in cast lhs == dst || rhs == dst
    GLfloat tmp[16];

    tmp[0] = rhs[0] * lhs[0] + rhs[1] * lhs[4] +
        rhs[2] * lhs[8] + rhs[3] * lhs[12];
    tmp[1] = rhs[0] * lhs[1] + rhs[1] * lhs[5] +
        rhs[2] * lhs[9] + rhs[3] * lhs[13];
    tmp[2] = rhs[0] * lhs[2] + rhs[1] * lhs[6] +
        rhs[2] * lhs[10] + rhs[3] * lhs[14];
    tmp[3] = rhs[0] * lhs[3] + rhs[1] * lhs[7] +
        rhs[2] * lhs[11] + rhs[3] * lhs[15];

    tmp[4] = rhs[4] * lhs[0] + rhs[5] * lhs[4] +
        rhs[6] * lhs[8] + rhs[7] * lhs[12];
    tmp[5] = rhs[4] * lhs[1] + rhs[5] * lhs[5] +
        rhs[6] * lhs[9] + rhs[7] * lhs[13];
    tmp[6] = rhs[4] * lhs[2] + rhs[5] * lhs[6] +
        rhs[6] * lhs[10] + rhs[7] * lhs[14];
    tmp[7] = rhs[4] * lhs[3] + rhs[5] * lhs[7] +
        rhs[6] * lhs[11] + rhs[7] * lhs[15];

    tmp[8] = rhs[8] * lhs[0] + rhs[9] * lhs[4] +
        rhs[10] * lhs[8] + rhs[11] * lhs[12];
    tmp[9] = rhs[8] * lhs[1] + rhs[9] * lhs[5] +
        rhs[10] * lhs[9] + rhs[11] * lhs[13];
    tmp[10] = rhs[8] * lhs[2] + rhs[9] * lhs[6] +
        rhs[10] * lhs[10] + rhs[11] * lhs[14];
    tmp[11] = rhs[8] * lhs[3] + rhs[9] * lhs[7] +
        rhs[10] * lhs[11] + rhs[11] * lhs[15];

    tmp[12] = rhs[12] * lhs[0] + rhs[13] * lhs[4] +
        rhs[14] * lhs[8] + rhs[15] * lhs[12];
    tmp[13] = rhs[12] * lhs[1] + rhs[13] * lhs[5] +
        rhs[14] * lhs[9] + rhs[15] * lhs[13];
    tmp[14] = rhs[12] * lhs[2] + rhs[13] * lhs[6] +
        rhs[14] * lhs[10] + rhs[15] * lhs[14];
    tmp[15] = rhs[12] * lhs[3] + rhs[13] * lhs[7] +
        rhs[14] * lhs[11] + rhs[15] * lhs[15];

    memcpy(dst, tmp, sizeof(GLfloat) * 16);
}

#endif
