/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stddef.h>
#include <string.h>

#include "error.h"
#include "mem_code.h"
#include "gbavram.h"

static int gbavram_try_read16(uint32_t addr, uint16_t *val, void *ctxt);
static int gbavram_try_write16(uint32_t addr, uint16_t val, void *ctxt);

static int gbavram_try_read32(uint32_t addr, uint32_t *val, void *ctxt);
static int gbavram_try_write32(uint32_t addr, uint32_t val, void *ctxt);

struct memory_interface const gbavram_memory_interface = {
    .try_read32 = gbavram_try_read32,
    .try_read16 = gbavram_try_read16,
    .try_read8 = NULL,

    .try_write32 = gbavram_try_write32,
    .try_write16 = gbavram_try_write16,
    .try_write8 = NULL
};

void gbavram_init(struct gbavram *vram) {
    memset(vram, 0, sizeof(*vram));
}

void gbavram_cleanup(struct gbavram *vram) {
}

static int gbavram_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx < GBAVRAM_LEN) {
        struct gbavram *vram = ctxt;
        *val = vram->vram[idx];
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
 }

static int gbavram_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx < GBAVRAM_LEN) {
        struct gbavram *vram = ctxt;
        vram->vram[idx] = val;
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbavram_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAVRAM_LEN - 2) {
        struct gbavram *vram = ctxt;
        memcpy(val, vram->vram + idx, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbavram_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAVRAM_LEN - 2) {
        struct gbavram *vram = ctxt;
        memcpy(vram->vram + idx, &val, sizeof(val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}
