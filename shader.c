/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stdlib.h>

#include "SDL_opengl.h"

#include "log.h"
#include "wash_glptr.h"

#include "shader.h"

// the pointers returned by these functions need to be freed by the caller
static char *get_shader_source(GLuint shader_obj) {
    GLint src_len;
    pglGetShaderiv(shader_obj, GL_SHADER_SOURCE_LENGTH, &src_len);
    char *srcp = malloc(src_len + 1);
    pglGetShaderSource(shader_obj, src_len, NULL, srcp);
    srcp[src_len] = '\0';
    return srcp;
}

static char *get_shader_info_log(GLuint shader_obj) {
    GLint log_len;
    pglGetShaderiv(shader_obj, GL_INFO_LOG_LENGTH, &log_len);
    char *srcp = malloc(log_len + 1);
    pglGetShaderInfoLog(shader_obj, log_len, NULL, srcp);
    srcp[log_len] = '\0';
    return srcp;
}

GLuint compile_shader(char const *shader_source, GLenum tp) {
    GLint shader_success;
    GLuint obj = pglCreateShader(tp);
    char const *shader_lines[] = { shader_source };

    pglShaderSource(obj, 1, shader_lines, NULL);
    pglCompileShader(obj);
    pglGetShaderiv(obj, GL_COMPILE_STATUS, &shader_success);
    if (!shader_success) {
        LOG_ERROR("failure to compile shader\n");
        char *srcp = get_shader_source(obj);
        char *info_log = get_shader_info_log(obj);
        LOG_ERROR("shader source: %s\n", srcp);
        LOG_ERROR("shader log: %s\n", info_log);
        free(srcp);
        free(info_log);
#ifndef NDEBUG
        abort();
#endif
        return 0;
    }
    return obj;
}
