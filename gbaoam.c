/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stddef.h>
#include <string.h>

#include "intmath.h"
#include "error.h"
#include "mem_code.h"
#include "gbaoam.h"

static int gbaoam_try_read32(uint32_t addr, uint32_t *val, void *ctxt);
static int gbaoam_try_write32(uint32_t addr, uint32_t val, void *ctxt);

static int gbaoam_try_read16(uint32_t addr, uint16_t *val, void *ctxt);
static int gbaoam_try_write16(uint32_t addr, uint16_t val, void *ctxt);

struct memory_interface const gbaoam_memory_interface = {
    .try_read32 = gbaoam_try_read32,
    .try_read16 = gbaoam_try_read16,
    .try_read8 = NULL,

    .try_write32 = gbaoam_try_write32,
    .try_write16 = gbaoam_try_write16,
    .try_write8 = NULL
};

void gbaoam_init(struct gbaoam *oam) {
    memset(oam, 0, sizeof(*oam));
}

void gbaoam_cleanup(struct gbaoam *oam) {
}

static int gbaoam_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx < GBAOAM_LEN) {
        struct gbaoam *oam = ctxt;
        *val = oam->mem[idx];
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(2);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaoam_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAOAM_LEN - 2) {
        struct gbaoam *oam = ctxt;
        memcpy(val, oam->mem + idx, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(4);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaoam_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx < GBAOAM_LEN) {
        struct gbaoam *oam = ctxt;
        oam->mem[idx] = val;
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(2);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaoam_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAOAM_LEN - 2) {
        struct gbaoam *oam = ctxt;
        memcpy(oam->mem + idx, &val, sizeof(val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(4);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

void gbaoam_parse(struct gbaoam *oam) {
    unsigned obj_no;
    for (obj_no = 0; obj_no < GBAOAM_OBJ_COUNT; obj_no++) {
        uint16_t *obj = oam->mem + obj_no * 4;
        LOG_DBG("object %u: %04x %04x %04x\n", obj_no,
                (unsigned)obj[0], (unsigned)obj[1], (unsigned)obj[2]);
        if ((obj[0] & BIT_RANGE(8,9)) == 0x0200)
            LOG_DBG("\tDISABLED\n");
        else
            LOG_DBG("\tENABLED\n");
        LOG_DBG("\ty-coordinate: %u\n", (unsigned)(obj[0] & 0xff));
        LOG_DBG("\tx-coordinate: %u\n", (unsigned)(obj[1] & 0xff));
        if (obj[0] & (1 << 8)) {
            LOG_DBG("\ttransform enabled\n");
            unsigned param = (obj[1] >> 9) & 0x1f;
            LOG_DBG("\t\ttransform parameter index: %u\n", param);
        } else {
            LOG_DBG("\ttransform disabled\n");
            LOG_DBG("\thorizontal flip: %s\n", (obj[1] >> 12) & 1 ? "enabled" : "disabled");
            LOG_DBG("\tvertical flip: %s\n", (obj[1] >> 13) & 1 ? "enabled" : "disabled");
        }
        char const *obj_mode;
        switch ((obj[0] >> 10) & 3) {
        case 0:
            obj_mode = "normal";
            break;
        case 1:
            obj_mode = "semi-transparent";
            break;
        case 2:
            obj_mode = "obj window";
            break;
        default:
        case 3:
            obj_mode = "prohibited";
        }
        LOG_DBG("\tobj-mode: %s\n", obj_mode);
        LOG_DBG("\tobj-mosaic: %s\n", obj[0] & (1 << 12) ? "enabled" : "disabled");
        LOG_DBG("\t%u colors\n", obj[0] & (1 << 13) ? 256 : 16);
        if (!(obj[0] & (1 << 13)))
            LOG_DBG("\t\tpalette_number: %u\n", (obj[2] >> 12) & 0xf);
        char const *obj_shape;
        unsigned n_tiles_x, n_tiles_y;
        switch ((obj[0] >> 14) & 3) {
        case 0:
            obj_shape = "square";
            switch ((obj[1] >> 14) & 3) {
            case 0:
                n_tiles_x = 1;
                n_tiles_y = 1;
                break;
            case 1:
                n_tiles_x = 2;
                n_tiles_y = 2;
                break;
            case 2:
                n_tiles_x = 4;
                n_tiles_y = 4;
                break;
            case 3:
                n_tiles_x = 8;
                n_tiles_y = 8;
                break;
            }
            break;
        case 1:
            obj_shape = "horizontal";
            switch ((obj[1] >> 14) & 3) {
            case 0:
                n_tiles_x = 2;
                n_tiles_y = 1;
                break;
            case 1:
                n_tiles_x = 4;
                n_tiles_y = 1;
                break;
            case 2:
                n_tiles_x = 4;
                n_tiles_y = 2;
                break;
            case 3:
                n_tiles_x = 8;
                n_tiles_y = 4;
                break;
            }
            break;
        case 2:
            obj_shape = "vertical";
            switch ((obj[1] >> 14) & 3) {
            case 0:
                n_tiles_x = 1;
                n_tiles_y = 2;
                break;
            case 1:
                n_tiles_x = 1;
                n_tiles_y = 4;
                break;
            case 2:
                n_tiles_x = 2;
                n_tiles_y = 4;
                break;
            case 3:
                n_tiles_x = 4;
                n_tiles_y = 8;
                break;
            }
            break;
        default:
        case 3:
            obj_shape = "prohibited";
            n_tiles_x = 0;
            n_tiles_y = 0;
        }
        LOG_DBG("\tshape: %s\n", obj_shape);
        LOG_DBG("\tdimension %ux%u tiles\n", n_tiles_x, n_tiles_y);

        /*
         * tile indices are 10-bits, because in 16-color mode each
         * palette index is 4 bits.  In 256-color mode each palette
         * index is 8 bits and there are only 512 tiles; in this case
         * the lower-bit of the tile number will be ignored and should
         * be zero.
         *
         * it is both possible and valid to use 16-color objects and 256-color
         * objects at the same time.
         */
        LOG_DBG("\ttile number: %u\n", obj[2] & BIT_RANGE(0, 9));
        LOG_DBG("\tpriority: %u\n", (obj[2] >> 10) & 3);
    }
}
