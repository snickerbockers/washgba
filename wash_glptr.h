/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef WASH_GLPTR_H_
#define WASH_GLPTR_H_

#include "SDL_opengl.h"
#include "SDL_opengl_glext.h"

extern PFNGLCREATESHADERPROC pglCreateShader;
extern PFNGLDELETESHADERPROC pglDeleteShader;
extern PFNGLSHADERSOURCEPROC pglShaderSource;
extern PFNGLCOMPILESHADERPROC pglCompileShader;
extern PFNGLGETSHADERIVPROC pglGetShaderiv;
extern PFNGLGETSHADERSOURCEPROC pglGetShaderSource;
extern PFNGLGETSHADERSOURCEPROC pglGetShaderInfoLog;
extern PFNGLCREATEPROGRAMPROC pglCreateProgram;
extern PFNGLATTACHSHADERPROC pglAttachShader;
extern PFNGLLINKPROGRAMPROC pglLinkProgram;
extern PFNGLGETPROGRAMIVPROC pglGetProgramiv;
extern PFNGLGETUNIFORMLOCATIONPROC pglGetUniformLocation;
extern PFNGLGETATTRIBLOCATIONPROC pglGetAttribLocation;
extern PFNGLDELETEPROGRAMPROC pglDeleteProgram;
extern PFNGLGENBUFFERSPROC pglGenBuffers;
extern PFNGLBINDBUFFERPROC pglBindBuffer;
extern PFNGLBUFFERDATAPROC pglBufferData;
extern PFNGLDELETEBUFFERSPROC pglDeleteBuffers;
extern PFNGLVERTEXATTRIBPOINTERPROC pglVertexAttribPointer;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC pglEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC pglDisableVertexAttribArray;
extern PFNGLGENVERTEXARRAYSPROC pglGenVertexArrays;
extern PFNGLBINDVERTEXARRAYPROC pglBindVertexArray;
extern PFNGLDELETEVERTEXARRAYSPROC pglDeleteVertexArrays;
extern PFNGLUSEPROGRAMPROC pglUseProgram;
extern PFNGLACTIVETEXTUREPROC pglActiveTexture;
extern PFNGLUNIFORM1IPROC pglUniform1i;
extern PFNGLUNIFORMMATRIX4FVPROC pglUniformMatrix4fv;

typedef void(APIENTRY *PTR_GLCLEARCOLOR)(GLclampf, GLclampf, GLclampf, GLclampf);
typedef void(APIENTRY *PTR_GLCLEAR)(GLbitfield);
typedef void(APIENTRY *PTR_GLGENTEXTURES)(GLsizei, GLuint*);
typedef void(APIENTRY *PTR_GLBINDTEXTURE)(GLenum, GLuint);
typedef void(APIENTRY *PTR_GLTEXIMAGE2D)(GLenum, GLint, GLint, GLsizei, GLsizei,
                                GLint, GLenum, GLenum, const GLvoid*);
typedef void(APIENTRY *PTR_GLDELETETEXTURES)(GLsizei, const GLuint*);
typedef void(APIENTRY *PTR_GLTEXPARAMETERI)(GLenum, GLenum, GLint);
typedef void(APIENTRY *PTR_GLDRAWELEMENTS)(GLenum, GLsizei, GLenum, const GLvoid*);
typedef void(APIENTRY *PTR_GLPOLYGONMODE)(GLenum, GLenum);

extern PTR_GLCLEAR pglClear;
extern PTR_GLCLEARCOLOR pglClearColor;
extern PTR_GLGENTEXTURES pglGenTextures;
extern PTR_GLBINDTEXTURE pglBindTexture;
extern PTR_GLTEXIMAGE2D pglTexImage2D;
extern PTR_GLDELETETEXTURES pglDeleteTextures;
extern PTR_GLTEXPARAMETERI pglTexParameteri;
extern PTR_GLDRAWELEMENTS pglDrawElements;
extern PTR_GLPOLYGONMODE pglPolygonMode;

void wash_load_gl(void);

#endif
