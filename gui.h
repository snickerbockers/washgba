/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GUI_H_
#define GUI_H_

#include "SDL_opengl.h"

struct gui_font {
    GLint tex;
};

struct mesh {
    GLuint vbo, vao, ebo;
};

struct shader {
    GLuint vs, fs, prog;
    GLuint bound_tex_slot, trans_mat_slot, tex_mat_slot, color_mat_slot;
    GLuint vert_pos_slot, vert_tex_coord_slot, vert_color_slot;
};

struct gui {
    struct gui_font font;

    float width, height;

    struct shader tex_shader, untex_shader;

    struct mesh tex_mesh, untex_mesh;
};

void gui_init(struct gui *gui, float width, float height);
void gui_cleanup(struct gui *gui);

void
gui_draw_quad(struct gui *gui, GLuint tex, GLfloat const pos[2],
              GLfloat const sz[2], GLfloat const uv_pos[2],
              GLfloat const uv_sz[2]);
void
gui_draw_untextured_quad(struct gui *gui,
                         GLfloat const pos[2], GLfloat const sz[2],
                         GLfloat const color[4]);
void
gui_draw_string(struct gui *gui, char const *txt,
                GLfloat x_pos, GLfloat y_pos, GLfloat height);

#endif
