/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "mem_code.h"

#include "gbafw.h"

static int gbafw_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    struct gbafw *fw = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBAFW_ROM_LEN) {
        memcpy(val, fw->rom + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbafw_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    struct gbafw *fw = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBAFW_ROM_LEN) {
        memcpy(val, fw->rom + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbafw_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    struct gbafw *fw = ctxt;

    if (addr < GBAFW_ROM_LEN) {
        *val = fw->rom[addr];
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbafw_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    return MEM_ACCESS_FAILURE;
}

static int gbafw_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    return MEM_ACCESS_FAILURE;
}

static int gbafw_try_write8(uint32_t addr, uint8_t val, void *ctxt) {
    return MEM_ACCESS_FAILURE;
}

struct memory_interface const gbafw_memory_interface = {
    .try_read32 = gbafw_try_read32,
    .try_read16 = gbafw_try_read16,
    .try_read8 = gbafw_try_read8,

    .try_write32 = gbafw_try_write32,
    .try_write16 = gbafw_try_write16,
    .try_write8 = gbafw_try_write8
};

int gbafw_init_from_file(struct gbafw *fw, FILE *romfile) {
    if (fread(fw->rom, 1, sizeof(fw->rom), romfile) == sizeof(fw->rom))
        return 0;
    else
        return -1;
}

void gbafw_cleanup(struct gbafw *fw) {
}
