/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef PALRAM_H_
#define PALRAM_H_

#include <stdint.h>

#include "memory_map.h"

#define PALRAM_FIRST 0x05000000
#define PALRAM_LAST  0x050003ff

#define PALRAM_LEN ((PALRAM_LAST - PALRAM_FIRST + 1) / 2)

struct palram {
    uint16_t mem[PALRAM_LEN];
};

void palram_init(struct palram *pal);
void palram_cleanup(struct palram *pal);

extern struct memory_interface const palram_memory_interface;

#endif
