/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "log.h"
#include "wash_glptr.h"
#include "mat.h"
#include "gbaoam.h"
#include "intmath.h"
#include "shader.h"

#include "renderer.h"

static char const *vert_shader_src =
    "#version 330\n"
    "\n"
    "in vec4 vert_pos;\n"
    "in vec2 texcoord;\n"
    "uniform mat4 trans_mat;\n"
    "uniform mat4 tex_mat;\n"
    "\n"
    "out vec2 uv;\n"
    "\n"
    "void main() {\n"
    "    gl_Position = trans_mat * vert_pos;\n"
    "    uv = (tex_mat * vec4(texcoord, 0, 1)).xy;\n"
    "}\n";

static char const *frag_shader_src =
    "#version 330\n"
    "\n"
    "uniform sampler2D bound_tex;\n"
    "\n"
    "in vec2 uv;\n"
    "out vec4 color;\n"
    "void main() { color = texture(bound_tex, uv); if (color.a == 0) discard; }\n";

void renderer_init(struct renderer *rend) {
    memset(rend, 0, sizeof(*rend));

    rend->vs = compile_shader(vert_shader_src, GL_VERTEX_SHADER);
    rend->fs = compile_shader(frag_shader_src, GL_FRAGMENT_SHADER);

    rend->prog = pglCreateProgram();
    pglAttachShader(rend->prog, rend->vs);
    pglAttachShader(rend->prog, rend->fs);
    pglLinkProgram(rend->prog);

    GLint link_status;
    pglGetProgramiv(rend->prog, GL_LINK_STATUS, &link_status);
    if (!link_status)
        LOG_ERROR("unable to link GUI shader program\n");

    rend->vert_pos_slot = pglGetAttribLocation(rend->prog, "vert_pos");
    rend->vert_tex_coord_slot = pglGetAttribLocation(rend->prog, "texcoord");
    rend->bound_tex_slot = pglGetUniformLocation(rend->prog, "bound_tex");
    rend->trans_mat_slot = pglGetUniformLocation(rend->prog, "trans_mat");
    rend->tex_mat_slot = pglGetUniformLocation(rend->prog, "tex_mat");

#define N_INDICES 6
#define N_VERTS 4
#define VERT_LEN 10
#define VERT_POS_OFFS 0
#define VERT_UV_OFFS 4
#define VERT_COLOR_OFFS 6

    GLfloat quad_verts[] = {
        0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f
    };

    GLuint quad_indices[] = { 0, 1, 2, 0, 2, 3 };

    // create quad mesh
    pglGenVertexArrays(1, &rend->quad_array);
    pglGenBuffers(1, &rend->quad_buf);
    pglBindVertexArray(rend->quad_array);
    pglBindBuffer(GL_ARRAY_BUFFER, rend->quad_buf);
    pglBufferData(GL_ARRAY_BUFFER, VERT_LEN * sizeof(GLfloat) * N_VERTS,
                  quad_verts, GL_STATIC_DRAW);

    pglEnableVertexAttribArray(rend->vert_pos_slot);
    pglEnableVertexAttribArray(rend->vert_tex_coord_slot);
    pglVertexAttribPointer(rend->vert_pos_slot, 4, GL_FLOAT, GL_FALSE,
                           sizeof(GLfloat) * VERT_LEN,
                           (GLvoid*)(VERT_POS_OFFS * sizeof(GLfloat)));
    pglVertexAttribPointer(rend->vert_tex_coord_slot, 2, GL_FLOAT, GL_FALSE,
                           sizeof(GLfloat) * VERT_LEN,
                           (GLvoid*)(VERT_UV_OFFS * sizeof(GLfloat)));
    /* pglVertexAttribPointer(rend->vert_color_slot, 4, GL_FLOAT, GL_FALSE, */
    /*                        sizeof(GLfloat) * VERT_LEN, */
    /*                        (GLvoid*)(VERT_COLOR_OFFS * sizeof(GLfloat))); */

    pglGenBuffers(1, &rend->quad_elems);
    pglBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rend->quad_elems);
    pglBufferData(GL_ELEMENT_ARRAY_BUFFER, N_INDICES * sizeof(GLuint),
                  quad_indices, GL_STATIC_DRAW);
}

void renderer_cleanup(struct renderer *rend) {
    // delete quad mesh
    pglDeleteBuffers(1, &rend->quad_buf);
    pglDeleteVertexArrays(1, &rend->quad_array);

    pglDeleteProgram(rend->prog);
    pglDeleteShader(rend->fs);
    pglDeleteShader(rend->vs);
}

#define MAX_TILES 512

static void draw_bg_layer(struct renderer *rend, struct gba *gba,
                          unsigned layer, uint16_t const obj_pal[256],
                          GLfloat const proj[16]) {
    uint32_t dispcnt = gbavid_get_dispcnt(&gba->vid);

    if (!(dispcnt & (1 << layer + 8)))
        return; // layer disabled

    unsigned bgmode = dispcnt & BIT_RANGE(0, 2);
    if (bgmode != 4) {
        LOG_ERROR("ERROR: background mode %u unimplemented\n", bgmode);
        return;
    }

    LOG_INFO("drawing bg layer %u\n", layer);

    // bgmode 4 - 240x160 pixels, double buffers, 8-bit pallete
    unsigned bufno = dispcnt & (1 << 4) ? 1 : 0;
    LOG_INFO("bufno is %u\n", bufno);
    char const *framep = (char const*)(gba->vram.vram + (bufno * 0xa000) / 2);
    unsigned frame_w = 240, frame_h = 160;
    unsigned row, col;
    unsigned char *rawdat = malloc(4 * frame_w * frame_h);
    if (!rawdat) {
        LOG_ERROR("cannot draw background layer - failed alloc\n");
        return;
    }

    for (row = 0; row < frame_h; row++)
        for (col = 0; col < frame_w; col++) {
            unsigned idx = framep[row * frame_w + col];
            uint16_t pix16 = obj_pal[idx];

            unsigned red = (pix16 & 0x1f) << 3;
            unsigned green = ((pix16 >> 5) & 0x1f) << 3;
            unsigned blue = ((pix16 >> 10) & 0x1f) << 3;

            // flip it vertically
            unsigned pixno = row * frame_w + col;
            rawdat[pixno * 4] = red;
            rawdat[pixno * 4 + 1] = green;
            rawdat[pixno * 4 + 2] = blue;
            rawdat[pixno * 4 + 3] = /* idx == 0 ? 0 :  */255;
        }

    GLuint bgtex;
    pglGenTextures(1, &bgtex);
    pglBindTexture(GL_TEXTURE_2D, bgtex);

    pglTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frame_w, frame_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, rawdat);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    GLfloat trans_mat[16] = {
        2.0f, 0.0f, 0.0f, -1.0f,
        0.0f, 2.0f, 0.0f, -1.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };
    GLfloat tex_mat[16];
    mat_ident(tex_mat);

    pglUniformMatrix4fv(rend->trans_mat_slot, 1, GL_TRUE, trans_mat);
    /* pglUniformMatrix4fv(rend->tex_mat_slot, 1, GL_TRUE, tex_mat); */
    pglDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    free(rawdat);
    pglDeleteTextures(1, &bgtex);
}

static void upload_obj_tiles(GLuint tile_tex[MAX_TILES],
                             uint16_t const obj_pal[256],
                             unsigned char const *obj_tiles) {
    pglGenTextures(MAX_TILES, tile_tex);
    unsigned tilno;
    for (tilno = 0; tilno < MAX_TILES; tilno++) {
        unsigned char const *tile = obj_tiles + 8 * 8 * tilno;

        unsigned char pix[8 * 8 * 4];
        for (unsigned row = 0; row < 8; row++) {
            for (unsigned col = 0; col < 8; col++) {
                unsigned idx = tile[row * 8 + col];
                uint16_t pix16 = obj_pal[idx];

                unsigned red = (pix16 & 0x1f) << 3;
                unsigned green = ((pix16 >> 5) & 0x1f) << 3;
                unsigned blue = ((pix16 >> 10) & 0x1f) << 3;

                // flip it vertically
                unsigned pixno = (7 - row) * 8 + col;
                pix[pixno * 4] = red;
                pix[pixno * 4 + 1] = green;
                pix[pixno * 4 + 2] = blue;
                pix[pixno * 4 + 3] = idx == 0 ? 0 : 255;
            }
        }

        pglBindTexture(GL_TEXTURE_2D, tile_tex[tilno]);
        pglTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, pix);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }
}

static void draw_obj(struct renderer *rend, GLuint const *tile_tex,
                     struct gbaoam *oam, uint16_t const *trans_params,
                     GLfloat const proj[16], unsigned obj_no) {
    uint16_t *obj = oam->mem + obj_no * 4;

    GLfloat rotscale[16];
    mat_ident(rotscale);

    if (!(obj[0] || obj[1] || obj[2]))
        return; // TODO: THIS IS A HACK, DELETE THIS!!!

    if ((obj[0] & BIT_RANGE(8,9)) == 0x0200)
        return; // object is disabled

    if (!(obj[0] & (1 << 13))) {
        LOG_ERROR("TODO: 16-color mode\n");
        return;
    }

    if (obj[0] & (1 << 8)) {
        // TODO: this is sorta-working but not really
        unsigned param_idx = (obj[1] >> 9) & 0x1f;
        uint16_t const *paramp = trans_params + param_idx * 4;

        double param[4] = {
            (paramp[0] & BIT_RANGE(0, 14)) / (double)(1 << 8),
            (paramp[1] & BIT_RANGE(0, 14)) / (double)(1 << 8),
            (paramp[2] & BIT_RANGE(0, 14)) / (double)(1 << 8),
            (paramp[3] & BIT_RANGE(0, 14)) / (double)(1 << 8)
        };

        if (paramp[0] & (1 << 15))
            param[0] = -param[0];
        if (paramp[1] & (1 << 15))
            param[1] = -param[1];
        if (paramp[2] & (1 << 15))
            param[2] = -param[2];
        if (paramp[3] & (1 << 15))
            param[3] = -param[3];

        rotscale[0] = param[0];
        rotscale[1] = param[1];
        rotscale[4] = param[2];
        rotscale[5] = param[3];
    }

    unsigned coord[2] = { obj[1] & 0xff, obj[0] & 0xff };

    unsigned n_tiles_x, n_tiles_y;
    switch ((obj[0] >> 14) & 3) {
    case 0:
        // square object
        switch ((obj[1] >> 14) & 3) {
        case 0:
            n_tiles_x = 1;
            n_tiles_y = 1;
            break;
        case 1:
            n_tiles_x = 2;
            n_tiles_y = 2;
            break;
        case 2:
            n_tiles_x = 4;
            n_tiles_y = 4;
            break;
        case 3:
            n_tiles_x = 8;
            n_tiles_y = 8;
            break;
        }
        break;
    case 1:
        // horizontal object
        switch ((obj[1] >> 14) & 3) {
        case 0:
            n_tiles_x = 2;
            n_tiles_y = 1;
            break;
        case 1:
            n_tiles_x = 4;
            n_tiles_y = 1;
            break;
        case 2:
            n_tiles_x = 4;
            n_tiles_y = 2;
            break;
        case 3:
            n_tiles_x = 8;
            n_tiles_y = 4;
            break;
        }
        break;
    case 2:
        // vertical object
        switch ((obj[1] >> 14) & 3) {
        case 0:
            n_tiles_x = 1;
            n_tiles_y = 2;
            break;
        case 1:
            n_tiles_x = 1;
            n_tiles_y = 4;
            break;
        case 2:
            n_tiles_x = 2;
            n_tiles_y = 4;
            break;
        case 3:
            n_tiles_x = 4;
            n_tiles_y = 8;
            break;
        }
        break;
    default:
    case 3:
        // invalid/prohibitied object
        return;
    }

    unsigned tilno = (obj[2] & BIT_RANGE(0, 9)) >> 1;

    unsigned tile_x, tile_y;
    for (tile_y = 0; tile_y < n_tiles_y; tile_y++) {
        for (tile_x = 0; tile_x < n_tiles_x; tile_x++) {
            GLfloat sz_mat[16];
            unsigned tile = tilno + tile_y * 16 + tile_x;
            if (tile < MAX_TILES) {
                pglBindTexture(GL_TEXTURE_2D, tile_tex[tile]);
            } else {
                LOG_ERROR("INVALID TILE INDEX %u, %u\n", tilno, tile);
            }

            GLfloat trans_mat[16];
            mat_translation(trans_mat, coord[0] + 8 * tile_x, coord[1] + 8 * tile_y, 0.0f);
            mat_scale(sz_mat, 8, 8, 1.0f, 1.0f);

            mat_mult(trans_mat, sz_mat, trans_mat);
            mat_mult(trans_mat, trans_mat, proj);
            mat_mult(trans_mat, trans_mat, rotscale);

            pglUniformMatrix4fv(rend->trans_mat_slot, 1, GL_TRUE, trans_mat);
            pglDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        }
    }
}

void renderer_draw_frame(struct renderer *rend,
                         struct gba *gba, GLfloat const proj[16]) {
    GLfloat tex_mat[16];
    struct gbaoam *oam = &gba->oam;
    uint32_t dispcnt = gbavid_get_dispcnt(&gba->vid);

    pglActiveTexture(GL_TEXTURE0);

    pglUseProgram(rend->prog);
    pglBindVertexArray(rend->quad_array);
    pglUniform1i(rend->bound_tex_slot, 0);

    mat_ident(tex_mat);
    pglUniformMatrix4fv(rend->tex_mat_slot, 1, GL_TRUE, tex_mat);

    uint16_t obj_pal[256];
    memcpy(obj_pal, gba->pal.mem + 256, sizeof(obj_pal));

    uint16_t bg_pal[256];
    memcpy(bg_pal, gba->pal.mem, sizeof(bg_pal));

    // draw all background layers
    unsigned bgno;
    for (bgno = 0; bgno < 4; bgno++)
        draw_bg_layer(rend, gba, bgno, bg_pal, proj);

    // draw all tiles (if enabled)
    if (dispcnt & (1 << 12)) {
        unsigned char *obj_tiles = malloc(0x8000);
        if (!obj_tiles) {
            LOG_ERROR("%s - failure to allocate object buffer\n", __func__);
            return;
        }

        memcpy(obj_tiles, gba->vram.vram + 0x10000 / 2, 0x8000);

        GLuint *tile_tex = malloc(sizeof(GLuint) * MAX_TILES);
        if (!tile_tex) {
            LOG_ERROR("%s - failure to allocate tile buffer\n", __func__);
            free(obj_tiles);
            return;
        }

        upload_obj_tiles(tile_tex, obj_pal, obj_tiles);

        unsigned obj_no;
        uint16_t trans_params[32 * 4];
        uint16_t *dstp = trans_params;
        for (obj_no = 0; obj_no < GBAOAM_OBJ_COUNT; obj_no++)
            *dstp++ = oam->mem[obj_no * 4 + 3];

        for (obj_no = 0; obj_no < GBAOAM_OBJ_COUNT; obj_no++)
            draw_obj(rend, tile_tex, oam, trans_params, proj, obj_no);

        pglDeleteTextures(MAX_TILES, tile_tex);
        free(tile_tex);
    }
    pglBindVertexArray(0);
}
