/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include "error.h"
#include "intmath.h"
#include "memory_map.h"

#include "thumb.h"

static DEF_ERROR_U32_ATTR(thumb_instruction)

#define MASK_THUMB_MOV_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_MOV_IMM8 (1 << 13)

#define MASK_THUMB_LDR_PC_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_LDR_PC_IMM8 (9 << 11)

#define MASK_THUMB_STR_RD_IND_RN_RM BIT_RANGE(9, 15)
#define VAL_THUMB_STR_RD_IND_RN_RM 0x5000

#define MASK_THUMB_ADD_REG_REG_IMM3 BIT_RANGE(9, 15)
#define VAL_THUMB_ADD_REG_REG_IMM3 0x1c00

#define MASK_THUMB_B_IMM8 BIT_RANGE(12, 15)
#define VAL_THUMB_B_IMM8 0xd000

#define MASK_THUMB_B_IMM11 BIT_RANGE(11, 15)
#define VAL_THUMB_B_IMM11 0xe000

#define MASK_THUMB_BX_REG BIT_RANGE(7, 15)
#define VAL_THUMB_BX_REG 0x4700

#define MASK_THUMB_PUSH BIT_RANGE(9, 15)
#define VAL_THUMB_PUSH 0xb400

#define MASK_THUMB_SUB_SP_IMM7 BIT_RANGE(7, 15)
#define VAL_THUMB_SUB_SP_IMM7 0xb080

#define MASK_THUMB_STR_REG_IND_SP_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_STR_REG_IND_SP_IMM8 0x9000

#define MASK_THUMB_MVN BIT_RANGE(6, 15)
#define VAL_THUMB_MVN 0x43c0

#define MASK_THUMB_PRE_LONG_BRANCH BIT_RANGE(11, 15)
#define VAL_THUMB_PRE_LONG_BRANCH 0xf000

#define MASK_THUMB_BL_LONG BIT_RANGE(11, 15)
#define VAL_THUMB_BL_LONG 0xf800

#define MASK_THUMB_LSL_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_LSL_IMM5 0

#define MASK_THUMB_STRH_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_STRH_IMM5 0x8000

#define MASK_THUMB_TST BIT_RANGE(6, 15)
#define VAL_THUMB_TST 0x4200

#define MASK_THUMB_LSR_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_LSR_IMM5 0x0800

#define MASK_THUMB_ADD_REG_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_ADD_REG_REG_REG 0x1800

#define MASK_THUMB_MOV_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_MOV_REG_REG 0x1c00

#define MASK_THUMB_MOV_HIGH_HIGH BIT_RANGE(8, 15)
#define VAL_THUMB_MOV_HIGH_HIGH 0x4600

#define MASK_THUMB_ORR BIT_RANGE(6, 15)
#define VAL_THUMB_ORR 0x4300

#define MASK_THUMB_SUB_REG_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_SUB_REG_IMM8 0x3800

#define MASK_THUMB_STRB_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_STRB_IMM5 0x7000

// sub <Rd>, <Rn>, #immed_3
#define MASK_THUMB_SUB_REG_REG_IMM3 BIT_RANGE(9, 15)
#define VAL_THUMB_SUB_REG_REG_IMM3 0x1e00

// str <Rd>, [<Rn>, #immed_5 * 4]
#define MASK_THUMB_STR_IND_RN_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_STR_IND_RN_IMM5 0x6000

// ADD <Rd>, #immed_8
#define MASK_THUMB_ADD_REG_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_ADD_REG_IMM8 0x3000

// ldrh <Rd>, [<Rn>, #immed_5 * 2]
#define MASK_THUMB_LDRH_REG_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_LDRH_REG_IMM5 0x8800

// add r13, immed_7 * 4
#define MASK_THUMB_ADD_R13_IMM7 BIT_RANGE(7, 15)
#define VAL_THUMB_ADD_R13_IMM7 0xb000

// pop {registers}
#define MASK_THUMB_POP BIT_RANGE(9, 15)
#define VAL_THUMB_POP 0xbc00

// cmp <Rn>, #immed_8
#define MASK_THUMB_CMP_REG_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_CMP_REG_IMM8 0x2800

/*
 * cmp <Rn>, <Rm> (where Rn and Rm can optionally reference the 8
 * general-purpose registers that are usually not available in thumb mode)
 */
#define MASK_THUMB_CMP_HIGH_HIGH BIT_RANGE(8, 15)
#define VAL_THUMB_CMP_HIGH_HIGH 0x4500

// cmp <Rn>, <Rm>
#define MASK_THUMB_CMP_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_CMP_REG_REG 0x4280

// ldmia <Rn>!, <register_list>
#define MASK_THUMB_LDMIA_RN_REGS BIT_RANGE(11, 15)
#define VAL_THUMB_LDMIA_RN_REGS 0xc800

// add <Rd>, PC, #imm8 * 4
#define MASK_THUMB_ADD_REG_PC_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_ADD_REG_PC_IMM8 0xa000

// add <Rd>, SP, #imm8 * 4
#define MASK_THUMB_ADD_REG_R13_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_ADD_REG_R13_IMM8 0xa800

// stmia <Rn>!, <register_list>
#define MASK_THUMB_STMIA_REG_REGS BIT_RANGE(11, 15)
#define VAL_THUMB_STMIA_REG_REGS 0xc000

// ldrb <Rd>, [<Rn>, #imm5]
#define MASK_THUMB_LDRB_RD_IND_RN_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_LDRB_RD_IND_RN_IMM5 0x7800

// ldrh <Rd>, [<Rn>, <Rm>]
#define MASK_LDRH_REG_IND_REG_REG BIT_RANGE(9, 15)
#define VAL_LDRH_REG_IND_REG_REG 0x5a00

// strh <Rd>, [<Rn>, <Rm>]
#define MASK_STRH_RD_IND_RN_RM BIT_RANGE(9, 15)
#define VAL_STRH_RD_IND_RN_RM 0x5200

// ror <Rd>, <Rs>
#define MASK_THUMB_ROR_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_ROR_REG_REG 0x41c0

// eor <Rd>, <Rm>
#define MASK_THUMB_EOR_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_EOR_REG_REG 0x4040

// mul <Rd>, <Rm>
#define MASK_THUMB_MUL_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_MUL_REG_REG 0x4340

// asr <Rd>, <Rm>, #imm5
#define MASK_THUMB_ASR_REG_REG_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_ASR_REG_REG_IMM5 0x1000

// ldr <Rd>, [<R13>, #imm8 * 4]
#define MASK_THUMB_LDR_REG_IND_R13_IMM8 BIT_RANGE(11, 15)
#define VAL_THUMB_LDR_REG_IND_R13_IMM8 0x9800

// neg <Rd>, <Rm>
#define MASK_THUMB_NEG_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_NEG_REG_REG 0x4240

// and <Rd>, <Rm>
#define MASK_THUMB_AND_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_AND_REG_REG 0x4000

// ldr <Rd>, [<Rn>, #imm5 * 4]
#define MASK_THUMB_LDR_REG_IND_REG_IMM5 BIT_RANGE(11, 15)
#define VAL_THUMB_LDR_REG_IND_REG_IMM5 0x6800

// sub <Rd>, <Rn>, <Rm>
#define MASK_THUMB_SUB_REG_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_SUB_REG_REG_REG 0x1a00

// ldr <Rd>, [<Rn>, <Rm>]
#define MASK_THUMB_LDR_REG_IND_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_LDR_REG_IND_REG_REG 0x5800

// cmn <Rn>, <Rm>
#define MASK_THUMB_CMN_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_CMN_REG_REG 0x42c0

// bic <Rd>, <Rm>
#define MASK_THUMB_BIC_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_BIC_REG_REG 0x4380

// ldrsb <Rd>, [<Rn>, <Rm>]
#define MASK_THUMB_LDSRB_REG_IND_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_LDSRB_REG_IND_REG_REG 0x5600

// ldrb <Rd>, [<Rn>, <Rm>]
#define MASK_THUMB_LDRB_REG_IND_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_LDRB_REG_IND_REG_REG 0x5c00

// lsr <Rd>, <Rs>
#define MASK_THUMB_LSR_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_LSR_REG_REG 0x40c0

// add <Rd>, <Rm> (both registers are high)
#define MASK_THUMB_ADD_HIGH_HIGH BIT_RANGE(8, 15)
#define VAL_THUMB_ADD_HIGH_HIGH 0x4400

// lsl <Rd>, <Rs>
#define MASK_THUMB_LSL_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_LSL_REG_REG 0x4080

// ldrsh <Rd>, [<Rn>, <Rm>]
#define MASK_THUMB_LDRSH_REG_IND_REG_REG BIT_RANGE(9, 15)
#define VAL_THUMB_LDRSH_REG_IND_REG_REG 0x5e00

// SWI $immed_8
#define MASK_THUMB_SWI_IMM8 BIT_RANGE(8, 15)
#define VAL_THUMB_SWI_IMM8 0xdf00

// ASR <Rd>, <Rs>
#define MASK_THUMB_ASR_REG_REG BIT_RANGE(6, 15)
#define VAL_THUMB_ASR_REG_REG 0x4100

static inline uint32_t thumb_read32(struct arm7 *arm7, uint32_t addr);

// decode a thumb instruction and return an equivalent ARM instruction
static uint32_t arm7_translate_thumb(uint_fast16_t input) {
    if ((input & MASK_THUMB_MOV_IMM8) == VAL_THUMB_MOV_IMM8) {
        return (input & BIT_RANGE(0, 7)) |
            ((input & BIT_RANGE(8, 10)) << 4) |
            0xe3b00000;
    } else if ((input & MASK_THUMB_STR_RD_IND_RN_RM) ==
               VAL_THUMB_STR_RD_IND_RN_RM) {
        unsigned rm = input >> 6 & 0x7;
        unsigned rn = input >> 3 & 0x7;
        unsigned rd = input & 0x7;
        return rm | (rd << 12) | (rn << 16) | 0xe7800000;
    } else if ((input & MASK_THUMB_ADD_REG_REG_IMM3) ==
               VAL_THUMB_ADD_REG_REG_IMM3) {
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned imm3 = (input >> 6) & 0x7;
        return 0xe2900000 | imm3 | (rd << 12) | (rn << 16);
    } else if ((input & MASK_THUMB_PUSH) == VAL_THUMB_PUSH) {
        unsigned regs = input & 0xff;
        unsigned r_bit = (input >> 8) & 1;
        return regs | (r_bit << 14) | 0xe92d0000;
    } else if ((input & MASK_THUMB_SUB_SP_IMM7) == VAL_THUMB_SUB_SP_IMM7) {
        return (input & 0x7f) | 0xe24ddf00;
    } else if ((input & MASK_THUMB_STR_REG_IND_SP_IMM8) ==
               VAL_THUMB_STR_REG_IND_SP_IMM8) {
        unsigned rd = (input >> 8) & 0x7;
        unsigned imm8 = input & 0xff;
        return (imm8 << 2) | (rd << 12) | 0xe58d0000;
    } else if ((input & MASK_THUMB_MVN) == VAL_THUMB_MVN) {
        unsigned rd = input & 0x7;
        unsigned rm = (input >> 3) & 0x7;
        return rm | (rd << 12) | 0xe1f00000;
    } else if ((input & MASK_THUMB_LSL_IMM5) == VAL_THUMB_LSL_IMM5) {
        unsigned imm5 = (input >> 6) & 0x1f;
        unsigned rm = (input >> 3) & 0x7;
        unsigned rd = input & 0x7;
        return rm | (imm5 << 7) | (rd << 12) | 0xe1b00000;
    } else if ((input & MASK_THUMB_STRH_IMM5) == VAL_THUMB_STRH_IMM5) {
        unsigned imm5 = (input >> 6) & 0x1f;
        unsigned rn = (input >> 3) & 0x7;
        unsigned rd = input & 0x7;
        return ((imm5 & 0x7) << 1) | ((imm5 & 0x18) << 5) |
            (rd << 12) | (rn << 16) | 0xe1c000b0;
    } else if ((input & MASK_THUMB_TST) == VAL_THUMB_TST) {
        unsigned rn = input & 0x7;
        unsigned rm = (input >> 3) & 0x7;
        return rm | (rn << 16) | 0xe1100000;
    } else if ((input & MASK_THUMB_LSR_IMM5) == VAL_THUMB_LSR_IMM5) {
        unsigned rd = input & 0x7;
        unsigned rm = (input >> 3) & 0x7;
        unsigned imm5 = (input >> 6) & 0x1f;
        return rm | (imm5 << 7) | (rd << 12) | 0xe1b00020;
    } else if ((input & MASK_THUMB_ADD_REG_REG_REG) == VAL_THUMB_ADD_REG_REG_REG) {
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned rm = (input >> 6) & 0x7;
        return rm | (rd << 12) | (rn << 16) | 0xe0900000;
    } else if ((input & MASK_THUMB_MOV_REG_REG) == VAL_THUMB_MOV_REG_REG) {
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        return (rd << 12) | (rn << 16) | 0xe2900000;
    } else if ((input & MASK_THUMB_MOV_HIGH_HIGH) == VAL_THUMB_MOV_HIGH_HIGH) {
        unsigned rd = input & 0x7;
        unsigned rm = (input >> 3) & 0x7;
        unsigned h2 = (input >> 6) & 1;
        unsigned h1 = (input >> 7) & 1;
        return rm | (rd << 12) | (h2 << 3) | (h1 << 15) | 0xe1a00000;
    } else if ((input & MASK_THUMB_ORR) == VAL_THUMB_ORR) {
        unsigned rd = input & 0x7;
        unsigned rm = (input >> 3) & 0x7;
        return rm | (rd << 12) | (rd << 16) | 0xe1900000;
    } else if ((input & MASK_THUMB_SUB_REG_IMM8) == VAL_THUMB_SUB_REG_IMM8) {
        unsigned imm8 = input & 0xff;
        unsigned rd = (input >> 8) & 0x7;
        return imm8 | (rd << 12) | (rd << 16) | 0xe2500000;
    } else if ((input & MASK_THUMB_STRB_IMM5) == VAL_THUMB_STRB_IMM5) {
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned imm5 = (input >> 6) & 0x1f;
        return imm5 | (rd << 12) | (rn << 16) | 0xe5c00000;
    } else if ((input & MASK_THUMB_SUB_REG_REG_IMM3) ==
               VAL_THUMB_SUB_REG_REG_IMM3) {
        // sub <Rd>, <Rn>, #immed_3
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned imm3 = (input >> 6) & 0x7;
        return imm3 | (rd << 12) | (rn << 16) | 0xe2500000;
    } else if ((input & MASK_THUMB_STR_IND_RN_IMM5) ==
               VAL_THUMB_STR_IND_RN_IMM5) {
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned imm5 = (input >> 6) & 0x1f;
        return (imm5 << 2) | (rd << 12) | (rn << 16) | 0xe5800000;
    } else if ((input & MASK_THUMB_ADD_REG_IMM8) == VAL_THUMB_ADD_REG_IMM8) {
        // ADD <Rd>, #immed_8
        unsigned rd = (input >> 8) & 0x7;
        unsigned imm8 = input & 0xff;
        return imm8 | (rd << 12) | (rd << 16) | 0xe2900000;
    } else if ((input & MASK_THUMB_LDRH_REG_IMM5) == VAL_THUMB_LDRH_REG_IMM5) {
        // ldrh <Rd>, [<Rn>, #immed_5 * 2]
        unsigned rd = input & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned imm5 = (input >> 6) & 0x1f;
        return ((imm5 & BIT_RANGE(0, 2)) << 1) |
            ((imm5 & BIT_RANGE(3, 4)) << 5) |
            (rd << 12) | (rn << 16) | 0xe1d000b0;
    } else if ((input & MASK_THUMB_ADD_R13_IMM7) == VAL_THUMB_ADD_R13_IMM7) {
        // add r13, immed_7 * 4
        return 0xe28ddf00 | (input & BIT_RANGE(0, 6));
    } else if ((input & MASK_THUMB_POP) == VAL_THUMB_POP) {
        // pop {registers}
        return (input & BIT_RANGE(0, 7)) | ((input & 0x100) << 7) | 0xe8bd0000;
    } else if ((input & MASK_THUMB_CMP_REG_IMM8) == VAL_THUMB_CMP_REG_IMM8) {
        // cmp <Rn>, #immed_8
        return (input & BIT_RANGE(0, 7)) |
            ((input & BIT_RANGE(8, 10)) << 8) | 0xe3500000;
    } else if ((input & MASK_THUMB_CMP_HIGH_HIGH) == VAL_THUMB_CMP_HIGH_HIGH) {
        /*
         * cmp <Rn>, <Rm> (where Rn and Rm can optionally reference the 8
         * general-purpose registers that are usually not available in thumb mode)
         */
        unsigned rn = input & 7;
        unsigned rm = (input >> 3) & 7;
        unsigned h2 = (input >> 6) & 1;
        unsigned h1 = (input >> 7) & 1;

        return (h1 << 19) | (h2 << 3) | (rn << 16) | rm | 0xe1500000;
    } else if ((input & MASK_THUMB_LDMIA_RN_REGS) == VAL_THUMB_LDMIA_RN_REGS) {
        // ldmia <Rn>!, <register_list>
        unsigned reg_list = input & BIT_RANGE(0, 7);
        unsigned rn = (input >> 8) & 7;
        unsigned w_flag = (reg_list & (1 << rn)) ? 0 : 1;

        return (w_flag << 21) | (rn << 16) | reg_list | 0xe8900000;
    } else if ((input & MASK_THUMB_ADD_REG_R13_IMM8) ==
               VAL_THUMB_ADD_REG_R13_IMM8) {
        // add <Rd>, SP, #imm8 * 4
        return (input & BIT_RANGE(0, 7)) | ((input & BIT_RANGE(8, 10)) << 4) |
            0xe28d0f00;
    } else if ((input & MASK_THUMB_STMIA_REG_REGS) == VAL_THUMB_STMIA_REG_REGS) {
        return (input & BIT_RANGE(0, 7)) | ((input & BIT_RANGE(8, 10)) << 8) |
            0xe8a00000;
    } else if ((input & MASK_THUMB_LDRB_RD_IND_RN_IMM5) ==
               VAL_THUMB_LDRB_RD_IND_RN_IMM5) {
        return ((input & BIT_RANGE(6, 10)) >> 6) |
            ((input & BIT_RANGE(0, 2)) << 12) |
            ((input & BIT_RANGE(3, 5)) << 13) |
            0xe5d00000;
    } else if ((input & MASK_THUMB_CMP_REG_REG) == VAL_THUMB_CMP_REG_REG) {
        return ((input & BIT_RANGE(3, 5)) >> 3) |
            ((input & BIT_RANGE(0, 2)) << 16) |
            0xe1500000;
    } else if ((input & MASK_LDRH_REG_IND_REG_REG) ==
               VAL_LDRH_REG_IND_REG_REG) {
        // ldrh <Rd>, [<Rn>, <Rm>]
        unsigned rm = (input >> 6) & 7;
        unsigned rn = (input >> 3) & 7;
        unsigned rd = input & 7;
        return rm | (rd << 12) | (rn << 16) | 0xe19000b0;
    } else if ((input & MASK_STRH_RD_IND_RN_RM) == VAL_STRH_RD_IND_RN_RM) {
        // strh <Rd>, [<Rn>, <Rm>]
        unsigned rm = (input >> 6) & 7;
        unsigned rn = (input >> 3) & 7;
        unsigned rd = input & 7;
        return rm | (rd << 12) | (rn << 16) | 0xe18000b0;
    } else if ((input & MASK_THUMB_ROR_REG_REG) == VAL_THUMB_ROR_REG_REG) {
        // ror <Rd>, <Rs>
        unsigned rd = input & 7;
        unsigned rs = (input >> 3) & 7;
        return rd | (rs << 8) | (rd << 12) | 0xe1b00070;
    } else if ((input & MASK_THUMB_EOR_REG_REG) == VAL_THUMB_EOR_REG_REG) {
        // eor <Rd>, <Rm>
        unsigned rd = input & 7;
        unsigned rm = (input >> 3) & 7;
        return rm | (rd << 12) | (rd << 16) | 0xe0300000;
    } else if ((input & MASK_THUMB_MUL_REG_REG) == VAL_THUMB_MUL_REG_REG) {
        // mul <Rd>, <Rm>
        unsigned rd = input & 7;
        unsigned rm = (input >> 3) & 7;
        return rm | (rd << 8) | (rd << 16) | 0xe0100090;
    } else if ((input & MASK_THUMB_ASR_REG_REG_IMM5) == VAL_THUMB_ASR_REG_REG_IMM5) {
        unsigned imm5 = (input >> 6) & 0x1f;
        unsigned rd = input & 7;
        unsigned rm = (input >> 3) & 7;
        return rm | (imm5 << 7) | (rd << 12) | 0xe1b00040;
    } else if ((input & MASK_THUMB_LDR_REG_IND_R13_IMM8) ==
               VAL_THUMB_LDR_REG_IND_R13_IMM8) {
        // ldr <Rd>, [<R13>, #imm8 * 4]
        return ((input & BIT_RANGE(0, 7)) << 2) |
            ((input & BIT_RANGE(8, 10)) << 4) |
            0xe59d0000;
    } else if ((input & MASK_THUMB_NEG_REG_REG) == VAL_THUMB_NEG_REG_REG) {
        // neg <Rd>, <Rm>
        return ((input & 7) << 12) | ((input & BIT_RANGE(3, 5)) << 13) |
            0xe2700000;
    } else if ((input & MASK_THUMB_AND_REG_REG) == VAL_THUMB_AND_REG_REG) {
        // and <Rd>, <Rm>
        unsigned rd = input & 7;
        unsigned rm = (input >> 3) & 7;
        return rm | (rd << 12) | (rd << 16) | 0xe0100000;
    } else if ((input & MASK_THUMB_LDR_REG_IND_REG_IMM5) ==
               VAL_THUMB_LDR_REG_IND_REG_IMM5) {
        return ((input & 7) << 12) |
            ((input & BIT_RANGE(3, 5)) << 13) |
            ((input & BIT_RANGE(6, 10)) >> 4) |
            0xe5900000;
    } else if ((input & MASK_THUMB_SUB_REG_REG_REG) ==
               VAL_THUMB_SUB_REG_REG_REG) {
        // sub <Rd>, <Rn>, <Rm>
        return ((input & BIT_RANGE(6, 8)) >> 6) |
            ((input & 7) << 12) |
            ((input & BIT_RANGE(3, 5)) << 13) |
            0xe0500000;
    } else if ((input & MASK_THUMB_LDR_REG_IND_REG_REG) ==
               VAL_THUMB_LDR_REG_IND_REG_REG) {
        unsigned rm = (input >> 6) & 0x7;
        unsigned rn = (input >> 3) & 0x7;
        unsigned rd = input & 7;
        return rm | (rd << 12) | (rn << 16) | 0xe7900000;
    } else if ((input & MASK_THUMB_CMN_REG_REG) == VAL_THUMB_CMN_REG_REG) {
        return ((input & 7) << 16) | ((input >> 3) & 7) | 0xe1700000;
    } else if ((input & MASK_THUMB_BIC_REG_REG) == VAL_THUMB_BIC_REG_REG) {
        unsigned rd = input & 7;
        unsigned rm = (input >> 3) & 7;
        return rm | (rd << 12) | (rd << 16) | 0xe1d00000;
    } else if ((input & MASK_THUMB_LDSRB_REG_IND_REG_REG) ==
               VAL_THUMB_LDSRB_REG_IND_REG_REG) {
        unsigned rd = input & 7;
        unsigned rn = (input >> 3) & 7;
        unsigned rm = (input >> 6) & 7;
        return rm | (rd << 12) | (rn << 16) | 0xe19000d0;
    } else if ((input & MASK_THUMB_LDRB_REG_IND_REG_REG) ==
               VAL_THUMB_LDRB_REG_IND_REG_REG) {
        unsigned rd = input & 7;
        unsigned rn = (input >> 3) & 7;
        unsigned rm = (input >> 6) & 7;
        return rm | (rd << 12) | (rn << 16) | 0xe7d00000;
    } else if ((input & MASK_THUMB_LSR_REG_REG) ==
               VAL_THUMB_LSR_REG_REG) {
        unsigned rd = input & 7;
        unsigned rs = (input >> 3) & 7;
        return rd | (rd << 12) | (rs << 8) | 0xe1b00030;
    } else if ((input & MASK_THUMB_ADD_HIGH_HIGH) == VAL_THUMB_ADD_HIGH_HIGH) {
        unsigned rd = (input & 7) | ((input >> 7) & 1);
        unsigned rm = (input >> 3) & 15;
        return 0xe0800000 | rm | (rd << 12) | (rd << 16);
    } else if ((input & MASK_THUMB_LSL_REG_REG) == VAL_THUMB_LSL_REG_REG) {
        unsigned rd = input & 7;
        unsigned rs = (input >> 3) & 7;
        return 0xe1b00010 | rd | (rs << 8) | (rd << 12);
    } else if ((input & MASK_THUMB_LDRSH_REG_IND_REG_REG) ==
               VAL_THUMB_LDRSH_REG_IND_REG_REG) {
        unsigned rd = input & 7;
        unsigned rn = (input >> 3) & 7;
        unsigned rm = (input >> 6) & 7;
        return 0xe19000f0 | rm | (rd << 12) | (rn << 16);
    } else if ((input & MASK_THUMB_SWI_IMM8) == VAL_THUMB_SWI_IMM8) {
        return 0xef000000 | (input & 0xff);
    } else if ((input & MASK_THUMB_ASR_REG_REG) == VAL_THUMB_ASR_REG_REG) {
        unsigned rd = input & 7;
        unsigned rs = (input >> 3) & 7;
        return 0xe1b00050 | rd | (rd << 12) | (rs << 8);
    }

    error_set_thumb_instruction(input);
    EMU_ERROR(ERROR_UNRECOGNIZED_THUMB_OPCODE);
}

static unsigned
arm7_thumb_b_imm8(struct arm7 *arm7, arm7_inst inst) {
    if (arm7_eval_cond(arm7, inst >> 8)) {
        int32_t dst = (inst & BIT_RANGE(0, 7)) * 2;
        if (dst & (1 << 8))
            dst |= 0xfffffe00;
        else
            dst &= 0x1ff;

        arm7->reg[ARM7_REG_PC] += dst;
        arm7_reset_pipeline(arm7);
    } else {
        next_inst(arm7);
    }

    return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;
}

static unsigned
arm7_thumb_b_imm11(struct arm7 *arm7, arm7_inst inst) {
    int32_t dst = (inst & BIT_RANGE(0, 10)) * 2;
    if (dst & (1 << 11))
        dst |= 0xfffff000;
    else
        dst &= 0xfff;

    arm7->reg[ARM7_REG_PC] += dst;
    arm7_reset_pipeline(arm7);

    return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;
}

static unsigned
arm7_thumb_bx_reg(struct arm7 *arm7, arm7_inst inst) {
    /*
     * unlike most other thumb instructions, bx can access
     * all 15 general-purpose registers
     */
    uint32_t src_val = *arm7_gen_reg(arm7, (inst >> 3) & 0xf);
    uint32_t dst = src_val & ~1;

    if (!(src_val & 1)) {
        if (dst & 3) {
            // arm documentation says this case is undefined behavior
            error_set_address(dst);
            EMU_ERROR(ERROR_UNIMPLEMENTED);
        }
        arm7->reg[ARM7_REG_CPSR] &= ~ARM7_CPSR_T_MASK; // exit thumb mode
    }

    arm7->reg[ARM7_REG_PC] = dst;
    arm7_reset_pipeline(arm7);
    return 2 * S_CYCLE + 1 * N_CYCLE;
}

static unsigned
arm7_thumb_prepare_long_branch(struct arm7 *arm7, arm7_inst inst) {
    int32_t offs = inst & BIT_RANGE(0, 10);
    if (offs & (1 << 10))
        offs |= 0xfffffc00;

    *arm7_gen_reg(arm7, 14) = (offs << 12) + *arm7_gen_reg(arm7, 15);
    next_inst(arm7);

    return S_CYCLE;
}

static unsigned
arm7_thumb_bl_long(struct arm7 *arm7, arm7_inst inst) {

    uint32_t offs = inst & BIT_RANGE(0, 10);

    uint32_t link = (*arm7_gen_reg(arm7, 15) - 2) | 1;
    *arm7_gen_reg(arm7, 15) = *arm7_gen_reg(arm7, 14) +
        offs * 2;
    *arm7_gen_reg(arm7, 14) = link;
    arm7_reset_pipeline(arm7);

    return 2 * S_CYCLE + N_CYCLE;
}

static unsigned
arm7_thumb_add_reg_pc_imm8(struct arm7 *arm7, arm7_inst inst) {
    unsigned rd = (inst >> 8) & 7;
    unsigned imm8 = inst & 0xff;
    *arm7_gen_reg(arm7, rd) = (*arm7_gen_reg(arm7, 15) & ~3) + (imm8 << 2);
    next_inst(arm7);
    return 2 * S_CYCLE + 1 * N_CYCLE;
}

static unsigned
arm7_thumb_ldr_pc_imm8(struct arm7 *arm7, arm7_inst inst) {
    unsigned rd = (inst >> 8) & 7;
    unsigned imm8 = inst & 0xff;
    uint32_t addr = (*arm7_gen_reg(arm7, 15) & ~3) + imm8 * 4;
    *arm7_gen_reg(arm7, rd) = thumb_read32(arm7, addr);
    next_inst(arm7);

    return 1; // TODO: INCORRECT CYCLE COUNT!!!
}

arm7_op_fn arm7_thumb_decode(struct arm7 *arm7, arm7_inst *inst) {
    if (((*inst & MASK_THUMB_B_IMM8) == VAL_THUMB_B_IMM8) &&
        (*inst & BIT_RANGE(8, 11)) != 0x0f00) { // condition code NV is illegal
        return arm7_thumb_b_imm8;
    } else if ((*inst & MASK_THUMB_B_IMM11) == VAL_THUMB_B_IMM11) {
        return arm7_thumb_b_imm11;
    } else if ((*inst & MASK_THUMB_BX_REG) == VAL_THUMB_BX_REG) {
        return arm7_thumb_bx_reg;
    } else if ((*inst & MASK_THUMB_PRE_LONG_BRANCH) ==
               VAL_THUMB_PRE_LONG_BRANCH) {
        return arm7_thumb_prepare_long_branch;
    } else if ((*inst & MASK_THUMB_BL_LONG) == VAL_THUMB_BL_LONG) {
        return arm7_thumb_bl_long;
    } else if ((*inst & MASK_THUMB_ADD_REG_PC_IMM8) == VAL_THUMB_ADD_REG_PC_IMM8) {
        return arm7_thumb_add_reg_pc_imm8;
    } else if ((*inst & MASK_THUMB_LDR_PC_IMM8) == VAL_THUMB_LDR_PC_IMM8) {
        return arm7_thumb_ldr_pc_imm8;
    } else {
        *inst = arm7_translate_thumb(*inst);
        return arm7_decode(arm7, *inst);
    }
}

static inline uint32_t thumb_read32(struct arm7 *arm7, uint32_t addr) {
    uint32_t val;
    switch (memory_map_try_read_32(arm7->map, addr, &val)) {
    case MEM_ACCESS_SUCCESS:
        return val;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
    default:
    case MEM_ACCESS_FAILURE:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNMAPPED_READ);
    }
}
