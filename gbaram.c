/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "mem_code.h"

#include "gbaram.h"

static int gbaram_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBARAM_LEN) {
        memcpy(val, ram->mem + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbaram_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBARAM_LEN) {
        memcpy(val, ram->mem + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbaram_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if (addr < GBARAM_LEN) {
        *val = ram->mem[addr];
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbaram_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if ((addr & (sizeof(val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBARAM_LEN) {
        memcpy(ram->mem + addr, &val, sizeof(val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbaram_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if ((addr & (sizeof(val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    if (addr < GBARAM_LEN) {
        memcpy(ram->mem + addr, &val, sizeof(val));
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

static int gbaram_try_write8(uint32_t addr, uint8_t val, void *ctxt) {
    struct gbaram *ram = ctxt;

    if (addr < GBARAM_LEN) {
        ram->mem[addr] = val;
        return MEM_ACCESS_SUCCESS;
    } else {
        return MEM_ACCESS_FAILURE;
    }
}

struct memory_interface const gbaram_memory_interface = {
    .try_read32 = gbaram_try_read32,
    .try_read16 = gbaram_try_read16,
    .try_read8 = gbaram_try_read8,

    .try_write32 = gbaram_try_write32,
    .try_write16 = gbaram_try_write16,
    .try_write8 = gbaram_try_write8
};

void gbaram_init(struct gbaram *ram) {
    memset(ram->mem, 0, sizeof(ram->mem));
}

void gbaram_cleanup(struct gbaram *ram) {
}
