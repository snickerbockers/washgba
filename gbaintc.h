/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBAINTC_H_
#define GBAINTC_H_

#include <stdint.h>
#include <stdbool.h>

#include "arm7.h"

#define GBAINTC_VBLANK_SHIFT 0
#define GBAINTC_HBLANK_SHIFT 1
#define GBAINTC_VCOUNT_SHIFT 2
#define GBAINTC_TIM0_SHIFT 3
#define GBAINTC_TIM1_SHIFT 4
#define GBAINTC_TIM2_SHIFT 5
#define GBAINTC_TIM3_SHIFT 6
#define GBAINTC_SERIAL_SHIFT 7
#define GBAINTC_DMA0_SHIFT 8
#define GBAINTC_DMA1_SHIFT 9
#define GBAINTC_DMA2_SHIFT 10
#define GBAINTC_DMA3_SHIFT 11
#define GBAINTC_KEYPAD_SHIFT 12
#define GBAINTC_GAMEPACK_SHIFT 13

#define GBAINTC_VBLANK_MASK (1 << GBAINTC_VBLANK_SHIFT)
#define GBAINTC_HBLANK_MASK (1 << GBAINTC_HBLANK_SHIFT)
#define GBAINTC_VCOUNT_MASK (1 << GBAINTC_VCOUNT_SHIFT)
#define GBAINTC_TIM0_MASK (1 << GBAINTC_TIM0_SHIFT)
#define GBAINTC_TIM1_MASK (1 << GBAINTC_TIM1_SHIFT)
#define GBAINTC_TIM2_MASK (1 << GBAINTC_TIM2_SHIFT)
#define GBAINTC_TIM3_MASK (1 << GBAINTC_TIM3_SHIFT)
#define GBAINTC_SERIAL_MASK (1 << GBAINTC_SERIAL_SHIFT)
#define GBAINTC_DMA0_MASK (1 << GBAINTC_DMA0_SHIFT)
#define GBAINTC_DMA1_MASK (1 << GBAINTC_DMA1_SHIFT)
#define GBAINTC_DMA2_MASK (1 << GBAINTC_DMA2_SHIFT)
#define GBAINTC_DMA3_MASK (1 << GBAINTC_DMA3_SHIFT)
#define GBAINTC_KEYPAD_MASK (1 << GBAINTC_KEYPAD_SHIFT)
#define GBAINTC_GAMEPACK_MASK (1 << GBAINTC_GAMEPACK_SHIFT)

struct gbaintc {
    struct arm7 *cpu;

    unsigned irq_lines;
    bool ime;
};

void gbaintc_init(struct gbaintc *intc, struct arm7 *cpu);
void gbaintc_cleanup(struct gbaintc *intc);

bool gbaintc_set_ime(struct gbaintc *intc, bool ime);

// irqmask can be more than one irq if you want
void gbaintc_raise_irq(struct gbaintc *intc, unsigned irqmask);

// like gbaintc_raise_irq, except it can only be used outside of cpu context
void gbaintc_raise_irq_outside_cpu_context(struct gbaintc *intc, unsigned irq);

uint16_t gbaintc_get_irqs(struct gbaintc *intc);

void gbaintc_clear_irqs(struct gbaintc *intc, uint16_t irq);

#endif
