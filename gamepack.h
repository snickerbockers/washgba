/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GAMEPACK_H_
#define GAMEPACK_H_

#include <stdint.h>

#include "memory_map.h"
#include "hostfile.h"

#define GAMEPACK_WAITSTATE0_FIRST 0x08000000
#define GAMEPACK_WAITSTATE0_LAST  0x09ffffff

#define GAMEPACK_WAITSTATE1_FIRST 0x0a000000
#define GAMEPACK_WAITSTATE1_LAST  0x0bffffff

#define GAMEPACK_WAITSTATE2_FIRST 0x0c000000
#define GAMEPACK_WAITSTATE2_LAST  0x0dffffff

#define GAMEPACK_SRAM_FIRST 0x0e000000
#define GAMEPACK_SRAM_LAST  0x0e00ffff

#define GAMEPACK_LEN ((GAMEPACK_WAITSTATE0_LAST - GAMEPACK_WAITESTATE0_FIRST + 1) / 4)
#define GAMEPACK_SRAM_LEN (GAMEPACK_SRAM_LAST - GAMEPACK_SRAM_FIRST + 1)

#define MAX_ROM_SIZE (32 * 1024 * 1024)

struct gamepack {
    char *dat;
    char *sram;
};

void gamepack_init(struct gamepack *pack, washdc_hostfile rom);
void gamepack_cleanup(struct gamepack *pack);

extern struct memory_interface const gamepack_memory_interface;
extern struct memory_interface const gamepack_sram_memory_interface;

#endif
