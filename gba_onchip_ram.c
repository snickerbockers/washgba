/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "mem_code.h"

#include "gba_onchip_ram.h"

static int gba_onchip_ram_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    memcpy(val, ram->mem + addr, sizeof(*val));
    return MEM_ACCESS_SUCCESS;
}

static int gba_onchip_ram_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    if ((addr & (sizeof(*val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    memcpy(val, ram->mem + addr, sizeof(*val));
    return MEM_ACCESS_SUCCESS;
}

static int gba_onchip_ram_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    *val = ram->mem[addr];
    return MEM_ACCESS_SUCCESS;
}

static int gba_onchip_ram_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    if ((addr & (sizeof(val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    memcpy(ram->mem + addr, &val, sizeof(val));
    return MEM_ACCESS_SUCCESS;
}

static int gba_onchip_ram_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    if ((addr & (sizeof(val) - 1)) != 0)
        return MEM_ACCESS_BADALIGN;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    memcpy(ram->mem + addr, &val, sizeof(val));
    return MEM_ACCESS_SUCCESS;
}

static int gba_onchip_ram_try_write8(uint32_t addr, uint8_t val, void *ctxt) {
    struct gba_onchip_ram *ram = ctxt;

    addr &= (GBA_ONCHIP_RAM_LEN - 1);
    ram->mem[addr] = val;
    return MEM_ACCESS_SUCCESS;
}

struct memory_interface const gba_onchip_ram_memory_interface = {
    .try_read32 = gba_onchip_ram_try_read32,
    .try_read16 = gba_onchip_ram_try_read16,
    .try_read8 = gba_onchip_ram_try_read8,

    .try_write32 = gba_onchip_ram_try_write32,
    .try_write16 = gba_onchip_ram_try_write16,
    .try_write8 = gba_onchip_ram_try_write8
};

void gba_onchip_ram_init(struct gba_onchip_ram *ram) {
    memset(ram, 0, sizeof(*ram));
}

void gba_onchip_ram_cleanup(struct gba_onchip_ram *ram) {
}
