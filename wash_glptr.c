/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_opengl_glext.h"

#include "log.h"

#include "wash_glptr.h"

PFNGLCREATESHADERPROC pglCreateShader;
PFNGLDELETESHADERPROC pglDeleteShader;
PFNGLSHADERSOURCEPROC pglShaderSource;
PFNGLCOMPILESHADERPROC pglCompileShader;
PFNGLGETSHADERIVPROC pglGetShaderiv;
PFNGLGETSHADERSOURCEPROC pglGetShaderSource;
PFNGLGETSHADERSOURCEPROC pglGetShaderInfoLog;
PFNGLCREATEPROGRAMPROC pglCreateProgram;
PFNGLATTACHSHADERPROC pglAttachShader;
PFNGLLINKPROGRAMPROC pglLinkProgram;
PFNGLGETPROGRAMIVPROC pglGetProgramiv;
PFNGLGETUNIFORMLOCATIONPROC pglGetUniformLocation;
PFNGLGETATTRIBLOCATIONPROC pglGetAttribLocation;
PFNGLDELETEPROGRAMPROC pglDeleteProgram;
PFNGLGENBUFFERSPROC pglGenBuffers;
PFNGLBINDBUFFERPROC pglBindBuffer;
PFNGLBUFFERDATAPROC pglBufferData;
PFNGLDELETEBUFFERSPROC pglDeleteBuffers;
PFNGLVERTEXATTRIBPOINTERPROC pglVertexAttribPointer;
PFNGLENABLEVERTEXATTRIBARRAYPROC pglEnableVertexAttribArray;
PFNGLDISABLEVERTEXATTRIBARRAYPROC pglDisableVertexAttribArray;
PFNGLGENVERTEXARRAYSPROC pglGenVertexArrays;
PFNGLBINDVERTEXARRAYPROC pglBindVertexArray;
PFNGLDELETEVERTEXARRAYSPROC pglDeleteVertexArrays;
PFNGLUSEPROGRAMPROC pglUseProgram;
PFNGLACTIVETEXTUREPROC pglActiveTexture;
PFNGLUNIFORM1IPROC pglUniform1i;
PFNGLUNIFORMMATRIX4FVPROC pglUniformMatrix4fv;

PTR_GLCLEAR pglClear;
PTR_GLCLEARCOLOR pglClearColor;
PTR_GLGENTEXTURES pglGenTextures;
PTR_GLBINDTEXTURE pglBindTexture;
PTR_GLTEXIMAGE2D pglTexImage2D;
PTR_GLDELETETEXTURES pglDeleteTextures;
PTR_GLTEXPARAMETERI pglTexParameteri;
PTR_GLDRAWELEMENTS pglDrawElements;
PTR_GLPOLYGONMODE pglPolygonMode;

#define CORE_PTR(func, major, minor)                        \
    do {                                                    \
        if (check_gl_version(#func, major, minor) != 0 ||   \
            !(p##func = SDL_GL_GetProcAddress(#func))) {    \
            LOG_ERROR("ERROR: unable to load %s\n", #func); \
        }                                                   \
    } while (0)

#define ORIG_PTR(func)                                      \
    do {                                                    \
        if (!(p##func = SDL_GL_GetProcAddress(#func))) {    \
            LOG_ERROR("ERROR: unable to load %s\n", #func); \
        }                                                   \
    } while (0)

static int check_gl_version(char const *name, GLint major, GLint minor);

void wash_load_gl(void) {
    CORE_PTR(glCreateShader, 2, 0);
    CORE_PTR(glDeleteShader, 2, 0);
    CORE_PTR(glShaderSource, 2, 0);
    CORE_PTR(glCompileShader, 2, 0);
    CORE_PTR(glGetShaderiv, 2, 0);
    CORE_PTR(glGetShaderSource, 2, 0);
    CORE_PTR(glGetShaderInfoLog, 2, 0);
    CORE_PTR(glCreateProgram, 2, 0);
    CORE_PTR(glAttachShader, 2, 0);
    CORE_PTR(glLinkProgram, 2, 0);
    CORE_PTR(glGetProgramiv, 2, 0);
    CORE_PTR(glGetUniformLocation, 2, 0);
    CORE_PTR(glGetAttribLocation, 2, 0);
    CORE_PTR(glDeleteProgram, 2, 0);
    CORE_PTR(glGenBuffers, 1, 5);
    CORE_PTR(glBindBuffer, 1, 5);
    CORE_PTR(glBufferData, 1, 5);
    CORE_PTR(glDeleteBuffers, 1, 5);
    CORE_PTR(glVertexAttribPointer, 2, 0);
    CORE_PTR(glEnableVertexAttribArray, 2, 0);
    CORE_PTR(glDisableVertexAttribArray, 2, 0);
    CORE_PTR(glGenVertexArrays, 3, 0);
    CORE_PTR(glBindVertexArray, 3, 0);
    CORE_PTR(glDeleteVertexArrays, 3, 0);
    CORE_PTR(glUseProgram, 2, 0);
    CORE_PTR(glActiveTexture, 1, 3);
    CORE_PTR(glUniform1i, 2, 0);
    CORE_PTR(glUniformMatrix4fv, 2, 0);

    ORIG_PTR(glClear);
    ORIG_PTR(glClearColor);
    ORIG_PTR(glGenTextures);
    ORIG_PTR(glBindTexture);
    ORIG_PTR(glTexImage2D);
    ORIG_PTR(glDeleteTextures);
    ORIG_PTR(glTexParameteri);
    ORIG_PTR(glDrawElements);
    ORIG_PTR(glPolygonMode);
}

// returns 0 if the requested gl version is supported, else -1
static int check_gl_version(char const *name, GLint major, GLint minor) {
    void(*pglGetIntegerv)(GLenum, GLint*) = SDL_GL_GetProcAddress("glGetIntegerv");
    if (!pglGetIntegerv) {
        LOG_ERROR("ERROR: unable to load glGetIntegerv from OpenGL!\n");
        return -1;
    }

    GLint major_gl_version;
    pglGetIntegerv(GL_MAJOR_VERSION, &major_gl_version);
    if (major > major_gl_version) {
        LOG_ERROR("ERROR: unable to load function \"%s\" due to invalid GL "
                  "major version\n", name);
        return -1;
    }

    if (major == major_gl_version) {
        GLint minor_gl_version;
        pglGetIntegerv(GL_MINOR_VERSION, &minor_gl_version);
        if (minor > minor_gl_version) {
            LOG_ERROR("ERROR: unable to load function \"%s\" due to invalid GL "
                      "minor version\n", name);
            return -1;
        }
    }

    // success
    return 0;
}
