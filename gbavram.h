/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBAVRAM_H_
#define GBAVRAM_H_

#include <stdint.h>

#include "memory_map.h"

#define GBAVRAM_FIRST 0x06000000
#define GBAVRAM_LAST 0x06017fff

#define GBAVRAM_LEN ((GBAVRAM_LAST - GBAVRAM_FIRST + 1) / 2)

struct gbavram {
    uint16_t vram[GBAVRAM_LEN];
};

void gbavram_init(struct gbavram *vram);
void gbavram_cleanup(struct gbavram *vram);

extern struct memory_interface const gbavram_memory_interface;

#endif
