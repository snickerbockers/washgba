/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef EMU_H_
#define EMU_H_

#include <stdbool.h>

#include "error.h"

enum wash_state {
    // the emulation thread has not been started yet
    WASH_STATE_NOT_RUNNING,

    // the emulation thread is currently executing
    WASH_STATE_RUNNING,

    // the emulation thread has been suspended by the gdb stub
    WASH_STATE_DEBUG,

    // the emulation thread has been suspended by the command-line interface
    WASH_STATE_SUSPEND
};

bool washgba_is_running(void);

void wash_kill(void);

void wash_state_transition(enum wash_state state_new,
                           enum wash_state state_old);
void wash_inject_irq(char const *id);

#endif

