/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stddef.h>
#include <string.h>

#include "error.h"
#include "mem_code.h"
#include "palram.h"

static int palram_try_read16(uint32_t addr, uint16_t *val, void *ctxt);
static int palram_try_write16(uint32_t addr, uint16_t val, void *ctxt);

static int palram_try_read32(uint32_t addr, uint32_t *val, void *ctxt);
static int palram_try_write32(uint32_t addr, uint32_t val, void *ctxt);

struct memory_interface const palram_memory_interface = {
    .try_read32 = palram_try_read32,
    .try_read16 = palram_try_read16,
    .try_read8 = NULL,

    .try_write32 = palram_try_write32,
    .try_write16 = palram_try_write16,
    .try_write8 = NULL
};

void palram_init(struct palram *pal) {
    memset(pal, 0, sizeof(*pal));
}

void palram_cleanup(struct palram *pal) {
}

static int palram_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (addr < PALRAM_LEN) {
        struct palram *pal = ctxt;
        *val = pal->mem[idx];
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int palram_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 1) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx < PALRAM_LEN) {
        struct palram *pal = ctxt;
        pal->mem[idx] = val;
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int palram_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= PALRAM_LEN - 2) {
        struct palram *pal = ctxt;
        memcpy(val, pal->mem + idx, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int palram_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    unsigned idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= PALRAM_LEN - 2) {
        struct palram *pal = ctxt;
        memcpy(pal->mem + idx, &val, sizeof(val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}
