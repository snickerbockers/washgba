/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "gba.h"
#include "error.h"

#include "gbadmac.h"

static void gba_dma_xfer(struct gba *gba, unsigned chan, unsigned n_units,
                         uint32_t src, uint32_t dst, unsigned ctrl);

void gbadmac_init(struct gbadmac *dmac) {
    memset(dmac, 0, sizeof(*dmac));
}

void gbadmac_cleanup(struct gbadmac *dmac) {
}

static DEF_ERROR_U32_ATTR(src_addr)
static DEF_ERROR_U32_ATTR(dst_addr)
static DEF_ERROR_INT_ATTR(dma_chan)
static DEF_ERROR_INT_ATTR(dma_start_timing)
static DEF_ERROR_INT_ATTR(dma_irq)
static DEF_ERROR_INT_ATTR(gamepack_dma_enable)
static DEF_ERROR_INT_ATTR(dma_repeat_enable)
static DEF_ERROR_INT_ATTR(unit_size)
static DEF_ERROR_INT_ATTR(number_of_units)

void gba_dmac_set_saddr(struct gba *gba, unsigned chan, uint32_t saddr) {
    LOG_DBG("DMAC: write %08x to DMA%uSAD\n", (unsigned)saddr, chan);
    gba->dmac.chans[chan].saddr = saddr;
}

uint32_t gba_dmac_get_saddr(struct gba *gba, unsigned chan) {
    return gba->dmac.chans[chan].saddr;
}

void gba_dmac_set_daddr(struct gba *gba, unsigned chan, uint32_t daddr) {
    LOG_DBG("DMAC: write %08x to DMA%uDAD\n", (unsigned)daddr, chan);
    gba->dmac.chans[chan].daddr = daddr;
}

uint32_t gba_dmac_get_daddr(struct gba *gba, unsigned chan) {
    return gba->dmac.chans[chan].daddr;
}

void gba_dmac_set_cnt_l(struct gba *gba, unsigned chan, uint16_t cnt_l) {
    LOG_DBG("DMAC: write %08x to DMA%uCNT_L\n", (unsigned)cnt_l, chan);
    gba->dmac.chans[chan].cnt_l = cnt_l;
}

uint16_t gba_dmac_get_cnt_l(struct gba *gba, unsigned chan) {
    return gba->dmac.chans[chan].cnt_l;
}

void gba_dmac_set_cnt_h(struct gba *gba, unsigned chan, uint16_t cnt_h) {
    LOG_DBG("DMAC: write %08x to DMA%uCNT_H\n", (unsigned)cnt_h, chan);
    struct gbadmac_chan *chanp = gba->dmac.chans + chan;

    uint32_t old_cnt_h = chanp->cnt_h;
    chanp->cnt_h = cnt_h;

    if (!(old_cnt_h & GBA_DMAC_ENABLE_BIT) && (cnt_h & GBA_DMAC_ENABLE_BIT))
        chanp->daddr_internal = chanp->daddr; // reload from user-visible daddr register

    if ((cnt_h & GBA_DMAC_START_TIMING_BITS) == GBA_DMAC_START_TIMING_SPECIAL &&
        // don't raise an error for unimplemented sound dma transfers
        !(((chan == 1 || chan == 2) &&
           (chanp->daddr_internal == 0x040000a0 || chanp->daddr_internal == 0x040000a4) &&
           !(chanp->cnt_h & (1 << 14))))) {
        LOG_ERROR("\"special\" dma start timing is not yet implemented.\n");

        error_set_dma_start_timing((cnt_h >> 12) & 3);
        error_set_src_addr(chanp->saddr);
        error_set_dst_addr(chanp->daddr_internal);
        error_set_dma_chan(chan);
        error_set_dma_irq((cnt_h >> 14) & 1);
        error_set_gamepack_dma_enable((cnt_h >> 11) & 1);
        error_set_dma_repeat_enable((cnt_h >> 9) & 1);
        error_set_number_of_units(chanp->cnt_l);
        error_set_unit_size(cnt_h & (1 << 10) ? 4 : 2);
        EMU_ERROR(ERROR_UNIMPLEMENTED);
    }

    if ((gba->dmac.chans[chan].cnt_h & GBA_DMAC_ENABLE_BIT) &&
        (cnt_h & GBA_DMAC_START_TIMING_BITS) == GBA_DMAC_START_TIMING_IMMED) {
        if (cnt_h & GBA_DMAC_REPEAT_BIT) {
            /*
             * we'd need to implement a delay for this
             * or else it would loop infinitely
             */
            LOG_ERROR("repeat bit with start timing is not supported.\n");

            error_set_dma_start_timing((cnt_h >> 12) & 3);
            error_set_src_addr(chanp->saddr);
            error_set_dst_addr(chanp->daddr_internal);
            error_set_dma_chan(chan);
            error_set_dma_irq((cnt_h >> 14) & 1);
            error_set_gamepack_dma_enable((cnt_h >> 11) & 1);
            error_set_dma_repeat_enable((cnt_h >> 9) & 1);
            error_set_number_of_units(chanp->cnt_l);
            error_set_unit_size(cnt_h & (1 << 10) ? 4 : 2);
            EMU_ERROR(ERROR_UNIMPLEMENTED);
        }
        gba_dma_xfer(gba, chan, chanp->cnt_l, chanp->saddr,
                     chanp->daddr_internal, chanp->cnt_h);
    }
}

uint16_t gba_dmac_get_cnt_h(struct gba *gba, unsigned chan) {
    return gba->dmac.chans[chan].cnt_h;
}

void gba_dmac_notify_hblank(struct gba *gba) {
    unsigned chan;
    for (chan = 0; chan < 4; chan++) {
        struct gbadmac_chan *chanp = gba->dmac.chans + chan;

        uint32_t ctrl = chanp->cnt_h;
        if ((ctrl & GBA_DMAC_ENABLE_BIT) &&
            (ctrl & GBA_DMAC_START_TIMING_BITS) == GBA_DMAC_START_TIMING_HBLANK) {
            gba_dma_xfer(gba, chan, chanp->cnt_l, chanp->saddr,
                         chanp->daddr_internal, chanp->cnt_h);
        }
    }
}

void gba_dmac_notify_vblank(struct gba *gba) {
    unsigned chan;
    for (chan = 0; chan < 4; chan++) {
        struct gbadmac_chan *chanp = gba->dmac.chans + chan;

        uint32_t ctrl = chanp->cnt_h;
        if ((ctrl & GBA_DMAC_ENABLE_BIT) &&
            (ctrl & GBA_DMAC_START_TIMING_BITS) == GBA_DMAC_START_TIMING_VBLANK) {
            gba_dma_xfer(gba, chan, chanp->cnt_l, chanp->saddr,
                         chanp->daddr_internal, chanp->cnt_h);
        }
    }
}

static void gba_dma_xfer(struct gba *gba, unsigned chan, unsigned n_units,
                         uint32_t src, uint32_t dst, unsigned ctrl) {
    if ((chan == 1 || chan == 2) &&
        (dst == 0x040000a0 || dst == 0x040000a4) &&
        !(ctrl & (1 << 14))) {
        // bit 14 of ctrl sets IRQ, so as long as that's clear there's no feedback needed
        LOG_ERROR("UNIMPLEMENTED SOUND DMA TRANSFER, SKIPPING FOR NOW.\n");
        return;
    }

    unsigned unit_sz = ctrl & (1 << 10) ? 4 : 2;

    if (!n_units)
        n_units = (chan == 3 ? 0x10000 : 0x4000);

    /*
     * unimplemented features:
     *     IRQ
     *     gamepack DMA
     */
    if ((ctrl & ((1 << 14) | (1 << 11))) ||
        !n_units ||
        (ctrl & GBA_DMAC_START_TIMING_BITS) == GBA_DMAC_START_TIMING_SPECIAL) {
        error_set_dma_start_timing((ctrl >> 12) & 3);
        error_set_src_addr(src);
        error_set_dst_addr(dst);
        error_set_dma_chan(chan);
        error_set_dma_irq((ctrl >> 14) & 1);
        error_set_gamepack_dma_enable((ctrl >> 11) & 1);
        error_set_dma_repeat_enable((ctrl >> 9) & 1);
        error_set_number_of_units(n_units);
        error_set_unit_size(unit_sz);
        EMU_ERROR(ERROR_UNIMPLEMENTED);
    }

    unsigned const actual_unit_sz = unit_sz;
    unsigned const actual_n_units = n_units;

    /*
     * if it's 4 bytes at a time, then just double the length
     * and make it 2 bytes at at a time
     */
    if (unit_sz == 4) {
        unit_sz = 2;
        n_units += n_units;
    }

    LOG_INFO("%s - DMA transfer %u bytes from %08x to %08x\n", __func__,
             (unsigned)(n_units * unit_sz), (unsigned)src, (unsigned)dst);

    while (n_units--) {
        uint16_t val;
        switch (memory_map_try_read_16(&gba->map, src, &val)) {
        case MEM_ACCESS_SUCCESS:
            break;
        case MEM_ACCESS_BADSIZE:
            error_set_src_addr(src);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
        case MEM_ACCESS_BADALIGN:
            error_set_src_addr(src);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
        default:
        case MEM_ACCESS_FAILURE:
            error_set_src_addr(src);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNMAPPED_READ);
        }

        switch (memory_map_try_write_16(&gba->map, dst, val)) {
        case MEM_ACCESS_SUCCESS:
            break;
        case MEM_ACCESS_BADSIZE:
            error_set_dst_addr(dst);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNSUPPORTED_WRITE_SIZE);
        case MEM_ACCESS_BADALIGN:
            error_set_dst_addr(dst);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNSUPPORTED_WRITE_ALIGN);
        default:
            error_set_dst_addr(dst);
            error_set_unit_size(actual_unit_sz);
            error_set_number_of_units(actual_n_units);
            EMU_ERROR(ERROR_UNMAPPED_WRITE);
        }

        // destination advancement
        switch ((ctrl >> 5) & 3) {
        case 0:
        case 3:
            dst += 2;
            break;
        case 1:
            dst -= 2;
            break;
        case 2:
            break;
        default:
            EMU_ERROR(ERROR_UNIMPLEMENTED);
        }

        // source advancement
        switch ((ctrl >> 7) & 3) {
        case 0:
            src += 2;
            break;
        case 1:
            src -= 2;
            break;
        case 2:
            break;
        default:
            EMU_ERROR(ERROR_UNIMPLEMENTED);
        }
    }

    struct gbadmac_chan *chanp = gba->dmac.chans + chan;
    if (((ctrl >> 5) & 3) == 3) {
        // reload from user-visible daddr register
        chanp->daddr_internal = chanp->daddr;
    }

    // clear enable bit if repeat bit is not set
    if (!(chanp->cnt_h & GBA_DMAC_REPEAT_BIT))
        chanp->cnt_h &= ~GBA_DMAC_ENABLE_BIT;
}
