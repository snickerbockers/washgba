/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <string.h>

#include "error.h"
#include "mem_code.h"

#include "gba.h"
#include "gbaio.h"
#include "gbaintc.h"
#include "gbadmac.h"
#include "gbavid.h"

static inline uint32_t gbaio_read32(struct gbaio *gbaio, unsigned idx) {
    if (idx >= GBAIO_LEN)
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    uint32_t retval;
    memcpy(&retval, gbaio->buf + idx, sizeof(retval));
    return retval;
}

static void gbaio_post_write(struct gba *gba, uint32_t addr, unsigned n_bytes) {
    struct gbaio *gbaio = &gba->io;
    uint32_t val32;
    unsigned idx = addr / 2;
    switch (idx * 2) {
    case 0:
        // DISPCNT
        gbavid_set_dispcnt(&gba->vid, gbaio->buf[idx]);
        break;
    case 4:
        gbavid_set_dispstat(&gba->vid, gbaio->buf[idx]);
        break;
    case 6:
        // vertical counter
        LOG_DBG("GBAIO: write %04x to VCOUNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 8:
        // bg0 control
        LOG_DBG("GBAIO: write %04x to BG0CNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0xa:
        // bg1 control
        LOG_DBG("GBAIO: write %04x to BG1CNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0xc:
        // bg2 control
        LOG_DBG("GBAIO: write %04x to BG2CNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0xe:
        // bg3 control
        LOG_DBG("GBAIO: write %04x to BG3CNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x10:
        // bg0 x-offset
        LOG_DBG("GBAIO: write %04x to BG0HOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x12:
        // bg0 y-offset
        LOG_DBG("GBAIO: write %04x to BG0VOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x14:
        // bg1 x-offset
        LOG_DBG("GBAIO: write %04x to BG1HOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x16:
        // bg1 y-offset
        LOG_DBG("GBAIO: write %04x to BG1VOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x18:
        // bg2 x-offset
        LOG_DBG("GBAIO: write %04x to BG2HOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x1a:
        // bg2 y-offset
        LOG_DBG("GBAIO: write %04x to BG2VOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x1c:
        // bg3 x-offset
        LOG_DBG("GBAIO: write %04x to BG3HOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x1e:
        // bg3 y-offset
        LOG_DBG("GBAIO: write %04x to BG3VOFS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x20:
        // BG2 Rotation/scaling parameter A
        LOG_DBG("GBAIO: write %04x to BG2PA\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x22:
        // BG2 rotation/scaling parameter B
        LOG_DBG("GBAIO: write %04x to BG2PB\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x24:
        // BG2 rotation/scaling parameter C
        LOG_DBG("GBAIO: write %04x to BG2PC\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x26:
        // BG2 rotation/scaling parameter D
        LOG_DBG("GBAIO: write %04x to BG2PD\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x28:
    case 0x2a:
        // BG2 reference point x-component
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        LOG_DBG("GBAIO: write %08x to BG2X\n", (unsigned)val32);
        break;
    case 0x2c:
    case 0x2e:
        // BG2 reference point y-component
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        LOG_DBG("GBAIO: write %08x to BG2Y\n", val32);
        break;
    case 0x30:
        // BG3 Rotation/scaling parameter A
        LOG_DBG("GBAIO: write %04x to BG3PA\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x32:
        // BG3 rotation/scaling parameter B
        LOG_DBG("GBAIO: write %04x to BG3PB\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x34:
        // BG3 rotation/scaling parameter C
        LOG_DBG("GBAIO: write %04x to BG3PC\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x36:
        // BG3 rotation/scaling parameter D
        LOG_DBG("GBAIO: write %04x to BG3PD\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x38:
    case 0x3a:
        // BG3 reference point x-component
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        LOG_DBG("GBAIO: write %08x to BG3X\n", (unsigned)val32);
        break;
    case 0x3c:
    case 0x3e:
        // BG3 reference point y-component
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        LOG_DBG("GBAIO: write %08x to BG3Y\n", (unsigned)val32);
        break;
    case 0x40:
        // window 0 width
        LOG_DBG("GBAIO: write %04x to WIN0H\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x42:
        // window 1 width
        LOG_DBG("GBAIO: write %04x to WIN1H\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x44:
        // window 0 height
        LOG_DBG("GBAIO: write %04x to WIN0V\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x46:
        // window 1 height
        LOG_DBG("GBAIO: write %04x to WIN1V\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x48:
        // inside of window 0 and 1
        LOG_DBG("GBAIO: write %04x to WININ\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x4a:
        // inside of obj window & outside of windows
        LOG_DBG("GBAIO: write %04x to WINOUT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x4c:
        // mosaic size
        LOG_DBG("GBAIO: write %04x to MOSAIC\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x50:
        // color special effects selection
        LOG_DBG("GBAIO: write %04x to BLDCNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x52:
        // alpha blending coefficients
        LOG_DBG("GBAIO: write %04x to BLDALPHA\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x54:
        // brightness coefficient
        LOG_DBG("GBAIO: write %04x to BLDY\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x82:
        LOG_DBG("GBAIO: write %04x to SOUNDCNT_H\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x84:
        LOG_DBG("GBAIO: write %04x to SOUNDCNT_X\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x88:
        LOG_DBG("GBAIO: write %04x to SOUNDBIAS\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0xb0:
    case 0xb2:
        // DMA channel 0 source address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_saddr(gba, 0, val32);
        break;
    case 0xb4:
    case 0xb6:
        // DMA channel 0 destination address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_daddr(gba, 0, val32);
        break;
    case 0xb8:
        // DMA channel 0 unit count (units are either 2 or 4 bytes)
        gba_dmac_set_cnt_l(gba, 0, gbaio->buf[idx]);
        break;
    case 0xba:
        // DMA channel 0 control
        gba_dmac_set_cnt_h(gba, 0, gbaio->buf[idx]);
        break;
    case 0xbc:
    case 0xbe:
        // DMA channel 1 source address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_saddr(gba, 1, val32);
        break;
    case 0xc0:
    case 0xc2:
        // DMA channel 1 destination address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_daddr(gba, 1, val32);
        break;
    case 0xc4:
        // DMA channel 1 unit count (units are either 2 or 4 bytes)
        gba_dmac_set_cnt_l(gba, 1, gbaio->buf[idx]);
        break;
    case 0xc6:
        // DMA channel 1 control
        gba_dmac_set_cnt_h(gba, 1, gbaio->buf[idx]);
        break;
    case 0xc8:
    case 0xca:
        // DMA channel 2 source address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_saddr(gba, 2, val32);
        break;
    case 0xcc:
    case 0xce:
        // DMA channel 2 destination address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_daddr(gba, 2, val32);
        break;
    case 0xd0:
        // DMA channel 2 unit count (units are either 2 or 4 bytes)
        gba_dmac_set_cnt_l(gba, 2, gbaio->buf[idx]);
        break;
    case 0xd2:
        // DMA channel 2 control
        gba_dmac_set_cnt_h(gba, 2, gbaio->buf[idx]);
        break;
    case 0xd4:
    case 0xd6:
        // DMA channel 3 source address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_saddr(gba, 3, val32);
        break;
    case 0xd8:
    case 0xda:
        // DMA channel 3 destination address
        memcpy(&val32, gbaio->buf + (idx & ~1), sizeof(val32));
        gba_dmac_set_daddr(gba, 3, val32);
        break;
    case 0xdc:
        // DMA channel 3 unit count (units are either 2 or 4 bytes)
        gba_dmac_set_cnt_l(gba, 3, gbaio->buf[idx]);
        break;
    case 0xde:
        // DMA channel 3 control
        gba_dmac_set_cnt_h(gba, 3, gbaio->buf[idx]);
        break;
    case 0x100:
        LOG_DBG("GBAIO: write %04x to TM0CNT_L\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x102:
        LOG_DBG("GBAIO: write %04x to TM0CNT_H\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x134:
        LOG_DBG("GBAIO: write %04x to RCNT\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x200:
        // interrupt enable, bits 0-13 control various irq sources
        LOG_DBG("GBAIO: write %04x to IE\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x202:
        // interrupt acknowledge/clear
        LOG_DBG("GBAIO: write %04x to IF\n", (unsigned)gbaio->buf[idx]);
        gbaintc_clear_irqs(&gba->intc, gbaio->buf[idx]);
        break;
    case 0x204:
        // gamepack waite-state control
        LOG_DBG("GBAIO: write %04x to WAITCNT\n", (unsigned)gbaio->buf[idx]);
        break;
    case 0x208:
        /*
         * master interrupt enable, it's active-high so interrupts are only
         * enabled when 1 is written to it.
         */
        LOG_DBG("GBAIO: write %04x to IME\n", (unsigned)gbaio->buf[idx]);
        gbaintc_set_ime(&gba->intc, gbaio->buf[idx] & 1);
        break;
    case 0x300:
        if (addr != 0x301)
            LOG_DBG("GBAIO: write %02x to POSTFLG\n", (unsigned)gbaio->buf[addr / 2]);
        if (addr == 0x301 || n_bytes > 1)
            LOG_DBG("GBAIO: write %02x to HALTCNT\n", (unsigned)(gbaio->buf[addr / 2] >> 8));
        break;
    default:
        LOG_DBG("GBAIO: unimplemented %u-bit register write %04x to io offset "
                "%08x\n", (unsigned)n_bytes * 8,
                (unsigned)gbaio->buf[idx], (unsigned)idx * 2);
    }
}

static inline void set32(void *dstp, uint32_t val) {
    memcpy(dstp, &val, sizeof(val));
}

static void gbaio_pre_read(struct gba *gba, uint32_t addr, unsigned n_bytes) {
    struct gbaio *gbaio = &gba->io;
    switch (addr & ~1) {
    case 0:
        gbaio->buf[addr / 2] = gbavid_get_dispcnt(&gba->vid);
        break;
    case 0x4:
        gbaio->buf[addr / 2] = gbavid_get_dispstat(&gba->vid);
        break;
    case 0x6:
        gbaio->buf[addr / 2] = gbavid_vcount(&gba->vid);
        LOG_DBG("GBAIO: read %04x from VCOUNT\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x82:
        LOG_DBG("GBAIO: read %04x from SOUNDCNT_H\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x84:
        LOG_DBG("GBAIO: read %04x from SOUNDCNT_X\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x88:
        LOG_DBG("GBAIO: read %04x from SOUNDBIAS\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0xb0:
    case 0xb2:
        // DMA channel 0 source address
        set32(gbaio->buf + addr / 2, gba_dmac_get_saddr(gba, 0));
        break;
    case 0xb4:
    case 0xb6:
        // DMA channel 0 destination address
        set32(gbaio->buf + addr / 2, gba_dmac_get_daddr(gba, 0));
        break;
    case 0xb8:
        // DMA channel 0 unit count (units are either 2 or 4 bytes)
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_l(gba, 0);
        break;
    case 0xba:
        // DMA channel 0 control register
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_h(gba, 0);
        break;
    case 0xbc:
    case 0xbe:
        // DMA channel 1 source address
        set32(gbaio->buf + addr / 2, gba_dmac_get_saddr(gba, 1));
        break;
    case 0xc0:
    case 0xc2:
        // DMA channel 1 destination address
        set32(gbaio->buf + addr / 2, gba_dmac_get_daddr(gba, 1));
        break;
    case 0xc4:
        // DMA channel 1 unit count (units are either 2 or 4 bytes)
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_l(gba, 1);
        break;
    case 0xc6:
        // DMA channel 1 control register
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_h(gba, 1);
        break;
    case 0xc8:
    case 0xca:
        // DMA channel 2 source address
        set32(gbaio->buf + addr / 2, gba_dmac_get_saddr(gba, 2));
        break;
    case 0xcc:
    case 0xce:
        // DMA channel 2 destination address
        set32(gbaio->buf + addr / 2, gba_dmac_get_daddr(gba, 2));
        break;
    case 0xd0:
        // DMA channel 2 unit count (units are either 2 or 4 bytes)
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_l(gba, 2);
        break;
    case 0xd2:
        // DMA channel 2 control register
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_h(gba, 2);
        break;
    case 0xd4:
    case 0xd6:
        // DMA channel 3 source address
        set32(gbaio->buf + addr / 2, gba_dmac_get_saddr(gba, 3));
        break;
    case 0xd8:
    case 0xda:
        // DMA channel 3 destination address
        set32(gbaio->buf + addr / 2, gba_dmac_get_daddr(gba, 3));
        break;
    case 0xdc:
        // DMA channel 3 unit count (units are either 2 or 4 bytes)
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_l(gba, 3);
        break;
    case 0xde:
        // DMA channel 3 control register
        gbaio->buf[addr / 2] = gba_dmac_get_cnt_h(gba, 3);
        break;
    case 0x130:
        gbaio->buf[addr / 2] = ~gba->btns; // buttons are active-low
        LOG_DBG("GBAIO: read %04x from KEYINPUT\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x134:
        LOG_DBG("GBAIO: read %04x from RCNT\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x200:
        LOG_DBG("GBAIO: read %04x from IE\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x202:
        // interrupt acknowledge/clear
        gbaio->buf[addr / 2] = gbaintc_get_irqs(&gba->intc);
        LOG_DBG("GBAIO: read %04x from IF\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x204:
        LOG_DBG("GBAIO: READ %04x from WAITCNT\n", (unsigned)gbaio->buf[addr / 2]);
        break;
    case 0x300:
        if (addr != 0x301)
            LOG_DBG("GBAIO: read %02x from POSTFLG\n", (unsigned)gbaio->buf[addr / 2]);
        if (addr == 0x301 || n_bytes > 1)
            LOG_DBG("GBAIO: read %02x from HALTCNT\n", (unsigned)(gbaio->buf[addr / 2] >> 8));
        break;
    default:
        LOG_DBG("GBAIO: unimplemented %u-bit register read from offset "
                "%08x\n", (unsigned)n_bytes * 8, (unsigned)addr);
    }
}

static int gbaio_try_read8(uint32_t addr, uint8_t *val, void *ctxt) {
    if (addr <= GBAIO_LEN * 2 - 1) {
        struct gba *gba = ctxt;
        struct gbaio *gbaio = &gba->io;
        gbaio_pre_read(gba, addr, 1);
        memcpy(val, ((uint8_t*)gbaio->buf) + addr, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaio_try_write8(uint32_t addr, uint8_t val, void *ctxt) {
    if (addr <= GBAIO_LEN * 2 - 1) {
        struct gba *gba = ctxt;
        struct gbaio *gbaio = &gba->io;
        memcpy(((uint8_t*)gbaio->buf) + addr, &val, sizeof(val));
        gbaio_post_write(gba, addr, 1);
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaio_try_read16(uint32_t addr, uint16_t *val, void *ctxt) {
    uint32_t idx = addr / 2;
    if (addr & 1)
        return MEM_ACCESS_BADALIGN;
    else if (idx <= GBAIO_LEN - 1) {
        struct gba *gba = ctxt;
        struct gbaio *gbaio = &gba->io;
        gbaio_pre_read(gba, addr, 2);
        *val = gbaio->buf[idx];
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaio_try_write16(uint32_t addr, uint16_t val, void *ctxt) {
    uint32_t idx = addr / 2;
    if (addr & 1)
        return MEM_ACCESS_BADALIGN;
    else if (idx <= GBAIO_LEN - 1) {
        struct gba *gba = ctxt;
        struct gbaio *gbaio = &gba->io;
        gbaio->buf[idx] = val;
        gbaio_post_write(gba, addr, 2);
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaio_try_read32(uint32_t addr, uint32_t *val, void *ctxt) {
    struct gba *gba = ctxt;
    struct gbaio *gbaio = &gba->io;
    uint32_t idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAIO_LEN - 2) {
        gbaio_pre_read(gba, addr, 2);
        gbaio_pre_read(gba, addr + 2, 2);
        memcpy(val, gbaio->buf + idx, sizeof(*val));
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(4);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

static int gbaio_try_write32(uint32_t addr, uint32_t val, void *ctxt) {
    struct gba *gba = ctxt;
    struct gbaio *gbaio = &gba->io;
    uint32_t idx = addr / 2;
    if (addr & 3) {
        return MEM_ACCESS_BADALIGN;
    } else if (idx <= GBAIO_LEN - 2) {
        memcpy(gbaio->buf + idx, &val, sizeof(val));
        gbaio_post_write(gba, addr, 2);
        gbaio_post_write(gba, addr + 2, 2);
        return MEM_ACCESS_SUCCESS;
    } else {
        /*
         * somehow the memory_map screwed up or was not configured properly;
         * this should be impossible
         */
        error_set_address(addr);
        error_set_length(4);
        CRITICAL_ERROR(ERROR_MEM_OUT_OF_BOUNDS);
    }
}

struct memory_interface const gbaio_memory_interface = {
    .try_read32 = gbaio_try_read32,
    .try_read16 = gbaio_try_read16,
    .try_read8 = gbaio_try_read8,

    .try_write32 = gbaio_try_write32,
    .try_write16 = gbaio_try_write16,
    .try_write8 = gbaio_try_write8
};

void gbaio_init(struct gbaio *gbaio) {
    memset(gbaio, 0, sizeof(*gbaio));
}

void gbaio_cleanup(struct gbaio *gbaio) {
}

