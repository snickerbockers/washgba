/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2021, 2022 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stddef.h>
#include <string.h>

#include "gbaintc.h"

// the arm7 calls this to see if there's an irq pending
static bool irq_fn(void *ctxt);

void gbaintc_init(struct gbaintc *intc, struct arm7 *cpu) {
    memset(intc, 0, sizeof(*intc));

    intc->cpu = cpu;
    arm7_set_irq(cpu, irq_fn, intc);
}

void gbaintc_cleanup(struct gbaintc *intc) {
    arm7_set_irq(intc->cpu, NULL, NULL);
}

bool gbaintc_set_ime(struct gbaintc *intc, bool ime) {
    intc->ime = ime;
    arm7_excp_refresh(intc->cpu);
}

static bool irq_fn(void *ctxt) {
    struct gbaintc *intc = ctxt;

    // it is not an accident that this is '&' rather than '&&'
    return intc->ime & intc->irq_lines;
}

void gbaintc_raise_irq(struct gbaintc *intc, unsigned irq) {
    intc->irq_lines |= irq;
    arm7_excp_refresh(intc->cpu);
}

void gbaintc_raise_irq_outside_cpu_context(struct gbaintc *intc, unsigned irq) {
    intc->irq_lines |= irq;
    arm7_excp_refresh_outside_cpu_context(intc->cpu);
}

uint16_t gbaintc_get_irqs(struct gbaintc *intc) {
    return intc->irq_lines;
}

void gbaintc_clear_irqs(struct gbaintc *intc, uint16_t irq) {
    intc->irq_lines &= ~irq;
    arm7_excp_refresh(intc->cpu);
}
