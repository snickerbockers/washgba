/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2018, 2019, 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef ARM7_H_
#define ARM7_H_

#include <stdbool.h>
#include <assert.h>

#include "error.h"
#include "sched.h"
#include "memory_map.h"
#include "arm7_reg_idx.h"
#include "mem_code.h"
#include "sched.h"

#define ARM7_CLOCK_SCALE 1 // TODO: this

// negative/less-than
#define ARM7_CPSR_N_SHIFT 31
#define ARM7_CPSR_N_MASK (1 << ARM7_CPSR_N_SHIFT)

// zero
#define ARM7_CPSR_Z_SHIFT 30
#define ARM7_CPSR_Z_MASK (1 << ARM7_CPSR_Z_SHIFT)

// carry/borrow/extend
#define ARM7_CPSR_C_SHIFT 29
#define ARM7_CPSR_C_MASK (1 << ARM7_CPSR_C_SHIFT)

// overflow
#define ARM7_CPSR_V_SHIFT 28
#define ARM7_CPSR_V_MASK (1 << ARM7_CPSR_V_SHIFT)

// IRQ disable
#define ARM7_CPSR_I_SHIFT 7
#define ARM7_CPSR_I_MASK (1 << ARM7_CPSR_I_SHIFT)

// FIQ disable
#define ARM7_CPSR_F_SHIFT 6
#define ARM7_CPSR_F_MASK (1 << ARM7_CPSR_F_SHIFT)

#define ARM7_CPSR_T_SHIFT 5
#define ARM7_CPSR_T_MASK (1 << ARM7_CPSR_T_SHIFT)

// CPU mode
#define ARM7_CPSR_M_SHIFT 0
#define ARM7_CPSR_M_MASK (0x1f << ARM7_CPSR_M_SHIFT)

#define ARM7_CPSR_NZCV_MASK (ARM7_CPSR_N_MASK | ARM7_CPSR_Z_MASK |  \
                             ARM7_CPSR_C_MASK | ARM7_CPSR_V_MASK)

// bits to clear when writing to R15/PC in thumb-mode
#define THUMB_R15_WRITE_MASK (~1)

enum arm7_mode {
    ARM7_MODE_USER = (0x10 << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_FIQ  = (0x11 << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_IRQ  = (0x12 << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_SVC  = (0x13 << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_ABT  = (0x17 << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_UND  = (0x1b << ARM7_CPSR_M_SHIFT),
    ARM7_MODE_SYS  = (0x1f << ARM7_CPSR_M_SHIFT)
};

/*
 * ARM7DI-type CPU wired into the AICA sound system.
 *
 * Like the SH4, it supports both little-endian and big-endian byte orders.
 * AFAIK, this is always little-endian on the Dreamcast.  Documentation seems to
 * indicate the endianess is set by an external pin on the CPU, and that is
 * hopefully hardwired into LE mode.
 */

enum arm7_excp {
    ARM7_EXCP_NONE = 0,
    ARM7_EXCP_RESET = 1,
    ARM7_EXCP_DATA_ABORT = 2,
    ARM7_EXCP_FIQ = 4,
    ARM7_EXCP_IRQ = 8,
    ARM7_EXCP_PREF_ABORT = 16,
    ARM7_EXCP_SWI = 32
};

typedef uint32_t arm7_inst;

typedef bool(*arm7_irq_fn)(void *dat);

struct arm7 {
    struct dc_clock *clk;
    struct memory_map *map;

    uint32_t reg[ARM7_REGISTER_COUNT];

    unsigned extra_cycles;

    /*
     * One oddity about ARM7 (compared to saner CPUs like x86 and SH4) is that
     * the CPU does not hide its pipelining from software.  The Program Counter
     * register (ARM7_REG_R15) always points to the instruction being fetched;
     * since there's a 3-stage pipeline which is *not* hidden from software,
     * that means that ARM7_REG_R15 always points two instructions ahead of the
     * instruction being executed.
     *
     * For the sake of simplicity, this interpreter will actually mimic this
     * design by buffering three instructions in a fake "pipeline".  pipeline[2]
     * buffers the execution stage (ARM7_REG_R15 - 8), pipeline[1] buffers the
     * decoding stage (ARM7_REG_R15 - 4), and pipeline[0] buffers the fetch
     * stage (ARM7_REG_R15).  Instructions are actually fetched two cycles ahead
     * of their execution like in a real ARM, but the decoding isn't done until
     * it's at the execution stage.
     */
    arm7_inst pipeline[3];
    uint32_t pipeline_pc[3];

    enum arm7_excp excp;

    arm7_irq_fn fiq_line, irq_line;
    void *fiq_line_arg, *irq_line_arg;

    struct SchedEvent excp_refresh_event;
    bool excp_refresh_event_scheduled;
};

void arm7_init(struct arm7 *arm7, struct dc_clock *clk);
void arm7_cleanup(struct arm7 *arm7);

void arm7_set_irq(struct arm7 *arm7, arm7_irq_fn handler, void *argp);
void arm7_set_fiq(struct arm7 *arm7, arm7_irq_fn handler, void *argp);

void arm7_set_mem_map(struct arm7 *arm7, struct memory_map *arm7_mem_map);

void arm7_reset(struct arm7 *arm7, bool val);

void arm7_get_regs(struct arm7 *arm7, void *dat_out);

uint32_t arm7_pc_next(struct arm7 *arm7);


/*
 * it's not safe to refresh the ARM7 irqs from within
 * CPU context so arm7_excp_refresh will schedule a
 * special helper event that executes immediately and
 * refreshes the ARM7 irqs from outside of CPU context.
 *
 * If you're already outside of CPU context, you can save
 * a little time by calling arm7_excp_refresh_outside_cpu_context instead.
 *
 * Do not ever call arm7_excp_refresh_outside_cpu_context from within
 * CPU context.
 */
void arm7_excp_refresh(struct arm7 *arm7);
void arm7_excp_refresh_outside_cpu_context(struct arm7 *arm7);

ERROR_INT_ATTR(arm7_execution_mode);

typedef unsigned(*arm7_op_fn)(struct arm7*,arm7_inst);
arm7_op_fn arm7_decode(struct arm7 *arm7, arm7_inst inst);

static inline uint32_t arm7_do_fetch_inst(struct arm7 *arm7, uint32_t addr);

static inline bool arm7_thumb_mode(struct arm7 const *arm7) {
    return arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_T_MASK;
}

/*
 * call this when something like a branch or exception happens that invalidates
 * instructions in the pipeline.
 *
 * This won't effect the PC, but it will clear out anything already in the
 * pipeline.  What that means is that anything in the pipeline which hasn't
 * been executed yet will get trashed.  The upshot of this is that it's only
 * safe to call arm7_reset_pipeline when the PC has actually changed.
 */
static inline void arm7_reset_pipeline(struct arm7 *arm7) {
    uint32_t pc = arm7->reg[ARM7_REG_PC];
    if (arm7_thumb_mode(arm7)) {
        arm7->extra_cycles = 2;

        arm7->pipeline_pc[1] = pc;
        arm7->pipeline[1] = arm7_do_fetch_inst(arm7, pc);

        arm7->pipeline_pc[0] = pc + 2;
        arm7->pipeline[0] = arm7_do_fetch_inst(arm7, pc + 2);

        pc += 4;
        arm7->reg[ARM7_REG_PC] = pc;
    } else {
        arm7->extra_cycles = 2;

        arm7->pipeline_pc[1] = pc;
        arm7->pipeline[1] = arm7_do_fetch_inst(arm7, pc);

        arm7->pipeline_pc[0] = pc + 4;
        arm7->pipeline[0] = arm7_do_fetch_inst(arm7, pc + 4);

        pc += 8;
        arm7->reg[ARM7_REG_PC] = pc;
    }
}

static inline uint32_t arm7_do_fetch_inst(struct arm7 *arm7, uint32_t addr) {
    if (arm7_thumb_mode(arm7)) {
        uint16_t inst;
        switch (memory_map_try_read_16(arm7->map, addr, &inst)) {
        case MEM_ACCESS_SUCCESS:
            return inst;
        case MEM_ACCESS_BADSIZE:
            error_set_address(addr);
            error_set_length(2);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
        case MEM_ACCESS_BADALIGN:
            error_set_address(addr);
            error_set_length(2);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
        default:
        case MEM_ACCESS_FAILURE:
            error_set_address(addr);
            error_set_length(2);
            EMU_ERROR(ERROR_UNMAPPED_READ);
        }
        return inst;
    } else {
        uint32_t inst;
        switch (memory_map_try_read_32(arm7->map, addr, &inst)) {
        case MEM_ACCESS_SUCCESS:
            return inst;
        case MEM_ACCESS_BADSIZE:
            error_set_address(addr);
            error_set_length(4);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
        case MEM_ACCESS_BADALIGN:
            error_set_address(addr);
            error_set_length(4);
            EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
        default:
        case MEM_ACCESS_FAILURE:
            error_set_address(addr);
            error_set_length(4);
            EMU_ERROR(ERROR_UNMAPPED_READ);
        }
        return inst;
    }
}

static inline arm7_inst arm7_fetch_inst(struct arm7 *arm7, int *extra_cycles) {
    uint32_t pc = arm7->reg[ARM7_REG_PC];

    arm7_inst inst_fetched = arm7_do_fetch_inst(arm7, pc);
    uint32_t newpc = arm7->pipeline_pc[0];
    arm7_inst newinst = arm7->pipeline[0];
    arm7_inst ret = arm7->pipeline[1];
    uint32_t ret_pc = arm7->pipeline_pc[1];

    arm7->pipeline_pc[0] = pc;
    arm7->pipeline[0] = inst_fetched;
    arm7->pipeline_pc[1] = newpc;
    arm7->pipeline[1] = newinst;
    arm7->pipeline_pc[2] = ret_pc;
    arm7->pipeline[2] = ret;

    *extra_cycles = arm7->extra_cycles;
    arm7->extra_cycles = 0;

    return ret;
}

ERROR_U32_ATTR(arm7_cpu_mode);

inline static uint32_t *
arm7_gen_reg_bank(struct arm7 *arm7, unsigned reg, unsigned bank) {
    unsigned curmode = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK;
    if (curmode == bank ||
        (curmode == ARM7_MODE_USER && bank == ARM7_MODE_SYS) ||
        (curmode == ARM7_MODE_SYS && bank == ARM7_MODE_USER)) {
        return arm7->reg + ARM7_REG_R0 + reg;
    }

    switch (bank) {
    case ARM7_MODE_USER:
    case ARM7_MODE_SYS:
        if (curmode == ARM7_MODE_FIQ) {
            if (reg < 8 || reg == 15)
                return arm7->reg + ARM7_REG_R0 + reg;
            else
                return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        } else {
            if (reg != 13 && reg != 14)
                return arm7->reg + ARM7_REG_R0 + reg;
            else
                return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        }
    case ARM7_MODE_FIQ:
        if (reg >= 8 && reg <= 14)
            return arm7->reg + ARM7_REG_R8_FIQ + (reg - 8);
        else
            return arm7->reg + ARM7_REG_R0 + reg;
    case ARM7_MODE_IRQ:
        if (reg == 13 || reg == 14) {
            return arm7->reg + ARM7_REG_R13_IRQ + (reg - 13);
        } else if (curmode == ARM7_MODE_FIQ && (reg >= 8 && reg <= 14)) {
            return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        } else {
            return arm7->reg + ARM7_REG_R0 + reg;
        }
    case ARM7_MODE_SVC:
        if (reg == 13 || reg == 14) {
            return arm7->reg + ARM7_REG_R13_SVC + (reg - 13);
        } else if (curmode == ARM7_MODE_FIQ && (reg >= 8 && reg <= 14)) {
            return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        } else {
            return arm7->reg + ARM7_REG_R0 + reg;
        }
   case ARM7_MODE_ABT:
        if (reg == 13 || reg == 14) {
            return arm7->reg + ARM7_REG_R13_ABT + (reg - 13);
        } else if (curmode == ARM7_MODE_FIQ && (reg >= 8 && reg <= 14)) {
            return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        } else {
            return arm7->reg + ARM7_REG_R0 + reg;
        }
    case ARM7_MODE_UND:
        if (reg == 13 || reg == 14) {
            return arm7->reg + ARM7_REG_R13_UND + (reg - 13);
        } else if (curmode == ARM7_MODE_FIQ && (reg >= 8 && reg <= 14)) {
            return arm7->reg + ARM7_REG_R8_BANK + (reg - 8);
        } else {
            return arm7->reg + ARM7_REG_R0 + reg;
        }
    default:
        error_set_arm7_cpu_mode(bank);
        EMU_ERROR(ERROR_UNKNOWN_CPU_MODE);
    }
}

inline static uint32_t *arm7_gen_reg(struct arm7 *arm7, unsigned reg) {
    return arm7->reg + reg + ARM7_REG_R0;
}

inline static void
arm7_gen_reg_write(struct arm7 *arm7, unsigned reg, uint32_t val) {
    if (reg == 15 && arm7_thumb_mode(arm7))
        *arm7_gen_reg(arm7, reg) = val & THUMB_R15_WRITE_MASK;
    else
        *arm7_gen_reg(arm7, reg) = val;
}

inline static void
arm7_gen_reg_bank_write(struct arm7 *arm7, unsigned reg,
                        unsigned bank, unsigned val) {
    if (reg == 15 && arm7_thumb_mode(arm7))
        *arm7_gen_reg_bank(arm7, reg, bank) = val & THUMB_R15_WRITE_MASK;
    else
        *arm7_gen_reg_bank(arm7, reg, bank) = val;
}

static inline bool arm7_cond_eq(struct arm7 *arm7) {
    return (bool)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_Z_MASK);
}

static inline bool arm7_cond_ne(struct arm7 *arm7) {
    return !arm7_cond_eq(arm7);
}

static inline bool arm7_cond_cs(struct arm7 *arm7) {
    return (bool)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_C_MASK);
}

static inline bool arm7_cond_cc(struct arm7 *arm7) {
    return !arm7_cond_cs(arm7);
}

static inline bool arm7_cond_mi(struct arm7 *arm7) {
    return (bool)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_N_MASK);
}

static inline bool arm7_cond_pl(struct arm7 *arm7) {
    return !arm7_cond_mi(arm7);
}

static inline bool arm7_cond_vs(struct arm7 *arm7) {
    return (bool)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_V_MASK);
}

static inline bool arm7_cond_vc(struct arm7 *arm7) {
    return !arm7_cond_vs(arm7);
}

static inline bool arm7_cond_hi(struct arm7 *arm7) {
    return arm7_cond_ne(arm7) && arm7_cond_cs(arm7);
}

static inline bool arm7_cond_ls(struct arm7 *arm7) {
    return arm7_cond_cc(arm7) || arm7_cond_eq(arm7);
}

static inline bool arm7_cond_ge(struct arm7 *arm7) {
    return arm7_cond_mi(arm7) == arm7_cond_vs(arm7);
}

static inline bool arm7_cond_lt(struct arm7 *arm7) {
    return !arm7_cond_ge(arm7);
}

static inline bool arm7_cond_gt(struct arm7 *arm7) {
    return arm7_cond_ne(arm7) && arm7_cond_ge(arm7);
}

static inline bool arm7_cond_le(struct arm7 *arm7) {
    return !arm7_cond_gt(arm7);
}

static inline bool arm7_cond_al(struct arm7 *arm7) {
    return true;
}

static inline bool arm7_cond_nv(struct arm7 *arm7) {
    return false;
}

static inline bool arm7_eval_cond(struct arm7 *arm7, unsigned cond) {
    switch (cond & 0xf) {
    case 0:
        return arm7_cond_eq(arm7);
    case 1:
        return arm7_cond_ne(arm7);
    case 2:
        return arm7_cond_cs(arm7);
    case 3:
        return arm7_cond_cc(arm7);
    case 4:
        return arm7_cond_mi(arm7);
    case 5:
        return arm7_cond_pl(arm7);
    case 6:
        return arm7_cond_vs(arm7);
    case 7:
        return arm7_cond_vc(arm7);
    case 8:
        return arm7_cond_hi(arm7);
    case 9:
        return arm7_cond_ls(arm7);
    case 10:
        return arm7_cond_ge(arm7);
    case 11:
        return arm7_cond_lt(arm7);
    case 12:
        return arm7_cond_gt(arm7);
    case 13:
        return arm7_cond_le(arm7);
    case 14:
        return arm7_cond_al(arm7);
    case 15:
        EMU_ERROR(ERROR_UNIMPLEMENTED);
    }
}

void next_inst(struct arm7 *arm7);

/*
 * Used to weigh different types of cycles.
 *
 * TODO: I think the different cycle types refer to different clocks (CPU clock,
 * memory clock, etc).  I'm not sure how fast these are relative to each other,
 * so for now I weigh them all equally.
 *
 * see chapter 5.0 (Memory Interface) of the data sheet.
 */
#define S_CYCLE 1 // access address at or one word after previous address.
#define N_CYCLE 1 // access address with no relation to previous address.
#define I_CYCLE 1

/*
 * run one instruction and return number of cycles executed
 * this might just end up running an empty fetch that doesn't
 * do anything, but even that will return a nonzero cycle count.
 */
unsigned arm7_exec_inst(struct arm7 *arm7);

#endif
