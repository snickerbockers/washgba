/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBA_ONCHIP_RAM_H_
#define GBA_ONCHIP_RAM_H_

#include "memory_map.h"

#define GBA_ONCHIP_RAM_FIRST 0x03000000
#define GBA_ONCHIP_RAM_LAST 0x03007FFF
#define GBA_ONCHIP_RAM_LEN (GBA_ONCHIP_RAM_LAST - GBA_ONCHIP_RAM_FIRST + 1)

struct gba_onchip_ram {
    char mem[GBA_ONCHIP_RAM_LEN];
};

void gba_onchip_ram_init(struct gba_onchip_ram *ram);
void gba_onchip_ram_cleanup(struct gba_onchip_ram *ram);

extern struct memory_interface const gba_onchip_ram_memory_interface;

#endif
