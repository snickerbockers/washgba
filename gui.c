/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "bitmap_font.h"
#include "wash_glptr.h"
#include "shader.h"
#include "mat.h"

#include "gui.h"

#define VERT_LEN 10

#define FONT_GLYPHS_X 13
#define FONT_GLYPHS_Y 2

static void init_shader(struct shader *sh, char const *vs, char const *fs);
static void cleanup_shader(struct shader *sh);

static void mesh_init(struct mesh *mesh, struct shader const *sh,
                      GLfloat const *verts, unsigned n_verts,
                      GLuint const *indices, unsigned n_indices);
static void mesh_cleanup(struct mesh *mesh);

static char const *vert_shader_src_tex =
    "#version 330\n"
    "\n"
    "in vec4 vert_pos;\n"
    "in vec2 texcoord;\n"
    "uniform mat4 trans_mat;\n"
    "uniform mat4 tex_mat;\n"
    "\n"
    "out vec2 uv;\n"
    "\n"
    "void main() {\n"
    "    gl_Position = trans_mat * vert_pos;\n"
    "    uv = (tex_mat * vec4(texcoord, 0, 1)).xy;\n"
    "}\n";
static char const *frag_shader_src_tex =
    "#version 330\n"
    "\n"
    "uniform sampler2D bound_tex;\n"
    "\n"
    "in vec2 uv;\n"
    "out vec4 color;\n"
    "void main() { color = texture(bound_tex, uv); }\n";

static char const *vert_shader_src_untex =
    "#version 330\n"
    "\n"
    "in vec4 vert_color;\n"
    "in vec4 vert_pos;\n"
    "uniform mat4 trans_mat;\n"
    "uniform mat4 color_mat;\n"
    "\n"
    "out vec2 uv;\n"
    "out vec4 rgba;\n"
    "\n"
    "void main() {\n"
    "    gl_Position = trans_mat * vert_pos;\n"
    "    rgba = color_mat * vert_color;\n"
    "}\n";
static char const *frag_shader_src_untex =
    "#version 330\n"
    "\n"
    "uniform sampler2D bound_tex;\n"
    "\n"
    "in vec2 uv;\n"
    "in vec4 rgba;\n"
    "out vec4 color;\n"
    "void main() { color = rgba; }\n";

void gui_init(struct gui *gui, float width, float height) {
    memset(gui, 0, sizeof(*gui));

    gui->width = width;
    gui->height = height;

    init_shader(&gui->tex_shader, vert_shader_src_tex, frag_shader_src_tex);
    init_shader(&gui->untex_shader,
                vert_shader_src_untex, frag_shader_src_untex);

    unsigned glyph_w = bmp_font.width / FONT_GLYPHS_X;
    unsigned glyph_h = bmp_font.height / FONT_GLYPHS_Y;

    GLuint glyph_indices[] = { 0, 1, 2, 0, 2, 3 };
    GLfloat glyph_verts[] = {
        0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f
    };

    mesh_init(&gui->tex_mesh, &gui->tex_shader,
              glyph_verts, 4, glyph_indices, 6);
    mesh_init(&gui->untex_mesh, &gui->untex_shader,
              glyph_verts, 4, glyph_indices, 6);

    pglGenTextures(1, &gui->font.tex);
    pglBindTexture(GL_TEXTURE_2D, gui->font.tex);
    pglTexImage2D(GL_TEXTURE_2D, 0,
                  bmp_font.bytes_per_pixel == 4 ? GL_RGBA : GL_RGB,
                  bmp_font.width, bmp_font.height, 0,
                  bmp_font.bytes_per_pixel == 4 ? GL_RGBA : GL_RGB,
                  bmp_font.bytes_per_pixel == 2 ? GL_UNSIGNED_SHORT_5_6_5 : GL_UNSIGNED_BYTE,
                  bmp_font.pixel_data);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    pglTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    GLuint unit_indices[] = { 0, 1, 2, 0, 2, 3 };
    GLfloat unit_verts[] = {
        -1.0f, -1.0f, 0.0f, 1.0f, 0.0, 1.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 1.0, 1.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0, 0.0,
        -1.0f,  1.0f, 0.0f, 1.0f, 0.0, 0.0
    };
}

void gui_cleanup(struct gui *gui) {
    pglDeleteTextures(1, &gui->font.tex);

    mesh_cleanup(&gui->tex_mesh);
    mesh_cleanup(&gui->untex_mesh);

    cleanup_shader(&gui->tex_shader);
    cleanup_shader(&gui->untex_shader);
}

static void do_draw_quad(struct gui *gui, struct shader *sh,
                         GLfloat const proj_mat[16],
                         GLfloat const pos[2], GLfloat const size[2],
                         GLfloat const uv_pos[2], GLfloat const uv_size[2],
                         GLfloat const color[4]) {
    GLfloat trans_mat[16], mview_mat[16], tex_mat[16], color_mat[16];

    mat_scale(color_mat, color[0], color[1], color[2], color[3]);

    mview_mat[0] = size[0];
    mview_mat[1] = 0.0f;
    mview_mat[2] = 0.0f;
    mview_mat[3] = pos[0] - 0.5f * gui->width;
    mview_mat[4] = 0.0f;
    mview_mat[5] = size[1];
    mview_mat[6] = 0.0f;
    mview_mat[7] = gui->height * 0.5f - pos[1] - size[1];
    mview_mat[8] = 0.0f;
    mview_mat[9] = 0.0f;
    mview_mat[10] = 1.0f;
    mview_mat[11] = 0.0f;
    mview_mat[12] = 0.0f;
    mview_mat[13] = 0.0f;
    mview_mat[14] = 0.0f;
    mview_mat[15] = 1.0f;

    mat_mult(trans_mat, mview_mat, proj_mat);
    pglUniformMatrix4fv(sh->trans_mat_slot, 1, GL_TRUE, trans_mat);

    if (uv_pos && uv_size) {
        tex_mat[0] = uv_size[0];
        tex_mat[1] = 0.0f;
        tex_mat[2] = 0.0f;
        tex_mat[3] = uv_pos[0];
        tex_mat[4] = 0.0f;
        tex_mat[5] = uv_size[1];
        tex_mat[6] = 0.0f;
        tex_mat[7] = uv_pos[1];
        tex_mat[8] = 0.0f;
        tex_mat[9] = 0.0f;
        tex_mat[10] = 1.0f;
        tex_mat[11] = 0.0f;
        tex_mat[12] = 0.0f;
        tex_mat[13] = 0.0f;
        tex_mat[14] = 0.0f;
        tex_mat[15] = 1.0f;
        pglUniformMatrix4fv(sh->tex_mat_slot, 1, GL_TRUE, tex_mat);
    }

    pglUniformMatrix4fv(sh->color_mat_slot, 1, GL_TRUE, color_mat);

    pglDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void
gui_draw_quad(struct gui *gui, GLuint tex, GLfloat const pos[2],
              GLfloat const sz[2], GLfloat const uv_pos[2],
              GLfloat const uv_sz[2]) {
    pglActiveTexture(GL_TEXTURE0);
    pglBindTexture(GL_TEXTURE_2D, tex);

    pglUseProgram(gui->tex_shader.prog);
    pglUniform1i(gui->tex_shader.bound_tex_slot, 0);

    GLfloat proj_mat[16];
    GLfloat proj_min[2] = { gui->width * -0.5f, gui->height * -0.5f };
    GLfloat proj_max[2] = { gui->width * 0.5f, gui->height * 0.5f };
    GLfloat color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

    mat_ortho2d(proj_mat, proj_min, proj_max);

    pglBindVertexArray(gui->tex_mesh.vao);
    pglEnableVertexAttribArray(gui->tex_shader.vert_pos_slot);
    pglEnableVertexAttribArray(gui->tex_shader.vert_tex_coord_slot);

    do_draw_quad(gui, &gui->tex_shader, proj_mat, pos, sz, uv_pos, uv_sz, color);
}

void
gui_draw_untextured_quad(struct gui *gui,
                         GLfloat const pos[2], GLfloat const sz[2],
                         GLfloat const color[4]) {
    pglActiveTexture(GL_TEXTURE0);
    pglBindTexture(GL_TEXTURE_2D, 0);

    pglUseProgram(gui->untex_shader.prog);
    pglUniform1i(gui->untex_shader.bound_tex_slot, 0);

    GLfloat proj_mat[16];
    GLfloat proj_min[2] = { gui->width * -0.5f, gui->height * -0.5f };
    GLfloat proj_max[2] = { gui->width * 0.5f, gui->height * 0.5f };

    mat_ortho2d(proj_mat, proj_min, proj_max);

    pglBindVertexArray(gui->untex_mesh.vao);
    pglBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui->untex_mesh.ebo);
    pglEnableVertexAttribArray(gui->untex_shader.vert_pos_slot);
    pglDisableVertexAttribArray(gui->untex_shader.vert_tex_coord_slot);
    pglEnableVertexAttribArray(gui->untex_shader.vert_color_slot);

    do_draw_quad(gui, &gui->untex_shader, proj_mat, pos, sz, NULL, NULL, color);
}

void gui_draw_string(struct gui *gui, char const *txt,
                     GLfloat x_pos, GLfloat y_pos, GLfloat height) {
    pglActiveTexture(GL_TEXTURE0);
    pglBindTexture(GL_TEXTURE_2D, gui->font.tex);

    pglUseProgram(gui->tex_shader.prog);
    pglUniform1i(gui->tex_shader.bound_tex_slot, 0);

    GLfloat proj_mat[16];
    GLfloat proj_min[2] = { gui->width * -0.5f, gui->height * -0.5f };
    GLfloat proj_max[2] = { gui->width * 0.5f, gui->height * 0.5f };
    GLfloat color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

    mat_ortho2d(proj_mat, proj_min, proj_max);

    pglBindVertexArray(gui->tex_mesh.vao);
    pglBindBuffer(GL_ARRAY_BUFFER, gui->tex_mesh.vbo);
    pglBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui->tex_mesh.ebo);
    pglEnableVertexAttribArray(gui->tex_shader.vert_pos_slot);
    pglEnableVertexAttribArray(gui->tex_shader.vert_tex_coord_slot);

    GLfloat aspect = (GLfloat)(bmp_font.width / FONT_GLYPHS_X) /
        (GLfloat)(bmp_font.height / FONT_GLYPHS_Y);
    GLfloat width = height * aspect;

    int ch;
    while ((ch = tolower(*txt++))) {
        int glyph[2];
        if (ch >= 'a' && ch <= 'm') {
            glyph[0] = ch - 'a';
            glyph[1] = 0;
        } else if (ch >= 'n' && ch <= 'z') {
            glyph[0] = ch - 'n';
            glyph[1] = 1;
        } else {
            goto next_char;
        }

        /* GLfloat pos[2] = { x_pos, gui->height * 0.5f - y_pos - height }; */
        GLfloat pos[2] = { x_pos, y_pos };
        GLfloat sz[2] = { height * aspect, height };
        GLfloat uv_pos[2] = {
            (GLfloat)glyph[0] / FONT_GLYPHS_X,
            (GLfloat)glyph[1] / FONT_GLYPHS_Y
        };
        GLfloat uv_sz[2] = {
            1.0f / FONT_GLYPHS_X,
            1.0f / FONT_GLYPHS_Y
        };

        do_draw_quad(gui, &gui->tex_shader, proj_mat, pos, sz, uv_pos, uv_sz, color);
    next_char:
        x_pos += width;
    }
}

static void init_shader(struct shader *sh, char const *vs, char const *fs) {
    sh->vs = compile_shader(vs, GL_VERTEX_SHADER);
    sh->fs = compile_shader(fs, GL_FRAGMENT_SHADER);

    sh->prog = pglCreateProgram();
    pglAttachShader(sh->prog, sh->vs);
    pglAttachShader(sh->prog, sh->fs);
    pglLinkProgram(sh->prog);

    GLint link_status;
    pglGetProgramiv(sh->prog, GL_LINK_STATUS, &link_status);
    if (!link_status)
        LOG_ERROR("unable to link GUI shader program\n");

    sh->vert_pos_slot = pglGetAttribLocation(sh->prog, "vert_pos");
    sh->vert_tex_coord_slot = pglGetAttribLocation(sh->prog, "texcoord");
    sh->vert_color_slot = pglGetAttribLocation(sh->prog, "vert_color");
    sh->bound_tex_slot = pglGetUniformLocation(sh->prog, "bound_tex");
    sh->trans_mat_slot = pglGetUniformLocation(sh->prog, "trans_mat");
    sh->tex_mat_slot = pglGetUniformLocation(sh->prog, "tex_mat");
    sh->color_mat_slot = pglGetUniformLocation(sh->prog, "color_mat");
}

static void cleanup_shader(struct shader *sh) {
    pglDeleteProgram(sh->prog);
    pglDeleteShader(sh->vs);
    pglDeleteShader(sh->fs);
}

static void mesh_init(struct mesh *mesh, struct shader const *sh,
               GLfloat const *verts, unsigned n_verts,
               GLuint const *indices, unsigned n_indices) {
    pglGenVertexArrays(1, &mesh->vao);
    pglGenBuffers(1, &mesh->vbo);
    pglBindVertexArray(mesh->vao);
    pglBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
    pglBufferData(GL_ARRAY_BUFFER, VERT_LEN * sizeof(GLfloat) * n_verts,
                  verts, GL_STATIC_DRAW);

    pglVertexAttribPointer(sh->vert_pos_slot, 4, GL_FLOAT, GL_FALSE,
                           sizeof(GLfloat) * VERT_LEN, (GLvoid*)0);
    pglVertexAttribPointer(sh->vert_tex_coord_slot, 2, GL_FLOAT, GL_FALSE,
                           sizeof(GLfloat) * VERT_LEN, (GLvoid*)(sizeof(GLfloat) * 4));
    pglVertexAttribPointer(sh->vert_color_slot, 4, GL_FLOAT, GL_FALSE,
                           sizeof(GLfloat) * VERT_LEN, (GLvoid*)(sizeof(GLfloat) * 6));

    pglGenBuffers(1, &mesh->ebo);
    pglBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ebo);
    pglBufferData(GL_ELEMENT_ARRAY_BUFFER, n_indices * sizeof(GLuint),
                  indices, GL_STATIC_DRAW);
}

static void mesh_cleanup(struct mesh *mesh) {
    pglDeleteBuffers(1, &mesh->ebo);
    pglDeleteBuffers(1, &mesh->vbo);
    pglDeleteVertexArrays(1, &mesh->vao);
}
