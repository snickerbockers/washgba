/*******************************************************************************
 *
 *
 *    WashingtonDC Dreamcast Emulator
 *    Copyright (C) 2018-2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "log.h"
#include "error.h"
#include "intmath.h"
#include "compiler_bullshit.h"
#include "thumb.h"

#include "arm7.h"

static DEF_ERROR_U32_ATTR(arm7_inst)
static DEF_ERROR_U32_ATTR(arm7_pc)
DEF_ERROR_INT_ATTR(arm7_execution_mode)

#define ARM7_INST_COND_SHIFT 28
#define ARM7_INST_COND_MASK (0xf << ARM7_INST_COND_SHIFT)

static bool arm7_excp_refresh_event_handler(struct SchedEvent *event);

static uint32_t arm7_do_fetch_inst(struct arm7 *arm7, uint32_t addr);

static unsigned arm7_spsr_idx(struct arm7 *arm7);

static uint32_t decode_immed(arm7_inst inst);

static uint32_t decode_shift(struct arm7 *arm7, arm7_inst inst, bool *carry);

static uint32_t decode_shift_ldr_str(struct arm7 *arm7,
                                     arm7_inst inst, bool *carry);

static uint32_t
decode_shift_by_immediate(struct arm7 *arm7, unsigned shift_fn,
                          unsigned val_reg, unsigned shift_amt, bool *carry);

static inline uint32_t arm7_read32(struct arm7 *arm7, uint32_t addr) {
    uint32_t val;
    switch (memory_map_try_read_32(arm7->map, addr, &val)) {
    case MEM_ACCESS_SUCCESS:
        return val;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
    default:
    case MEM_ACCESS_FAILURE:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNMAPPED_READ);
    }
}

static inline uint16_t arm7_read16(struct arm7 *arm7, uint32_t addr) {
    uint16_t val;
    switch (memory_map_try_read_16(arm7->map, addr, &val)) {
    case MEM_ACCESS_SUCCESS:
        return val;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
    default:
    case MEM_ACCESS_FAILURE:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNMAPPED_READ);
    }
}

static inline uint8_t
arm7_read8(struct arm7 *arm7, uint32_t addr) {
    uint8_t val;
    switch (memory_map_try_read_8(arm7->map, addr, &val)) {
    case MEM_ACCESS_SUCCESS:
        return val;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(1);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(1);
        EMU_ERROR(ERROR_UNSUPPORTED_READ_ALIGN);
    default:
    case MEM_ACCESS_FAILURE:
        error_set_address(addr);
        error_set_length(1);
        EMU_ERROR(ERROR_UNMAPPED_READ);
    }
}

static inline void
arm7_write32(struct arm7 *arm7, uint32_t addr, uint32_t val) {
    switch (memory_map_try_write_32(arm7->map, addr, val)) {
    case MEM_ACCESS_SUCCESS:
        break;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_WRITE_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNSUPPORTED_WRITE_ALIGN);
    default:
        error_set_address(addr);
        error_set_length(4);
        EMU_ERROR(ERROR_UNMAPPED_WRITE);
    }
}

static inline void
arm7_write16(struct arm7 *arm7, uint32_t addr, uint16_t val) {
    switch (memory_map_try_write_16(arm7->map, addr, val)) {
    case MEM_ACCESS_SUCCESS:
        break;
    case MEM_ACCESS_BADSIZE:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNSUPPORTED_WRITE_SIZE);
    case MEM_ACCESS_BADALIGN:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNSUPPORTED_WRITE_ALIGN);
    default:
        error_set_address(addr);
        error_set_length(2);
        EMU_ERROR(ERROR_UNMAPPED_WRITE);
    }
}

static inline void
arm7_write8(struct arm7 *arm7, uint32_t addr, uint8_t val) {
    if (memory_map_try_write_8(arm7->map, addr, val) != MEM_ACCESS_SUCCESS) {
        if (addr == 0x4000410) {
            /*
             * GBA firmware does this at PC=0x9f4 (actual instruction address
             * is 0x9f0, in thumb mode).  According to nocash docs this is
             * probably a bug.
             */
            return;
        }

        error_set_address(addr);
        error_set_length(1);
        EMU_ERROR(ERROR_UNMAPPED_WRITE);
    }
}

DEF_ERROR_U32_ATTR(arm7_cpu_mode)
static DEF_ERROR_U32_ATTR(old_cpu_mode)
static DEF_ERROR_U32_ATTR(new_cpu_mode)
static DEF_ERROR_U32_ATTR(old_cpsr)
static DEF_ERROR_U32_ATTR(new_cpsr)

static inline bool validate_cpu_mode(uint32_t mode) {
    switch (mode) {
    case ARM7_MODE_USER:
    case ARM7_MODE_FIQ:
    case ARM7_MODE_IRQ:
    case ARM7_MODE_SVC:
    case ARM7_MODE_UND:
    case ARM7_MODE_ABT:
    case ARM7_MODE_SYS:
        return true;
    default:
        return false;
    }
}

/*
 * call this whenever writing to the M, I or F fields in cpsr.
 * For NZCV, this doesn't really matter.
 */
static void arm7_cpsr_mode_change(struct arm7 *arm7, uint32_t new_val) {
    unsigned new_mode = new_val & ARM7_CPSR_M_MASK;
    unsigned old_mode = arm7->reg[ARM7_REG_CPSR] &
        ARM7_CPSR_M_MASK;

    /*
     * XXX We validate the CPU modes so that if EMU_ERROR gets called, the CPU
     * state has not been modified.  This is suboptimal and we may need to take
     * this check out later.  Note that all this does is make debug logs more
     * consistent, there's no real reason why we have to keep CPU state
     * preserved like this.
     */
    if (!validate_cpu_mode(old_mode) || !validate_cpu_mode(new_mode)) {
        error_set_old_cpu_mode(old_mode);
        error_set_new_cpu_mode(new_mode);
        error_set_old_cpsr(arm7->reg[ARM7_REG_CPSR]);
        error_set_new_cpsr(new_val);
        EMU_ERROR(ERROR_UNKNOWN_CPU_MODE);
    }

    if (new_mode == old_mode)
        goto the_end;

    switch (old_mode) {
    case ARM7_MODE_USER:
    case ARM7_MODE_SYS:
        break;
    case ARM7_MODE_FIQ:
        memcpy(arm7->reg + ARM7_REG_R8_FIQ,
               arm7->reg + ARM7_REG_R8, sizeof(uint32_t) * 7);
        memcpy(arm7->reg + ARM7_REG_R8,
               arm7->reg + ARM7_REG_R8_BANK, sizeof(uint32_t) * 7);
        break;
    case ARM7_MODE_IRQ:
        memcpy(arm7->reg + ARM7_REG_R13_IRQ,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_BANK,
               sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_SVC:
        memcpy(arm7->reg + ARM7_REG_R13_SVC,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_BANK,
               sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_UND:
        memcpy(arm7->reg + ARM7_REG_R13_UND,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_BANK,
               sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_ABT:
        memcpy(arm7->reg + ARM7_REG_R13_ABT,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_BANK,
               sizeof(uint32_t) * 2);
        break;
    default:
        error_set_old_cpu_mode(old_mode);
        error_set_new_cpu_mode(new_mode);
        EMU_ERROR(ERROR_UNKNOWN_CPU_MODE);
    }

    switch (new_mode) {
    case ARM7_MODE_USER:
    case ARM7_MODE_SYS:
        break;
    case ARM7_MODE_FIQ:
        memcpy(arm7->reg + ARM7_REG_R8_BANK,
               arm7->reg + ARM7_REG_R8, sizeof(uint32_t) * 7);
        memcpy(arm7->reg + ARM7_REG_R8,
               arm7->reg + ARM7_REG_R8_FIQ, sizeof(uint32_t) * 7);
        break;
    case ARM7_MODE_IRQ:
        memcpy(arm7->reg + ARM7_REG_R13_BANK,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_IRQ, sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_SVC:
        memcpy(arm7->reg + ARM7_REG_R13_BANK,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_SVC, sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_ABT:
        memcpy(arm7->reg + ARM7_REG_R13_BANK,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_ABT, sizeof(uint32_t) * 2);
        break;
    case ARM7_MODE_UND:
        memcpy(arm7->reg + ARM7_REG_R13_BANK,
               arm7->reg + ARM7_REG_R13, sizeof(uint32_t) * 2);
        memcpy(arm7->reg + ARM7_REG_R13,
               arm7->reg + ARM7_REG_R13_UND, sizeof(uint32_t) * 2);
        break;
    default:
        error_set_old_cpu_mode(old_mode);
        error_set_new_cpu_mode(new_mode);
        EMU_ERROR(ERROR_UNKNOWN_CPU_MODE);
    }

 the_end:
    arm7->reg[ARM7_REG_CPSR] = new_val;
}

void arm7_init(struct arm7 *arm7, struct dc_clock *clk) {
    memset(arm7, 0, sizeof(*arm7));
    arm7->clk = clk;
    arm7->reg[ARM7_REG_CPSR] =
        ARM7_MODE_SVC | ARM7_CPSR_I_MASK | ARM7_CPSR_F_MASK;

    arm7->excp_refresh_event.handler = arm7_excp_refresh_event_handler;
    arm7->excp_refresh_event.arg_ptr = arm7;
}

void arm7_cleanup(struct arm7 *arm7) {
}

void arm7_set_mem_map(struct arm7 *arm7, struct memory_map *arm7_mem_map) {
    arm7->map = arm7_mem_map;
    arm7_reset_pipeline(arm7);
}

void arm7_reset(struct arm7 *arm7, bool val) {
    // TODO: set the ARM7 to supervisor (svc) mode and enter a reset exception.
    printf("%s(%s)\n", __func__, val ? "true" : "false");

    if (val) {
        // enable the CPU
        arm7->excp |= ARM7_EXCP_RESET;
        arm7_excp_refresh_outside_cpu_context(arm7);
    }
}

unsigned arm7_exec_inst(struct arm7 *arm7) {
    int extra_cycles;
    arm7_inst inst = arm7_fetch_inst(arm7, &extra_cycles);
    arm7_op_fn handler;
    if (arm7_thumb_mode(arm7))
        handler = arm7_thumb_decode(arm7, &inst);
    else
        handler = arm7_decode(arm7, inst);

    // TODO: more accurate instruction cycle counting
    unsigned inst_cycles = 1;
    handler(arm7, inst);

    return (inst_cycles + extra_cycles) * ARM7_CLOCK_SCALE;
}

#define MASK_NOT_DATA_PROCESSING ((1 << 4) | (1 << 7) | (1 << 25))
#define VAL_NOT_DATA_PROCESSING ((1 << 4) | (1 << 7))

#define MASK_BX BIT_RANGE(4, 27)
#define VAL_BX 0x012fff10

// B or BL instruction
#define MASK_B BIT_RANGE(25, 27)
#define VAL_B  0x0a000000

#define MASK_LDR_STR BIT_RANGE(26, 27)
#define VAL_LDR_STR  0x04000000

#define MASK_STR (BIT_RANGE(26, 27) | (1 << 20))
#define VAL_STR  0x04000000

#define MASK_MRS (BIT_RANGE(23, 27) | BIT_RANGE(16, 21) | BIT_RANGE(0, 11))
#define VAL_MRS  0x010f0000

#define MASK_MSR_IMM8 (BIT_RANGE(12, 15) | BIT_RANGE(20, 21) | BIT_RANGE(23, 27))
#define VAL_MSR_IMM8 ((6 << 23) | (2 << 20) | (0xf << 12))

#define MASK_MSR_REG (BIT_RANGE(4, 15) | BIT_RANGE(20, 21) | BIT_RANGE(23, 27))
#define VAL_MSR_REG ((2 << 23) | (2 << 20) | (0xf << 12))

// data processing opcodes
#define MASK_ORR (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_ORR (12 << 21)

#define MASK_EOR (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_EOR (1 << 21)

#define MASK_BIC (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_BIC (14 << 21)

#define MASK_SUB (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_SUB (2 << 21)

#define MASK_RSB (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_RSB (3 << 21)

#define MASK_ADD (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_ADD (4 << 21)

#define MASK_ADC (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_ADC (5 << 21)

#define MASK_SBC (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_SBC (6 << 21)

#define MASK_RSC (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_RSC (7 << 21)

#define MASK_TST (BIT_RANGE(20, 24) | BIT_RANGE(26, 27))
#define VAL_TST ((8 << 21) | (1 << 20))

#define MASK_TEQ (BIT_RANGE(20, 24) | BIT_RANGE(26, 27))
#define VAL_TEQ ((9 << 21) | (1 << 20))

#define MASK_CMP (BIT_RANGE(20, 24) | BIT_RANGE(26, 27))
#define VAL_CMP ((10 << 21) | (1 << 20))

#define MASK_AND (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_AND 0

#define MASK_MOV (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_MOV (13 << 21)

#define MASK_MVN (BIT_RANGE(21, 24) | BIT_RANGE(26, 27))
#define VAL_MVN (15 << 21)

#define MASK_CMN (BIT_RANGE(20, 24) | BIT_RANGE(26, 27))
#define VAL_CMN ((11 << 21) | (1 << 20))

#define MASK_BLOCK_XFER BIT_RANGE(25, 27)
#define VAL_BLOCK_XFER (4 << 25)

#define MASK_MUL (BIT_RANGE(22, 27) | BIT_RANGE(4, 7))
#define VAL_MUL  (9 << 4)

#define MASK_SWI BIT_RANGE(24, 27)
#define VAL_SWI BIT_RANGE(24, 27)

#define MASK_SWAP (BIT_RANGE(4, 11) | BIT_RANGE(20,21) | BIT_RANGE(23, 27))
#define VAL_SWAP ((9 << 4) | (2 << 23))

#define MASK_LDRH_STRH (BIT_RANGE(4, 7) | BIT_RANGE(25, 27))
#define VAL_LDRH_STRH 0xb0

#define MASK_LDRSH (BIT_RANGE(4, 7) | (1 << 20) | BIT_RANGE(25, 27))
#define VAL_LDRSH ((1 << 20) | BIT_RANGE(4, 7))

#define MASK_LDRSB (BIT_RANGE(4, 7) | (1 << 20) | BIT_RANGE(25, 27))
#define VAL_LDRSB ((1 << 20) | 0xd0)

#define MASK_UMULL (BIT_RANGE(21, 27) | BIT_RANGE(4, 7))
#define VAL_UMULL 0x00800090

#define MASK_SMULL (BIT_RANGE(4, 7) | BIT_RANGE(21, 27))
#define VAL_SMULL 0x00c00090

#define MASK_UMLAL (BIT_RANGE(21, 27) | BIT_RANGE(4, 7))
#define VAL_UMLAL 0x00a00090

#define MASK_SMLAL (BIT_RANGE(21, 27) | BIT_RANGE(4, 7))
#define VAL_SMLAL 0x00e00090

/* #define MASK_TEQ_IMMED BIT_RANGE(20, 27) */
/* #define VAL_TEQ_IMMED ((1 << 25) | (9 << 21)) */

#define DATA_OP_FUNC_NAME(op_name) arm7_op_##op_name

#define DATA_OP_FUNC_PROTO(op_name) \
DATA_OP_FUNC_NAME(op_name)(uint32_t lhs, uint32_t rhs, bool carry_in,\
                           bool *n_out, bool *c_out, bool *z_out, bool *v_out)

#define DEF_DATA_OP(op_name)                                            \
    static inline uint32_t                                              \
    DATA_OP_FUNC_PROTO(op_name)

DEF_DATA_OP(and) {
    uint32_t val = lhs & rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return val;
}

/* DEF_DATA_OP(eor) { */
/*     return lhs ^ rhs; */
/* } */

DEF_DATA_OP(sub) {
    /*
     * XXX The nomenclature for lhs/rhs is flipped in ARM7's notation compared
     * to the SH4's notation; that's why I have rhs on the left and lhs on the
     * right here.
     */
    bool c_tmp;
    uint32_t val = sub_flags(rhs, lhs, false, &c_tmp, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    *c_out = !c_tmp;
    return val;
}

DEF_DATA_OP(sbc) {
    /*
     * XXX The nomenclature for lhs/rhs is flipped in ARM7's notation compared
     * to the SH4's notation; that's why I have rhs on the left and lhs on the
     * right here.
     */
    bool c_tmp;
    uint32_t val = sub_flags(rhs, lhs, !carry_in, &c_tmp, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    *c_out = !c_tmp;
    return val;
}

DEF_DATA_OP(rsb) {
    /*
     * XXX The nomenclature for lhs/rhs is flipped in ARM7's notation compared
     * to the SH4's notation; that's why I have rhs on the left and lhs on the
     * right here.
     */
    bool c_tmp;
    uint32_t val = sub_flags(lhs, rhs, false, &c_tmp, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    *c_out = !c_tmp;
    return val;
}

DEF_DATA_OP(rsc) {
    /*
     * XXX The nomenclature for lhs/rhs is flipped in ARM7's notation compared
     * to the SH4's notation; that's why I have rhs on the left and lhs on the
     * right here.
     */
    bool c_tmp;
    uint32_t val = sub_flags(lhs, rhs, !carry_in, &c_tmp, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    *c_out = !c_tmp;
    return val;
}

DEF_DATA_OP(add) {
    uint32_t val = add_flags(lhs, rhs, false, c_out, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    return val;
}

DEF_DATA_OP(cmn) {
    uint32_t val = add_flags(lhs, rhs, false, c_out, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    return 0xdeadbeef;
}

DEF_DATA_OP(adc) {
    uint32_t val = add_flags(lhs, rhs, carry_in, c_out, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    return val;
}

DEF_DATA_OP(tst) {
    uint32_t val = lhs & rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return 0xdeadbabe; // result should never be written
}

DEF_DATA_OP(teq) {
    uint32_t val = lhs ^ rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return 0xdeadbabe; // result should never be written
}

DEF_DATA_OP(cmp) {
    /*
     * XXX The nomenclature for lhs/rhs is flipped in ARM7's notation compared
     * to the SH4's notation; that's why I have rhs on the left and lhs on the
     * right here.
     */
    bool c_tmp;
    uint32_t val = sub_flags(rhs, lhs, false, &c_tmp, v_out);
    *n_out = val & (1 << 31);
    *z_out = !val;
    *c_out = !c_tmp;
    return 0xdeadbabe; // result should never be written
}

/*
 * This is really xor.  For some stupid reason the ARM mnemonic is 'eor' even
 * though literally everbody else in the entire world ignores the silent E and
 * calls this xor.
 */
DEF_DATA_OP(eor) {
    uint32_t val = lhs ^ rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return val;
}

/* DEF_DATA_OP(cmn) { */
/*     return lhs + rhs; */
/* } */

DEF_DATA_OP(orr) {
    uint32_t val = lhs | rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return val;
}

DEF_DATA_OP(mov) {
    *n_out = rhs & (1 << 31);
    *z_out = !rhs;

    return rhs;
}

DEF_DATA_OP(mvn) {
    uint32_t val = ~rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return val;
}

DEF_DATA_OP(bic) {
    uint32_t val = lhs & ~rhs;
    *n_out = val & (1 << 31);
    *z_out = !val;

    return val;
}

/* DEF_DATA_OP(mv) { */
/*     return ~rhs; */
/* } */


typedef void(*arm7_opcode_fn)(struct arm7*, arm7_inst);

void next_inst(struct arm7 *arm7) {
    arm7->reg[ARM7_REG_PC] += arm7_thumb_mode(arm7) ? 2 : 4;
}

uint32_t arm7_pc_next(struct arm7 *arm7) {
    return arm7->pipeline_pc[1];
}

#define EVAL_COND(cond) if (!arm7_cond_##cond(arm7)) goto cond_fail;

// branch (with or without link)
#define DEF_BRANCH_INST(cond)                                           \
    static unsigned                                                     \
    arm7_inst_branch_##cond(struct arm7 *arm7, arm7_inst inst) {        \
        EVAL_COND(cond)                                                 \
        uint32_t offs = inst & ((1 << 24) - 1);                         \
        if (offs & (1 << 23))                                           \
            offs |= 0xff000000;                                         \
        offs <<= 2;                                                     \
                                                                        \
        if (inst & (1 << 24)) {                                         \
            /* link bit */                                              \
            *arm7_gen_reg(arm7, 14) = arm7->reg[ARM7_REG_PC] - 4;       \
        }                                                               \
                                                                        \
        uint32_t pc_new = offs + arm7->reg[ARM7_REG_PC];                \
                                                                        \
        arm7->reg[ARM7_REG_PC] = pc_new;                                \
        arm7_reset_pipeline(arm7);                                      \
                                                                        \
        goto the_end;                                                   \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 2 * S_CYCLE + 1 * N_CYCLE;                               \
    }

DEF_BRANCH_INST(eq)
DEF_BRANCH_INST(ne)
DEF_BRANCH_INST(cs)
DEF_BRANCH_INST(cc)
DEF_BRANCH_INST(mi)
DEF_BRANCH_INST(pl)
DEF_BRANCH_INST(vs)
DEF_BRANCH_INST(vc)
DEF_BRANCH_INST(hi)
DEF_BRANCH_INST(ls)
DEF_BRANCH_INST(ge)
DEF_BRANCH_INST(lt)
DEF_BRANCH_INST(gt)
DEF_BRANCH_INST(le)
DEF_BRANCH_INST(al)
DEF_BRANCH_INST(nv)

// branch/exchange
#define DEF_BX_INST(cond)                                           \
    static unsigned                                                 \
    arm7_inst_bx_##cond(struct arm7 *arm7, arm7_inst inst) {        \
        EVAL_COND(cond)                                             \
                                                                    \
        uint32_t dst = *arm7_gen_reg(arm7, inst & 0xf);             \
        if (dst & 1)                                                \
            arm7->reg[ARM7_REG_CPSR] |= ARM7_CPSR_T_MASK;           \
        arm7->reg[ARM7_REG_PC] = dst & ~1;                          \
        arm7_reset_pipeline(arm7);                                  \
        goto the_end;                                               \
    cond_fail:                                                      \
         next_inst(arm7);                                           \
    the_end:                                                        \
         return 2 * S_CYCLE + 1 * N_CYCLE;                          \
    }

DEF_BX_INST(eq)
DEF_BX_INST(ne)
DEF_BX_INST(cs)
DEF_BX_INST(cc)
DEF_BX_INST(mi)
DEF_BX_INST(pl)
DEF_BX_INST(vs)
DEF_BX_INST(vc)
DEF_BX_INST(hi)
DEF_BX_INST(ls)
DEF_BX_INST(ge)
DEF_BX_INST(lt)
DEF_BX_INST(gt)
DEF_BX_INST(le)
DEF_BX_INST(al)
DEF_BX_INST(nv)

#define DEF_LDR_STR_INST(cond)                                          \
    static unsigned                                                     \
    arm7_inst_ldr_str_##cond(struct arm7 *arm7, arm7_inst inst) {       \
        EVAL_COND(cond);                                                \
        unsigned rn = (inst >> 16) & 0xf;                               \
        unsigned rd = (inst >> 12) & 0xf;                               \
                                                                        \
        bool writeback = inst & (1 << 21);                              \
        int len = (inst & (1 << 22)) ? 1 : 4;                           \
        int sign = (inst & (1 << 23)) ? 1 : -1;                         \
        bool pre = inst & (1 << 24);                                    \
        bool offs_reg = inst & (1 << 25);                               \
        bool to_mem = !(inst & (1 << 20));                              \
        bool carry = (bool)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_C_MASK); \
                                                                        \
        uint32_t offs;                                                  \
                                                                        \
        if (offs_reg) {                                                 \
            offs = decode_shift_ldr_str(arm7, inst, &carry);            \
        } else {                                                        \
            offs = inst & ((1 << 12) - 1);                              \
        }                                                               \
                                                                        \
        /* TODO: should this instruction update the carry flag? */      \
                                                                        \
        uint32_t addr = *arm7_gen_reg(arm7, rn);                        \
        if (rn == ARM7_REG_PC && arm7_thumb_mode(arm7))                 \
            addr &= ~3;                                                 \
                                                                        \
        if (pre) {                                                      \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
        }                                                               \
                                                                        \
                                                                        \
        if (len == 4) {                                                 \
            if (addr % 4) {                                             \
                /* Log this case, it's got some pretty fucked up */     \
                /* handling for loads (see below).  Stores appear */    \
                /* to only clear the lower two bits, but Imust */       \
                /* tread carefully; this would not be the first time I */ \
                /* misinterpreted an obscure corner-case in ARM7DI's */ \
                /* CPU manual.*/                                        \
                LOG_DBG("ARM7 Unaligned memory %s at PC=0x%08x\n",      \
                        to_mem ? "store" : "load",                      \
                        (unsigned)arm7->reg[ARM7_REG_PC]);              \
            }                                                           \
            if (to_mem) {                                               \
                uint32_t val = *arm7_gen_reg(arm7, rd);                 \
                if (rd == 15)                                           \
                    val += 4;                                           \
                addr &= ~3;                                             \
                arm7_write32(arm7, addr, val);                          \
            } else {                                                    \
                uint32_t val;                                           \
                val = arm7_read32(arm7, addr & ~3);                     \
                                                                        \
                /* Deal with unaligned offsets.  It does the load */    \
                /* from the aligned address (ie address with bits */    \
                /* 0 and 1 cleared) and then right-rotates so that */   \
                /* the LSB corresponds to the original unalgined address */ \
                switch (addr % 4) {                                     \
                case 3:                                                 \
                    val = ((val >> 24) & 0xffffff) | (val << 8);        \
                    break;                                              \
                case 2:                                                 \
                    val = ((val >> 16) & 0xffffff) | (val << 16);       \
                    break;                                              \
                case 1:                                                 \
                    val = ((val >> 8) & 0xffffff) | (val << 24);        \
                    break;                                              \
                }                                                       \
                arm7_gen_reg_write(arm7, rd, val);                      \
            }                                                           \
        } else {                                                        \
            if (to_mem) {                                               \
                uint32_t val = *arm7_gen_reg(arm7, rd);                 \
                if (rd == 15)                                           \
                    val += 4;                                           \
                arm7_write8(arm7, addr, val);                           \
            } else {                                                    \
                arm7_gen_reg_write(arm7, rd, arm7_read8(arm7, addr));   \
            }                                                           \
        }                                                               \
                                                                        \
        if (!pre) {                                                     \
            if (writeback) {                                            \
                /* docs say the writeback is implied when the */        \
                /* pre bit is not set, and that the writeback */        \
                /* bit should be zero in this case. */                  \
                error_set_arm7_inst(inst);                              \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            }                                                           \
            writeback = true;                                           \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
        }                                                               \
                                                                        \
        /* ldr ignores writeback when rn == rd because the */           \
        /* writeback happens before the load is complete */             \
        if (writeback && (to_mem || rn != rd)) {                        \
            if (rn == 15)                                               \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            arm7_gen_reg_write(arm7, rn, addr);                         \
        }                                                               \
                                                                        \
        if (!to_mem && rd == 15) {                                      \
            arm7_reset_pipeline(arm7);                                  \
            goto the_end;                                               \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;                 \
    }

DEF_LDR_STR_INST(eq)
DEF_LDR_STR_INST(ne)
DEF_LDR_STR_INST(cs)
DEF_LDR_STR_INST(cc)
DEF_LDR_STR_INST(mi)
DEF_LDR_STR_INST(pl)
DEF_LDR_STR_INST(vs)
DEF_LDR_STR_INST(vc)
DEF_LDR_STR_INST(hi)
DEF_LDR_STR_INST(ls)
DEF_LDR_STR_INST(ge)
DEF_LDR_STR_INST(lt)
DEF_LDR_STR_INST(gt)
DEF_LDR_STR_INST(le)
DEF_LDR_STR_INST(al)
DEF_LDR_STR_INST(nv)

#define DEF_LDRH_STRH_INST(cond)                                        \
    static unsigned                                                     \
    arm7_inst_ldrh_strh_##cond(struct arm7 *arm7, arm7_inst inst) {     \
        EVAL_COND(cond);                                                \
        bool writeback = inst & (1 << 21);                              \
        if (inst & (1 << 6)) {                                          \
            error_set_arm7_inst(inst);                                  \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        bool to_mem = (inst >> 20) & 1 ? false : true;                  \
        bool offs_is_imm = inst & (1 << 22);                            \
        unsigned rd = (inst >> 12) & 0xf;                               \
        unsigned rn = (inst >> 16) & 0xf;                               \
        bool pre = inst & (1 << 24);                                    \
        int sign = (inst & (1 << 23)) ? 1 : -1;                         \
                                                                        \
        int32_t offs;                                                   \
        if (offs_is_imm) {                                              \
            int imm8 = (inst & 0xf) | ((inst & BIT_RANGE(8, 11)) >> 4); \
            offs = imm8;                                                \
        } else {                                                        \
            offs = *arm7_gen_reg(arm7, inst & BIT_RANGE(0, 3));         \
        }                                                               \
                                                                        \
        uint32_t addr = *arm7_gen_reg(arm7, rn);                        \
        if (rn == ARM7_REG_PC && arm7_thumb_mode(arm7))                 \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        if (pre) {                                                      \
            /* pre-indexed addressing */                                \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
        }                                                               \
                                                                        \
        if (to_mem)                                                     \
            arm7_write16(arm7, addr, *arm7_gen_reg(arm7, rd));          \
        else                                                            \
            arm7_gen_reg_write(arm7, rd, arm7_read16(arm7, addr));      \
                                                                        \
        if (!pre) {                                                     \
            /* post-indexed addressing */                               \
            if (writeback) {                                            \
                /* docs say the writeback is implied when the */        \
                /* pre bit is not set, and that the writeback */        \
                /* bit should be zero in this case. */                  \
                error_set_arm7_inst(inst);                              \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            }                                                           \
            writeback = true;                                           \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
                                                                        \
        }                                                               \
                                                                        \
        /* ldr ignores writeback when rn == rd because the */           \
        /* writeback happens before the load is complete */             \
        if (writeback && (to_mem || rn != rd)) {                        \
            if (rn == 15)                                               \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            arm7_gen_reg_write(arm7, rn, addr);                         \
        }                                                               \
                                                                        \
        if (!to_mem && rd == 15) {                                      \
            arm7_reset_pipeline(arm7);                                  \
            goto the_end;                                               \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;                 \
    }

DEF_LDRH_STRH_INST(eq)
DEF_LDRH_STRH_INST(ne)
DEF_LDRH_STRH_INST(cs)
DEF_LDRH_STRH_INST(cc)
DEF_LDRH_STRH_INST(mi)
DEF_LDRH_STRH_INST(pl)
DEF_LDRH_STRH_INST(vs)
DEF_LDRH_STRH_INST(vc)
DEF_LDRH_STRH_INST(hi)
DEF_LDRH_STRH_INST(ls)
DEF_LDRH_STRH_INST(ge)
DEF_LDRH_STRH_INST(lt)
DEF_LDRH_STRH_INST(gt)
DEF_LDRH_STRH_INST(le)
DEF_LDRH_STRH_INST(al)
DEF_LDRH_STRH_INST(nv)

#define DEF_LDRSH_INST(cond)                                            \
    static unsigned                                                     \
    arm7_inst_ldrsh_##cond(struct arm7 *arm7, arm7_inst inst) {         \
        EVAL_COND(cond);                                                \
        if (!((inst & (1 << 24)) && (inst & (1 << 23))) ||              \
            (inst & (1 << 21))) {                                       \
            error_set_arm7_inst(inst);                                  \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        bool offs_is_imm = inst & (1 << 22);                            \
        unsigned rd = (inst >> 12) & 0xf;                               \
        unsigned rn = (inst >> 16) & 0xf;                               \
                                                                        \
        uint32_t offs;                                                  \
        if (offs_is_imm) {                                              \
            int imm8 = (inst & 0xf) | ((inst & BIT_RANGE(8, 11)) >> 4); \
            offs = imm8;                                                \
        } else {                                                        \
            offs = *arm7_gen_reg(arm7, inst & BIT_RANGE(0, 3));         \
        }                                                               \
        uint32_t addr = *arm7_gen_reg(arm7, rn);                        \
        if (rn == ARM7_REG_PC && arm7_thumb_mode(arm7))                 \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        addr += offs;                                                   \
                                                                        \
        uint32_t val = arm7_read16(arm7, addr);                         \
        if (val & (1 << 15)) {                                          \
            val |= BIT_RANGE(16, 31);                                   \
        }                                                               \
        arm7_gen_reg_write(arm7, rd, val);                              \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;                 \
    }

DEF_LDRSH_INST(eq)
DEF_LDRSH_INST(ne)
DEF_LDRSH_INST(cs)
DEF_LDRSH_INST(cc)
DEF_LDRSH_INST(mi)
DEF_LDRSH_INST(pl)
DEF_LDRSH_INST(vs)
DEF_LDRSH_INST(vc)
DEF_LDRSH_INST(hi)
DEF_LDRSH_INST(ls)
DEF_LDRSH_INST(ge)
DEF_LDRSH_INST(lt)
DEF_LDRSH_INST(gt)
DEF_LDRSH_INST(le)
DEF_LDRSH_INST(al)
DEF_LDRSH_INST(nv)

#define DEF_LDRSB_INST(cond)                                            \
    static unsigned                                                     \
    arm7_inst_ldrsb_##cond(struct arm7 *arm7, arm7_inst inst) {         \
        EVAL_COND(cond);                                                \
                                                                        \
        bool writeback = inst & (1 << 21);                              \
        bool offs_is_imm = inst & (1 << 22);                            \
        unsigned rd = (inst >> 12) & 0xf;                               \
        unsigned rn = (inst >> 16) & 0xf;                               \
        bool pre_inc = (inst >> 24) & 1;                                \
        int sign = (inst & (1 << 23)) ? 1 : -1;                         \
                                                                        \
        uint32_t offs;                                                  \
        if (offs_is_imm) {                                              \
            int imm8 = (inst & 0xf) | ((inst & BIT_RANGE(8, 11)) >> 4); \
            offs = imm8;                                                \
        } else {                                                        \
            offs = *arm7_gen_reg(arm7, inst & BIT_RANGE(0, 3));         \
        }                                                               \
        uint32_t addr = *arm7_gen_reg(arm7, rn);                        \
        if (rn == ARM7_REG_PC && arm7_thumb_mode(arm7))                 \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        if (pre_inc) {                                                  \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
        }                                                               \
                                                                        \
        uint32_t val = arm7_read8(arm7, addr);                          \
        if (val & (1 << 7)) {                                           \
            val |= BIT_RANGE(7, 31);                                    \
        }                                                               \
        arm7_gen_reg_write(arm7, rd, val);                              \
        if (!pre_inc) {                                                 \
            if (writeback) {                                            \
                /* docs say the writeback is implied when the */        \
                /* pre bit is not set, and that the writeback */        \
                /* bit should be zero in this case. */                  \
                error_set_arm7_inst(inst);                              \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            }                                                           \
            if (sign < 0)                                               \
                addr -= offs;                                           \
            else                                                        \
                addr += offs;                                           \
            writeback = true;                                           \
        }                                                               \
                                                                        \
        /* ldr ignores writeback when rn == rd because the */           \
        /* writeback happens before the load is complete */             \
        if (writeback && rn != rd) {                                    \
            if (rn == 15)                                               \
                EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
            arm7_gen_reg_write(arm7, rn, addr);                         \
        }                                                               \
                                                                        \
        if (rd == 15) {                                                 \
            arm7_reset_pipeline(arm7);                                  \
            goto the_end;                                               \
        }                                                               \
                                                                        \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;                 \
    }

DEF_LDRSB_INST(eq)
DEF_LDRSB_INST(ne)
DEF_LDRSB_INST(cs)
DEF_LDRSB_INST(cc)
DEF_LDRSB_INST(mi)
DEF_LDRSB_INST(pl)
DEF_LDRSB_INST(vs)
DEF_LDRSB_INST(vc)
DEF_LDRSB_INST(hi)
DEF_LDRSB_INST(ls)
DEF_LDRSB_INST(ge)
DEF_LDRSB_INST(lt)
DEF_LDRSB_INST(gt)
DEF_LDRSB_INST(le)
DEF_LDRSB_INST(al)
DEF_LDRSB_INST(nv)

#ifdef INVARIANTS
#define BASEPTR_SANITY_OPEN do {                                    \
    uint32_t baseptr_orig = *baseptr

#define BASEPTR_SANITY_CLOSE                                            \
    if (baseptr_orig != *baseptr) {                                     \
        LOG_ERROR("mode change %08X to %08X\n",                         \
                  oldmode,                                              \
                  (unsigned)(arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK)); \
        CRITICAL_ERROR(ERROR_INTEGRITY);                                   \
    }} while (0)

#else
#define BASEPTR_SANITY_OPEN do {
#define BASEPTR_SANITY_CLOSE } while (0)
#endif

#define DEF_BLOCK_XFER_INST(cond)                                       \
    static unsigned                                                     \
    arm7_inst_block_xfer_##cond(struct arm7 *arm7, arm7_inst inst) {    \
        EVAL_COND(cond);                                                \
        unsigned rn = (inst & BIT_RANGE(16, 19)) >> 16;                 \
        unsigned reg_list = inst & 0xffff;                              \
        bool pre = (bool)(inst & (1 << 24));                            \
        bool up = (bool)(inst & (1 << 23));                             \
        bool psr_user_force = (bool)(inst & (1 << 22));                 \
        bool writeback = (bool)(inst & (1 << 21));                      \
        bool load = (bool)(inst & (1 << 20));                           \
                                                                        \
        /* the spec says this should only be done in a */               \
        /* privileged mode */                                           \
        if (psr_user_force &&                                           \
            (arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK) == ARM7_MODE_USER) { \
            error_set_feature("whatever happens when you set the "      \
                              "S-bit in an ARM7 LDM/SDM instruction."); \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        unsigned bank = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK;    \
        if (psr_user_force && (!load || !(reg_list & (1<<15)))) {       \
            if (!load)                                                  \
                writeback = false; /* the spec says so */               \
            bank = ARM7_MODE_USER;                                      \
        }                                                               \
                                                                        \
        /* docs say you cant do this */                                 \
        if (rn == 15) {                                                 \
            error_set_arm7_inst(inst);                                  \
            error_set_feature("PC as base pointer");                    \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        uint32_t *baseptr = arm7_gen_reg(arm7, rn);                     \
        uint32_t base = *baseptr;                                       \
        int reg_no;                                                     \
                                                                        \
        /* things get really hairy when the base register is in the */  \
        /* list *and* the writeback bit is set */                       \
        if (writeback && (reg_list & (1 << rn))) {                      \
            /* quoth the official ARM7DI data sheet: */                 \
            /* When write-back is specified, the base is written */     \
            /* back at the end of the second cycle of the */            \
            /* instruction. During a STM, the first register is */      \
            /* written out at the start of the second cycle. A STM */   \
            /* which includes storing the base, with the base as the */ \
            /* first register to be stored, will therefore store the */ \
            /* unchanged value, whereas with the base second or */      \
            /* later in the transfer order, will store the modified */  \
            /* value. A LDM will always overwrite the updated base */   \
            /* if the base is in the list. */                           \
                                                                        \
            uint32_t final_base = base;                                 \
            int amt_per_reg = up ? 4 : -4;                              \
            for (reg_no = 0; reg_no < 15; reg_no++)                     \
                if (reg_list & (1 << reg_no))                           \
                    final_base += amt_per_reg;                          \
                                                                        \
            if (load) {                                                 \
                /* for LDM, the writeback always gets overwritten */    \
                /* with whatever just got loaded from memory */         \
                writeback = false;                                      \
            } else {                                                    \
                bool is_base_first = true;                              \
                for (reg_no = 0; reg_no < rn; reg_no++)                 \
                    if (reg_list & (1 << reg_no))                       \
                        is_base_first = false;                          \
                if (!is_base_first) {                                   \
                    /* perform writeback early because it will */       \
                    /* happen before the base gets stored */            \
                    writeback = false;                                  \
                    *baseptr = final_base;                              \
                }                                                       \
            }                                                           \
        }                                                               \
                                                                        \
        if (!reg_list) {                                                \
            error_set_arm7_inst(inst);                                  \
            error_set_feature("empty register list");                   \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        if (base % 4) {                                                 \
            error_set_arm7_inst(inst);                                  \
            error_set_feature("unaligned ARM7 block transfers");        \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        if (up) {                                                       \
            if (load) {                                                 \
                for (reg_no = 0; reg_no < 15; reg_no++)                 \
                    if (reg_list & (1 << reg_no)) {                     \
                        if (pre)                                        \
                            base += 4;                                  \
                        if (bank == ARM7_MODE_USER ||                   \
                            bank == ARM7_MODE_SYS) {                    \
                            arm7_gen_reg_bank_write(arm7, reg_no, bank, \
                                                    arm7_read32(arm7, base)); \
                        } else {                                        \
                            arm7_gen_reg_write(arm7, reg_no,            \
                                               arm7_read32(arm7, base)); \
                        }                                               \
                        if (!pre)                                       \
                            base += 4;                                  \
                    }                                                   \
                if (reg_list & (1 << 15)) {                             \
                    if (psr_user_force) {                               \
                        BASEPTR_SANITY_OPEN;                            \
                        unsigned oldmode =                              \
                            arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK; \
                        arm7_cpsr_mode_change(arm7,                     \
                                              arm7->reg[arm7_spsr_idx(arm7)]); \
                        baseptr = arm7_gen_reg_bank(arm7, rn, oldmode); \
                        BASEPTR_SANITY_CLOSE;                           \
                    }                                                   \
                    if (pre)                                            \
                        base += 4;                                      \
                    arm7_gen_reg_write(arm7, 15,                        \
                                       arm7_read32(arm7, base));        \
                    if (!pre)                                           \
                        base += 4;                                      \
                }                                                       \
            } else {                                                    \
                /* store */                                             \
                for (reg_no = 0; reg_no < 15; reg_no++)                 \
                    if (reg_list & (1 << reg_no)) {                     \
                        if (pre)                                        \
                            base += 4;                                  \
                        if (bank == ARM7_MODE_USER ||                   \
                            bank == ARM7_MODE_SYS) {                    \
                            uint32_t val = *arm7_gen_reg_bank(arm7,     \
                                                              reg_no,   \
                                                              bank);    \
                            arm7_write32(arm7, base, val);              \
                        } else {                                        \
                            uint32_t val = *arm7_gen_reg(arm7, reg_no); \
                            arm7_write32(arm7, base, val);              \
                        }                                               \
                        if (!pre)                                       \
                            base += 4;                                  \
                    }                                                   \
                                                                        \
                if (reg_list & (1 << 15)) {                             \
                    if (pre)                                            \
                        base += 4;                                      \
                    arm7_write32(arm7, base, arm7->reg[ARM7_REG_PC] + 4); \
                    if (!pre)                                           \
                        base += 4;                                      \
                }                                                       \
            }                                                           \
        } else {                                                        \
            /* TODO: */                                                 \
            /* This transfers higher registers before lower */          \
            /* registers.  The spec says that lower registers must */   \
            /* always go first.  I don't think that will be a */        \
            /* problem since it all happens instantly, but it's */      \
            /* somethingto keep in mind if you ever try to use this */  \
            /* interpreter on a system which has a FIFO register */     \
            /* like the one SH4 uses to communicate with PowerVR2's */  \
            /* Tile Accelerator. */                                     \
            if (load) {                                                 \
                if (reg_list & (1 << 15)) {                             \
                    if (psr_user_force) {                               \
                        BASEPTR_SANITY_OPEN;                            \
                        unsigned oldmode =                              \
                            arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK; \
                        arm7_cpsr_mode_change(arm7,                     \
                                              arm7->reg[arm7_spsr_idx(arm7)]); \
                        baseptr = arm7_gen_reg_bank(arm7, rn, oldmode); \
                        BASEPTR_SANITY_CLOSE;                           \
                    }                                                   \
                    if (pre)                                            \
                        base -= 4;                                      \
                    arm7_gen_reg_write(arm7, 15,                        \
                                       arm7_read32(arm7, base));        \
                    if (!pre)                                           \
                        base -= 4;                                      \
                }                                                       \
                for (reg_no = 14; reg_no >= 0; reg_no--) {              \
                    if (reg_list & (1 << reg_no)) {                     \
                        if (pre)                                        \
                            base -= 4;                                  \
                        if (bank == ARM7_MODE_USER ||                   \
                            bank == ARM7_MODE_SYS) {                    \
                            uint32_t *valp = arm7_gen_reg_bank(arm7,    \
                                                               reg_no,  \
                                                               bank);   \
                            *valp = arm7_read32(arm7, base);            \
                        } else {                                        \
                            uint32_t *valp = arm7_gen_reg(arm7, reg_no); \
                            *valp = arm7_read32(arm7, base);            \
                        }                                               \
                        if (!pre)                                       \
                            base -= 4;                                  \
                    }                                                   \
                }                                                       \
            } else {                                                    \
                if (reg_list & (1 << 15)) {                             \
                    if (psr_user_force)                                 \
                        EMU_ERROR(ERROR_UNIMPLEMENTED);                 \
                    if (pre)                                            \
                        base -= 4;                                      \
                    arm7_write32(arm7, base,                            \
                                 arm7->reg[ARM7_REG_PC] + 4);           \
                    if (!pre)                                           \
                        base -= 4;                                      \
                }                                                       \
                                                                        \
                for (reg_no = 14; reg_no >= 0; reg_no--) {              \
                    if (reg_list & (1 << reg_no)) {                     \
                        if (pre)                                        \
                            base -= 4;                                  \
                        if (bank == ARM7_MODE_USER ||                   \
                            bank == ARM7_MODE_SYS) {                    \
                            uint32_t val = *arm7_gen_reg_bank(arm7,     \
                                                              reg_no,   \
                                                              bank);    \
                            arm7_write32(arm7, base, val);              \
                        } else {                                        \
                            uint32_t val = *arm7_gen_reg(arm7, reg_no); \
                            arm7_write32(arm7, base, val);              \
                        }                                               \
                        if (!pre)                                       \
                            base -= 4;                                  \
                    }                                                   \
                }                                                       \
            }                                                           \
        }                                                               \
                                                                        \
        /* Now handle the writeback.  Spec has some fairly */           \
        /* complicated rules about this when the rn is in the */        \
        /* register list, but the code above should have raised */      \
        /* an ERROR_UNIMPLEMENTED if that was the case. */              \
        if (writeback)                                                  \
            *baseptr = base;                                            \
                                                                        \
        if (load && (reg_list & (1 << 15))) {                           \
            if (arm7_thumb_mode(arm7))                                  \
                arm7->reg[ARM7_REG_PC] &= ~1;                           \
            else                                                        \
                arm7->reg[ARM7_REG_PC] &= ~3;                           \
            arm7_reset_pipeline(arm7);                                  \
            goto the_end;                                               \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 1 * S_CYCLE + 1 * N_CYCLE + 1 * I_CYCLE;                 \
    }

DEF_BLOCK_XFER_INST(eq)
DEF_BLOCK_XFER_INST(ne)
DEF_BLOCK_XFER_INST(cs)
DEF_BLOCK_XFER_INST(cc)
DEF_BLOCK_XFER_INST(mi)
DEF_BLOCK_XFER_INST(pl)
DEF_BLOCK_XFER_INST(vs)
DEF_BLOCK_XFER_INST(vc)
DEF_BLOCK_XFER_INST(hi)
DEF_BLOCK_XFER_INST(ls)
DEF_BLOCK_XFER_INST(ge)
DEF_BLOCK_XFER_INST(lt)
DEF_BLOCK_XFER_INST(gt)
DEF_BLOCK_XFER_INST(le)
DEF_BLOCK_XFER_INST(al)
DEF_BLOCK_XFER_INST(nv)

/*
 * MRS
 * Copy CPSR (or SPSR) to a register
 */
#define DEF_MRS_INST(cond)                                              \
    static unsigned                                                     \
    arm7_inst_mrs_##cond(struct arm7 *arm7, arm7_inst inst) {           \
        EVAL_COND(cond);                                                \
        bool src_psr = (1 << 22) & inst;                                \
        unsigned dst_reg = (inst >> 12) & 0xf;                          \
        uint32_t mode = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK;    \
                                                                        \
        uint32_t const *src_p;                                          \
        if (src_psr && mode != ARM7_MODE_USER && mode != ARM7_MODE_SYS) \
            src_p = arm7->reg + arm7_spsr_idx(arm7);                    \
        else                                                            \
            src_p = arm7->reg + ARM7_REG_CPSR;                          \
                                                                        \
        arm7_gen_reg_write(arm7, dst_reg, *src_p);                      \
                                                                        \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 1 * S_CYCLE;                                             \
    }                                                                   \

DEF_MRS_INST(eq)
DEF_MRS_INST(ne)
DEF_MRS_INST(cs)
DEF_MRS_INST(cc)
DEF_MRS_INST(mi)
DEF_MRS_INST(pl)
DEF_MRS_INST(vs)
DEF_MRS_INST(vc)
DEF_MRS_INST(hi)
DEF_MRS_INST(ls)
DEF_MRS_INST(ge)
DEF_MRS_INST(lt)
DEF_MRS_INST(gt)
DEF_MRS_INST(le)
DEF_MRS_INST(al)
DEF_MRS_INST(nv)

static void arm7_do_msr(struct arm7 *arm7, bool dst_psr,
                        unsigned fields, uint32_t val) {
    uint32_t psr_in, psr_out;
    unsigned spsr_idx;
    uint32_t old_cpsr = arm7->reg[ARM7_REG_CPSR];
    if (dst_psr) {
        unsigned old_mode = old_cpsr & ARM7_CPSR_M_MASK;
        if (old_mode == ARM7_MODE_USER || old_mode == ARM7_MODE_SYS)
            return;
        spsr_idx = arm7_spsr_idx(arm7);
        psr_in = arm7->reg[spsr_idx];
    } else {
        psr_in = old_cpsr;
    }

    uint32_t field_mask = 0;
    if (fields & 1)
        field_mask |= 0xff;
    if (fields & 2)
        field_mask |= 0xff00;
    if (fields & 4)
        field_mask |= 0xff0000;
    if (fields & 8)
        field_mask |= 0xff000000;

    psr_out = (psr_in & ~field_mask) | (val & field_mask);

    if (dst_psr)
        arm7->reg[spsr_idx] = psr_out;
    else
        arm7_cpsr_mode_change(arm7, psr_out);
}

#define DEF_MSR_IMM8_INST(cond)                                     \
    static unsigned                                                 \
    arm7_inst_msr_imm8_##cond(struct arm7 *arm7, arm7_inst inst) {  \
        EVAL_COND(cond);                                            \
                                                                    \
        /* if true, destination is SPSR instead of CPSR */          \
        bool dst_psr = inst & (1 << 22);                            \
        bool ignore_this;                                           \
        uint32_t val = decode_immed(inst, &ignore_this);            \
        unsigned field_mask = (inst >> 16) & 0xf;                   \
        arm7_do_msr(arm7, dst_psr, field_mask, val);                \
    cond_fail:                                                      \
        next_inst(arm7);                                            \
        return 1 * S_CYCLE;                                         \
    }

DEF_MSR_IMM8_INST(eq)
DEF_MSR_IMM8_INST(ne)
DEF_MSR_IMM8_INST(cs)
DEF_MSR_IMM8_INST(cc)
DEF_MSR_IMM8_INST(mi)
DEF_MSR_IMM8_INST(pl)
DEF_MSR_IMM8_INST(vs)
DEF_MSR_IMM8_INST(vc)
DEF_MSR_IMM8_INST(hi)
DEF_MSR_IMM8_INST(ls)
DEF_MSR_IMM8_INST(ge)
DEF_MSR_IMM8_INST(lt)
DEF_MSR_IMM8_INST(gt)
DEF_MSR_IMM8_INST(le)
DEF_MSR_IMM8_INST(al)
DEF_MSR_IMM8_INST(nv)

#define DEF_MSR_REG_INST(cond)                                      \
    static unsigned                                                 \
    arm7_inst_msr_reg_##cond(struct arm7 *arm7, arm7_inst inst) {   \
        EVAL_COND(cond);                                            \
                                                                    \
        /* if true, destination is SPSR instead of CPSR */          \
        bool dst_psr = inst & (1 << 22);                            \
        unsigned field_mask = (inst >> 16) & 0xf;                   \
        uint32_t src_val = *arm7_gen_reg(arm7, inst & 0xf);         \
        arm7_do_msr(arm7, dst_psr, field_mask, src_val);            \
    cond_fail:                                                      \
        next_inst(arm7);                                            \
        return 1 * S_CYCLE;                                         \
    }

DEF_MSR_REG_INST(eq)
DEF_MSR_REG_INST(ne)
DEF_MSR_REG_INST(cs)
DEF_MSR_REG_INST(cc)
DEF_MSR_REG_INST(mi)
DEF_MSR_REG_INST(pl)
DEF_MSR_REG_INST(vs)
DEF_MSR_REG_INST(vc)
DEF_MSR_REG_INST(hi)
DEF_MSR_REG_INST(ls)
DEF_MSR_REG_INST(ge)
DEF_MSR_REG_INST(lt)
DEF_MSR_REG_INST(gt)
DEF_MSR_REG_INST(le)
DEF_MSR_REG_INST(al)
DEF_MSR_REG_INST(nv)

#ifdef INVARIANTS
#define MUL_INVARIANTS_CHECK                                            \
    if ((BIT_RANGE(22, 27) & inst) || (((BIT_RANGE(4, 7) & inst) >> 4) != 9)) \
        CRITICAL_ERROR(ERROR_INTEGRITY);
#else
#define MUL_INVARIANTS_CHECK
#endif

#define DEF_MUL_INST(cond)                                              \
    static unsigned                                                     \
    arm7_inst_mul_##cond(struct arm7 *arm7, arm7_inst inst) {           \
        EVAL_COND(cond);                                                \
        bool accum = (bool)(inst & (1 << 21));                          \
        bool set_flags = (bool)(inst & (1 << 20));                      \
        unsigned rd = (BIT_RANGE(16, 19) & inst) >> 16;                 \
        unsigned rn = (BIT_RANGE(12, 15) & inst) >> 12;                 \
        unsigned rs = (BIT_RANGE(8, 11) & inst) >> 8;                   \
        unsigned rm = BIT_RANGE(0, 3) & inst;                           \
                                                                        \
        MUL_INVARIANTS_CHECK;                                           \
                                                                        \
        /* doc says you can't do this */                                \
        if (rd == 15 || rn == 15 || rs == 15 || rm == 15) {             \
            error_set_arm7_inst(inst);                                  \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                             \
        }                                                               \
                                                                        \
        uint32_t val = *arm7_gen_reg(arm7, rm) * *arm7_gen_reg(arm7, rs); \
        if (accum)                                                      \
            val += *arm7_gen_reg(arm7, rn);                             \
                                                                        \
        arm7_gen_reg_write(arm7, rd, val);                              \
                                                                        \
        if (set_flags) {                                                \
            uint32_t cpsr = arm7->reg[ARM7_REG_CPSR];                   \
            if (val & (1 << 31))                                        \
                cpsr |= ARM7_CPSR_N_MASK;                               \
            else                                                        \
                cpsr &= ~ARM7_CPSR_N_MASK;                              \
                                                                        \
            if (!val)                                                   \
                cpsr |= ARM7_CPSR_Z_MASK;                               \
            else                                                        \
                cpsr &= ~ARM7_CPSR_Z_MASK;                              \
                                                                        \
            /* apparently the value of C is undefined */                \
            cpsr &= ~ARM7_CPSR_C_MASK;                                  \
                                                                        \
            /* V flag is unaffected by this instruction */              \
                                                                        \
            arm7->reg[ARM7_REG_CPSR] = cpsr;                            \
        }                                                               \
                                                                        \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 4 * S_CYCLE;                                             \
    }

DEF_MUL_INST(eq)
DEF_MUL_INST(ne)
DEF_MUL_INST(cs)
DEF_MUL_INST(cc)
DEF_MUL_INST(mi)
DEF_MUL_INST(pl)
DEF_MUL_INST(vs)
DEF_MUL_INST(vc)
DEF_MUL_INST(hi)
DEF_MUL_INST(ls)
DEF_MUL_INST(ge)
DEF_MUL_INST(lt)
DEF_MUL_INST(gt)
DEF_MUL_INST(le)
DEF_MUL_INST(al)
DEF_MUL_INST(nv)

#define DEF_UMULL_INST(cond)                                      \
    static unsigned                                               \
    arm7_inst_umull_##cond(struct arm7 *arm7, arm7_inst inst) {   \
        bool s_flag = (inst >> 20) & 1;                                 \
        unsigned rm = inst & 0xf;                                       \
        unsigned rs = (inst >> 8) & 0xf;                                \
        unsigned rdlo = (inst >> 12) & 0xf;                             \
        unsigned rdhi = (inst >> 16) & 0xf;                             \
        uint64_t lhs = *arm7_gen_reg(arm7, rm);                         \
        uint64_t rhs = *arm7_gen_reg(arm7, rs);                         \
        uint64_t prod = lhs * rhs;                                      \
        uint64_t prodlo = prod & 0xffffffffULL;                         \
        uint64_t prodhi = prod >> 32;                                   \
        arm7_gen_reg_write(arm7, rdlo, prodlo);                         \
        arm7_gen_reg_write(arm7, rdhi, prodhi);                         \
        if (s_flag) {                                                   \
            uint32_t n_flag = prodhi & (1 << 31) ? ARM7_CPSR_N_MASK : 0; \
            uint32_t z_flag = !(prodhi || prodlo) ? ARM7_CPSR_Z_MASK : 0; \
            arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_N_MASK | ARM7_CPSR_Z_MASK); \
            arm7->reg[ARM7_REG_CPSR] |= n_flag | z_flag;                \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 1; /* TODO: not sure how the timing works out here */    \
    }

DEF_UMULL_INST(eq)
DEF_UMULL_INST(ne)
DEF_UMULL_INST(cs)
DEF_UMULL_INST(cc)
DEF_UMULL_INST(mi)
DEF_UMULL_INST(pl)
DEF_UMULL_INST(vs)
DEF_UMULL_INST(vc)
DEF_UMULL_INST(hi)
DEF_UMULL_INST(ls)
DEF_UMULL_INST(ge)
DEF_UMULL_INST(lt)
DEF_UMULL_INST(gt)
DEF_UMULL_INST(le)
DEF_UMULL_INST(al)
DEF_UMULL_INST(nv)

#define DEF_SMULL_INST(cond)                                            \
    static unsigned                                                     \
    arm7_inst_smull_##cond(struct arm7 *arm7, arm7_inst inst) {         \
        bool s_flag = (inst >> 20) & 1;                                 \
        unsigned rm = inst & 0xf;                                       \
        unsigned rs = (inst >> 8) & 0xf;                                \
        unsigned rdlo = (inst >> 12) & 0xf;                             \
        unsigned rdhi = (inst >> 16) & 0xf;                             \
        int64_t lhs = *arm7_gen_reg(arm7, rm);                          \
        int64_t rhs = *arm7_gen_reg(arm7, rs);                          \
        if (lhs & (1 << 31))                                            \
            lhs |= 0xffffffff00000000LL;                                \
        if (rhs & (1 << 31))                                            \
            rhs |= 0xffffffff00000000LL;                                \
        int64_t prod = lhs * rhs;                                       \
        int64_t prodlo = prod & 0xffffffffULL;                          \
        int64_t prodhi = prod >> 32;                                    \
        arm7_gen_reg_write(arm7, rdlo, prodlo);                         \
        arm7_gen_reg_write(arm7, rdhi, prodhi);                         \
        if (s_flag) {                                                   \
            uint32_t n_flag = prodhi & (1 << 31) ? ARM7_CPSR_N_MASK : 0; \
            uint32_t z_flag = !(prodhi || prodlo) ? ARM7_CPSR_Z_MASK : 0; \
            arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_N_MASK | ARM7_CPSR_Z_MASK); \
            arm7->reg[ARM7_REG_CPSR] |= n_flag | z_flag;                \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 1; /* TODO: not sure how the timing works out here */    \
    }

DEF_SMULL_INST(eq)
DEF_SMULL_INST(ne)
DEF_SMULL_INST(cs)
DEF_SMULL_INST(cc)
DEF_SMULL_INST(mi)
DEF_SMULL_INST(pl)
DEF_SMULL_INST(vs)
DEF_SMULL_INST(vc)
DEF_SMULL_INST(hi)
DEF_SMULL_INST(ls)
DEF_SMULL_INST(ge)
DEF_SMULL_INST(lt)
DEF_SMULL_INST(gt)
DEF_SMULL_INST(le)
DEF_SMULL_INST(al)
DEF_SMULL_INST(nv)

#define DEF_UMLAL_INST(cond)                                      \
    static unsigned                                               \
    arm7_inst_umlal_##cond(struct arm7 *arm7, arm7_inst inst) {   \
        bool s_flag = (inst >> 20) & 1;                                 \
        unsigned rm = inst & 0xf;                                       \
        unsigned rs = (inst >> 8) & 0xf;                                \
        unsigned rdlo = (inst >> 12) & 0xf;                             \
        unsigned rdhi = (inst >> 16) & 0xf;                             \
        uint64_t lhs = *arm7_gen_reg(arm7, rm);                         \
        uint64_t rhs = *arm7_gen_reg(arm7, rs);                         \
        uint64_t prod = lhs * rhs;                                      \
        uint64_t dstlo = *arm7_gen_reg(arm7, rdlo);                     \
        uint64_t dsthi = *arm7_gen_reg(arm7, rdhi);                     \
        uint64_t dstval = ((dsthi << 32) | dstlo) + prod;               \
        uint64_t prodlo = dstval & 0xffffffffULL;                       \
        uint64_t prodhi = dstval >> 32;                                 \
        arm7_gen_reg_write(arm7, rdlo, prodlo);                         \
        arm7_gen_reg_write(arm7, rdhi, prodhi);                         \
        if (s_flag) {                                                   \
            uint32_t n_flag = prodhi & (1 << 31) ? ARM7_CPSR_N_MASK : 0; \
            uint32_t z_flag = !(prodhi || prodlo) ? ARM7_CPSR_Z_MASK : 0; \
            arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_N_MASK | ARM7_CPSR_Z_MASK); \
            arm7->reg[ARM7_REG_CPSR] |= n_flag | z_flag;                \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 1; /* TODO: not sure how the timing works out here */    \
    }

DEF_UMLAL_INST(eq)
DEF_UMLAL_INST(ne)
DEF_UMLAL_INST(cs)
DEF_UMLAL_INST(cc)
DEF_UMLAL_INST(mi)
DEF_UMLAL_INST(pl)
DEF_UMLAL_INST(vs)
DEF_UMLAL_INST(vc)
DEF_UMLAL_INST(hi)
DEF_UMLAL_INST(ls)
DEF_UMLAL_INST(ge)
DEF_UMLAL_INST(lt)
DEF_UMLAL_INST(gt)
DEF_UMLAL_INST(le)
DEF_UMLAL_INST(al)
DEF_UMLAL_INST(nv)

#define DEF_SMLAL_INST(cond)                                            \
    static unsigned                                                     \
    arm7_inst_smlal_##cond(struct arm7 *arm7, arm7_inst inst) {         \
        bool s_flag = (inst >> 20) & 1;                                 \
        unsigned rm = inst & 0xf;                                       \
        unsigned rs = (inst >> 8) & 0xf;                                \
        unsigned rdlo = (inst >> 12) & 0xf;                             \
        unsigned rdhi = (inst >> 16) & 0xf;                             \
        int64_t lhs = *arm7_gen_reg(arm7, rm);                          \
        int64_t rhs = *arm7_gen_reg(arm7, rs);                          \
        if (lhs & (1 << 31))                                            \
            lhs |= 0xffffffff00000000LL;                                \
        if (rhs & (1 << 31))                                            \
            rhs |= 0xffffffff00000000LL;                                \
        int64_t prod = lhs * rhs;                                       \
        int64_t dstlo = *arm7_gen_reg(arm7, rdlo);                     \
        int64_t dsthi = *arm7_gen_reg(arm7, rdhi);                     \
        int64_t dstval = ((dsthi << 32) | dstlo) + prod;                \
        int64_t prodlo = dstval & 0xffffffffULL;                        \
        int64_t prodhi = dstval >> 32;                                  \
        arm7_gen_reg_write(arm7, rdlo, prodlo);                         \
        arm7_gen_reg_write(arm7, rdhi, prodhi);                         \
        if (s_flag) {                                                   \
            uint32_t n_flag = prodhi & (1 << 31) ? ARM7_CPSR_N_MASK : 0; \
            uint32_t z_flag = !(prodhi || prodlo) ? ARM7_CPSR_Z_MASK : 0; \
            arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_N_MASK | ARM7_CPSR_Z_MASK); \
            arm7->reg[ARM7_REG_CPSR] |= n_flag | z_flag;                \
        }                                                               \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
        return 1; /* TODO: not sure how the timing works out here */    \
    }

DEF_SMLAL_INST(eq)
DEF_SMLAL_INST(ne)
DEF_SMLAL_INST(cs)
DEF_SMLAL_INST(cc)
DEF_SMLAL_INST(mi)
DEF_SMLAL_INST(pl)
DEF_SMLAL_INST(vs)
DEF_SMLAL_INST(vc)
DEF_SMLAL_INST(hi)
DEF_SMLAL_INST(ls)
DEF_SMLAL_INST(ge)
DEF_SMLAL_INST(lt)
DEF_SMLAL_INST(gt)
DEF_SMLAL_INST(le)
DEF_SMLAL_INST(al)
DEF_SMLAL_INST(nv)

#define DEF_COND_TBL(opcode)                                        \
    static arm7_op_fn const arm7_##opcode##_cond_tbl[16] = {        \
        arm7_inst_##opcode##_eq,                                    \
        arm7_inst_##opcode##_ne,                                    \
        arm7_inst_##opcode##_cs,                                    \
        arm7_inst_##opcode##_cc,                                    \
        arm7_inst_##opcode##_mi,                                    \
        arm7_inst_##opcode##_pl,                                    \
        arm7_inst_##opcode##_vs,                                    \
        arm7_inst_##opcode##_vc,                                    \
        arm7_inst_##opcode##_hi,                                    \
        arm7_inst_##opcode##_ls,                                    \
        arm7_inst_##opcode##_ge,                                    \
        arm7_inst_##opcode##_lt,                                    \
        arm7_inst_##opcode##_gt,                                    \
        arm7_inst_##opcode##_le,                                    \
        arm7_inst_##opcode##_al,                                    \
        arm7_inst_##opcode##_nv                                     \
    }

#define DEF_DATA_OP_INST(op_name, cond, is_logic, require_s, write_result) \
    static unsigned                                                     \
    arm7_inst_##op_name##_##cond(struct arm7 *arm7, arm7_inst inst) {   \
        EVAL_COND(cond);                                                \
        bool s_flag = inst & (1 << 20);                                 \
        bool i_flag = inst & (1 << 25);                                 \
        unsigned rn = (inst >> 16) & 0xf;                               \
        unsigned rd = (inst >> 12) & 0xf;                               \
                                                                        \
        bool carry_in = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_C_MASK;    \
        bool n_out, c_out, z_out, v_out;                                \
        bool thumb_mode = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_T_MASK;  \
                                                                        \
        uint32_t input_1 = *arm7_gen_reg(arm7, rn);                     \
        uint32_t input_2;                                               \
                                                                        \
        c_out = carry_in;                                               \
        if (i_flag) {                                                   \
            input_2 = decode_immed(inst, &c_out);                       \
        } else {                                                        \
            input_2 = decode_shift(arm7, inst, &c_out);                 \
            if ((inst & (1 << 4)) && rn == 15)                          \
                input_1 += 4;                                           \
        }                                                               \
                                                                        \
        uint32_t res = DATA_OP_FUNC_NAME(op_name)(input_1, input_2,     \
                                                  carry_in, &n_out,     \
                                                  &c_out, &z_out, &v_out); \
        if (s_flag && rd != 15) {                                       \
            if (is_logic) {                                             \
                uint32_t z_flag = z_out ? ARM7_CPSR_Z_MASK : 0;         \
                uint32_t n_flag = n_out ? ARM7_CPSR_N_MASK : 0;         \
                uint32_t c_flag = c_out ? ARM7_CPSR_C_MASK : 0;         \
                arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_Z_MASK |        \
                                              ARM7_CPSR_N_MASK |        \
                                              ARM7_CPSR_C_MASK);        \
                arm7->reg[ARM7_REG_CPSR] |= (z_flag | n_flag | c_flag); \
            } else {                                                    \
                uint32_t z_flag = z_out ? ARM7_CPSR_Z_MASK : 0;         \
                uint32_t n_flag = n_out ? ARM7_CPSR_N_MASK : 0;         \
                uint32_t c_flag = c_out ? ARM7_CPSR_C_MASK : 0;         \
                uint32_t v_flag = v_out ? ARM7_CPSR_V_MASK : 0;         \
                arm7->reg[ARM7_REG_CPSR] &= ~(ARM7_CPSR_Z_MASK |        \
                                              ARM7_CPSR_N_MASK |        \
                                              ARM7_CPSR_C_MASK |        \
                                              ARM7_CPSR_V_MASK);        \
                arm7->reg[ARM7_REG_CPSR] |= (z_flag | n_flag |          \
                                             c_flag | v_flag);          \
            }                                                           \
        } else if (s_flag && rd == 15) {                                \
            uint32_t curmode = arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK; \
            if (curmode != ARM7_MODE_USER && curmode != ARM7_MODE_SYS)  \
                arm7_cpsr_mode_change(arm7, arm7->reg[arm7_spsr_idx(arm7)]); \
        } else if (require_s) {                                         \
            CRITICAL_ERROR(ERROR_INTEGRITY);                            \
        }                                                               \
                                                                        \
        if (write_result) {                                             \
            arm7_gen_reg_write(arm7, rd, res);                          \
            if (rd == 15) {                                             \
                arm7_reset_pipeline(arm7);                              \
                goto the_end;                                           \
            }                                                           \
        }                                                               \
                                                                        \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 2 * S_CYCLE + 1 * N_CYCLE;                               \
    }

#define DEF_DATA_OP_INST_ALL_CONDS(op_name, is_logic, require_s, write_result) \
    DEF_DATA_OP_INST(op_name, eq, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, ne, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, cs, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, cc, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, mi, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, pl, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, vs, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, vc, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, hi, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, ls, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, ge, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, lt, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, gt, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, le, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, al, is_logic, require_s, write_result)    \
    DEF_DATA_OP_INST(op_name, nv, is_logic, require_s, write_result)

DEF_DATA_OP_INST_ALL_CONDS(orr, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(eor, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(and, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(bic, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(mov, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(add, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(adc, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(sub, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(sbc, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(rsb, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(rsc, false, false, true)
DEF_DATA_OP_INST_ALL_CONDS(cmp, false, true, false)
DEF_DATA_OP_INST_ALL_CONDS(tst, true, true, false)
DEF_DATA_OP_INST_ALL_CONDS(teq, true, true, false)
DEF_DATA_OP_INST_ALL_CONDS(mvn, true, false, true)
DEF_DATA_OP_INST_ALL_CONDS(cmn, false, true, false)

#define DEF_SWI_INST(cond)                                              \
    static unsigned                                                     \
    arm7_inst_swi_##cond(struct arm7 *arm7, arm7_inst inst) {           \
        EVAL_COND(cond);                                                \
        LOG_DBG("ARM7 SWI instruction used at PC=%08x\n",               \
                (unsigned)*arm7_gen_reg(arm7, 15));                     \
        arm7->excp |= ARM7_EXCP_SWI;                                    \
        arm7_excp_refresh(arm7);                                        \
        /* it is not a mistake that I have chosen */                    \
        /* to not call next_inst here */                                \
        goto the_end;                                                   \
    cond_fail:                                                          \
        next_inst(arm7);                                                \
    the_end:                                                            \
        return 2 * S_CYCLE + 1 * N_CYCLE;                               \
    }

DEF_SWI_INST(eq)
DEF_SWI_INST(ne)
DEF_SWI_INST(cs)
DEF_SWI_INST(cc)
DEF_SWI_INST(mi)
DEF_SWI_INST(pl)
DEF_SWI_INST(vs)
DEF_SWI_INST(vc)
DEF_SWI_INST(hi)
DEF_SWI_INST(ls)
DEF_SWI_INST(ge)
DEF_SWI_INST(lt)
DEF_SWI_INST(gt)
DEF_SWI_INST(le)
DEF_SWI_INST(al)
DEF_SWI_INST(nv)

#define DEF_SWAP_INST(cond)                                         \
    static unsigned                                                 \
    arm7_inst_swap_##cond(struct arm7 *arm7, arm7_inst inst) {      \
        EVAL_COND(cond);                                            \
        unsigned n_bytes = ((inst >> 22) & 1) ? 1 : 4;              \
        unsigned src_reg = inst & 0xf;                              \
        unsigned dst_reg = (inst >> 12) & 0xf;                      \
        unsigned addr_reg = (inst >> 16) & 0xf;                     \
                                                                    \
        if (addr_reg == 15 || src_reg == 15 || dst_reg == 15)       \
            EMU_ERROR(ERROR_UNIMPLEMENTED);                         \
                                                                    \
        uint32_t addr = *arm7_gen_reg(arm7, addr_reg);              \
                                                                    \
        if (n_bytes == 4 && addr % 4)                               \
            LOG_ERROR("TODO: unaligned ARM7 word swaps");           \
                                                                    \
        if (n_bytes == 4) {                                         \
            uint32_t dat_in = arm7_read32(arm7, addr);              \
            uint32_t dat_out = *arm7_gen_reg(arm7, src_reg);        \
            arm7_write32(arm7, addr, dat_out);                      \
            arm7_gen_reg_write(arm7, dst_reg, dat_in);              \
        } else {                                                    \
            uint8_t dat_in = arm7_read8(arm7, addr);                \
            uint8_t dat_out = *arm7_gen_reg(arm7, src_reg);         \
            arm7_write8(arm7, addr, dat_out);                       \
            arm7_gen_reg_write(arm7, dst_reg, dat_in);              \
        }                                                           \
    cond_fail:                                                      \
        next_inst(arm7);                                            \
        return 2 * S_CYCLE + 1 * N_CYCLE;                           \
    }

DEF_SWAP_INST(eq)
DEF_SWAP_INST(ne)
DEF_SWAP_INST(cs)
DEF_SWAP_INST(cc)
DEF_SWAP_INST(mi)
DEF_SWAP_INST(pl)
DEF_SWAP_INST(vs)
DEF_SWAP_INST(vc)
DEF_SWAP_INST(hi)
DEF_SWAP_INST(ls)
DEF_SWAP_INST(ge)
DEF_SWAP_INST(lt)
DEF_SWAP_INST(gt)
DEF_SWAP_INST(le)
DEF_SWAP_INST(al)
DEF_SWAP_INST(nv)

arm7_op_fn arm7_decode(struct arm7 *arm7, arm7_inst inst) {
    DEF_COND_TBL(branch);
    DEF_COND_TBL(ldr_str);
    DEF_COND_TBL(ldrh_strh);
    DEF_COND_TBL(ldrsh);
    DEF_COND_TBL(ldrsb);
    DEF_COND_TBL(block_xfer);
    DEF_COND_TBL(mrs);
    DEF_COND_TBL(msr_imm8);
    DEF_COND_TBL(msr_reg);
    DEF_COND_TBL(mul);
    DEF_COND_TBL(umull);
    DEF_COND_TBL(smull);
    DEF_COND_TBL(umlal);
    DEF_COND_TBL(smlal);
    DEF_COND_TBL(orr);
    DEF_COND_TBL(eor);
    DEF_COND_TBL(and);
    DEF_COND_TBL(bic);
    DEF_COND_TBL(mov);
    DEF_COND_TBL(add);
    DEF_COND_TBL(adc);
    DEF_COND_TBL(sub);
    DEF_COND_TBL(sbc);
    DEF_COND_TBL(rsb);
    DEF_COND_TBL(rsc);
    DEF_COND_TBL(cmp);
    DEF_COND_TBL(tst);
    DEF_COND_TBL(teq);
    DEF_COND_TBL(mvn);
    DEF_COND_TBL(cmn);
    DEF_COND_TBL(swi);
    DEF_COND_TBL(swap);
    DEF_COND_TBL(bx);

    // data processing instructions - invalid if I bit (25) is 0 and bits 7 and 4 are 1
    if ((inst & MASK_B) == VAL_B) {
        return arm7_branch_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_LDR_STR) == VAL_LDR_STR) {
        return arm7_ldr_str_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_LDRH_STRH) == VAL_LDRH_STRH) {
        return arm7_ldrh_strh_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_LDRSH) == VAL_LDRSH) {
        return arm7_ldrsh_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_LDRSB) == VAL_LDRSB) {
        return arm7_ldrsb_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_BLOCK_XFER) == VAL_BLOCK_XFER) {
        return arm7_block_xfer_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MRS) == VAL_MRS) {
        return arm7_mrs_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MSR_IMM8) == VAL_MSR_IMM8) {
        return arm7_msr_imm8_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MSR_REG) == VAL_MSR_REG) {
        return arm7_msr_reg_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MUL) == VAL_MUL) {
        return arm7_mul_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_AND) == VAL_AND &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_and_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_EOR) == VAL_EOR &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_eor_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SUB) == VAL_SUB &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_sub_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_RSB) == VAL_RSB &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_rsb_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_ADD) == VAL_ADD &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_add_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_ADC) == VAL_ADC &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_adc_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SBC) == VAL_SBC &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_sbc_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_RSC) == VAL_RSC &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_rsc_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_TST) == VAL_TST &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_tst_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_TEQ) == VAL_TEQ &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_teq_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_CMP) == VAL_CMP &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_cmp_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_CMN) == VAL_CMN &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_cmn_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_ORR) == VAL_ORR &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_orr_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MOV) == VAL_MOV &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_mov_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_BIC) == VAL_BIC &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_bic_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_MVN) == VAL_MVN &&
           (inst & MASK_NOT_DATA_PROCESSING) != VAL_NOT_DATA_PROCESSING) {
    return arm7_mvn_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SWI) == VAL_SWI) {
        return arm7_swi_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SWAP) == VAL_SWAP) {
        return arm7_swap_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_BX) == VAL_BX) {
        return arm7_bx_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_UMULL) == VAL_UMULL) {
        return arm7_umull_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SMULL) == VAL_SMULL) {
        return arm7_smull_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_UMLAL) == VAL_UMLAL) {
        return arm7_umlal_cond_tbl[(inst >> 28) & 0xf];
    } else if ((inst & MASK_SMLAL) == VAL_SMLAL) {
        return arm7_smlal_cond_tbl[(inst >> 28) & 0xf];
    }

    error_set_arm7_inst(inst);
    error_set_arm7_pc(arm7->reg[ARM7_REG_PC]);
    EMU_ERROR(ERROR_UNIMPLEMENTED);
}

static inline uint32_t ror(uint32_t in, unsigned n_bits) {
    n_bits %= 32;
    return (in >> n_bits) | (in << (32 - n_bits));
}

static uint32_t decode_immed(arm7_inst inst, bool *carry) {
    uint32_t n_bits = 2 * ((inst & BIT_RANGE(8, 11)) >> 8);
    uint32_t imm = inst & BIT_RANGE(0, 7);

    uint32_t ret = ror(imm, n_bits);
    if (n_bits)
        *carry = (ret >> 31) & 1;
    return ret;
}

static uint32_t
decode_shift_ldr_str(struct arm7 *arm7, arm7_inst inst, bool *carry) {
    bool amt_in_reg = inst & (1 << 4);
    unsigned shift_fn = (inst & BIT_RANGE(5, 6)) >> 5;

    if (amt_in_reg) {
        // Docs say this feature isn't available for load/store
        EMU_ERROR(ERROR_UNIMPLEMENTED);
    }

    return decode_shift_by_immediate(arm7, shift_fn, inst & 0xf,
                                     (inst & BIT_RANGE(7, 11)) >> 7, carry);
}

/*
 * decodes val_reg << shift_amt_reg, where "<<" is replaced by
 * whatever shift function we're actually using.
 */
static uint32_t
decode_shift_by_register(struct arm7 *arm7, unsigned shift_fn,
                         unsigned val_reg, unsigned shift_amt_reg,
                         bool *carry) {
    if (shift_amt_reg == 15)
        EMU_ERROR(ERROR_UNIMPLEMENTED);
    unsigned shift_amt = *arm7_gen_reg(arm7, shift_amt_reg) & 0xff;

    unsigned val_to_shift = *arm7_gen_reg(arm7, val_reg);
    if (val_reg == 15)
        val_to_shift += 4; // pipeline effects

    if (shift_amt == 0)
        return val_to_shift;

    uint32_t ret_val;
    switch (shift_fn) {
    case 0:
        // logical left shift
        if (shift_amt < 32) {
            *carry = (bool)((1 << (31 - shift_amt + 1)) & val_to_shift);
            return val_to_shift << shift_amt;
        } else if (shift_amt == 32) {
            *carry = val_to_shift & 1;
            return 0;
        } else {
            *carry = false;
            return 0;
        }
    case 1:
        // logical right shift
        if (shift_amt < 32) {
            *carry = ((1 << (shift_amt - 1)) & val_to_shift);
            return val_to_shift >> shift_amt;
        } else if (shift_amt == 32) {
            *carry = (bool)((1 << 31) & val_to_shift);
            return 0;
        } else {
            *carry = false;
            return 0;
        }
    case 2:
        // arithmetic right shift
        if (shift_amt < 32) {
            *carry = ((1 << (shift_amt - 1)) & val_to_shift);
            return ((int32_t)val_to_shift) >> shift_amt;
        } else {
            *carry = (bool)((1 << 31) & val_to_shift);
            return *carry ? ~0 : 0;
        }
    case 3:
        // right-rotate
        ret_val = ror(val_to_shift, shift_amt);
        *carry = (1 << 31) & ret_val;
        return ret_val;
    }

    CRITICAL_ERROR(ERROR_INTEGRITY);
}

static uint32_t
decode_shift_by_immediate(struct arm7 *arm7, unsigned shift_fn,
                          unsigned val_reg, unsigned shift_amt, bool *carry) {
#ifdef INVARIANTS
    // it comes from a 5-bit field in the instruction
    if (shift_amt > 31)
        CRITICAL_ERROR(ERROR_INTEGRITY);
#endif

    unsigned val_to_shift = *arm7_gen_reg(arm7, val_reg);
    /* if (val_reg == 15) */
    /*     val_to_shift += 4; // pipeline effects */

    uint32_t ret_val;
    switch (shift_fn) {
    case 0:
        // logical left-shift
        if (shift_amt) {
            *carry = (bool)((1 << (31 - shift_amt + 1)) & val_to_shift);
            return val_to_shift << shift_amt;
        } else {
            // carry flag is unaffected by LSL #0
            return val_to_shift;
        }
    case 1:
        // logical right-shift
        if (shift_amt) {
            *carry = ((1 << (shift_amt - 1)) & val_to_shift);
            return val_to_shift >> shift_amt;
        } else {
            *carry = (bool)((1 << 31) & val_to_shift);
            return 0;
        }
    case 2:
        // arithmetic right-shift
        if (shift_amt) {
            *carry = ((1 << (shift_amt - 1)) & val_to_shift);
            return ((int32_t)val_to_shift) >> shift_amt;
        } else {
            *carry = (bool)((1 << 31) & val_to_shift);
            return *carry ? ~0 : 0;
        }
    case 3:
        if (shift_amt) {
            // right-rotate
            ret_val = ror(val_to_shift, shift_amt);
            *carry = (1 << 31) & ret_val;
            return ret_val;
        } else {
            // rotate right extend
            uint32_t new_msb = *carry ? 0x80000000 : 0;
            *carry = (bool)(val_to_shift & 1);
            return (val_to_shift >> 1) | new_msb;
        }
    }

    CRITICAL_ERROR(ERROR_INTEGRITY);
}

static uint32_t
decode_shift(struct arm7 *arm7, arm7_inst inst, bool *carry) {
    bool amt_in_reg = inst & (1 << 4);
    unsigned shift_fn = (inst & BIT_RANGE(5, 6)) >> 5;

    /*
     * For all cases except logical left-shift, a shift of 0 is actually a
     * shift of 32.  For now I've chosen to raise an ERROR_UNIMPLEMENTED when
     * that happens because I'd rather not think about it.
     */
    if (amt_in_reg) {
        if (inst & (1 << 7)) {
            /*
             * setting bit 7 and bit 4 is illegal.  If this happens, it means
             * we have a decoder error.
             */
            CRITICAL_ERROR(ERROR_INTEGRITY);
        }

        return decode_shift_by_register(arm7, shift_fn, inst & 0xf,
                                        (inst & BIT_RANGE(8, 11)) >> 8, carry);
    } else {
        return decode_shift_by_immediate(arm7, shift_fn, inst & 0xf,
                                         (inst & BIT_RANGE(7, 11)) >> 7, carry);
    }
}

static unsigned arm7_spsr_idx(struct arm7 *arm7) {
    switch (arm7->reg[ARM7_REG_CPSR] & ARM7_CPSR_M_MASK) {
    case ARM7_MODE_FIQ:
        return ARM7_REG_SPSR_FIQ;
    case ARM7_MODE_IRQ:
        return ARM7_REG_SPSR_IRQ;
    case ARM7_MODE_SVC:
        return ARM7_REG_SPSR_SVC;
    case ARM7_MODE_ABT:
        return ARM7_REG_SPSR_ABT;
    case ARM7_MODE_UND:
        return ARM7_REG_SPSR_UND;
    case ARM7_MODE_USER:
    case ARM7_MODE_SYS:
        /* User mode doesn't have an SPSR */
    default:
        EMU_ERROR(ERROR_REG_DOES_NOT_EXIST);
    }
}

void arm7_get_regs(struct arm7 *arm7, void *dat_out) {
    memcpy(dat_out, arm7->reg, sizeof(uint32_t) * ARM7_REGISTER_COUNT);
    unsigned reg_no;
    for (reg_no = 0; reg_no <= 15; reg_no++) {
        memcpy(((char*)dat_out) + (reg_no + ARM7_REG_R0) * sizeof(uint32_t),
               arm7_gen_reg(arm7, reg_no), sizeof(uint32_t));
    }
    for (reg_no = 8; reg_no <= 14; reg_no++) {
        memcpy(((char*)dat_out) + ((reg_no - 8) + ARM7_REG_R8_FIQ) * sizeof(uint32_t),
               arm7_gen_reg_bank(arm7, reg_no, ARM7_MODE_FIQ),
               sizeof(uint32_t));
    }
    for (reg_no = 13; reg_no <= 14; reg_no++) {
        memcpy(((char*)dat_out) + ((reg_no - 13) + ARM7_REG_R13_SVC) * sizeof(uint32_t),
               arm7_gen_reg_bank(arm7, reg_no, ARM7_MODE_SVC),
               sizeof(uint32_t));
    }
    for (reg_no = 13; reg_no <= 14; reg_no++) {
        memcpy(((char*)dat_out) + ((reg_no - 13) + ARM7_REG_R13_ABT) * sizeof(uint32_t),
               arm7_gen_reg_bank(arm7, reg_no, ARM7_MODE_ABT),
               sizeof(uint32_t));
    }
    for (reg_no = 13; reg_no <= 14; reg_no++) {
        memcpy(((char*)dat_out) + ((reg_no - 13) + ARM7_REG_R13_IRQ) * sizeof(uint32_t),
               arm7_gen_reg_bank(arm7, reg_no, ARM7_MODE_IRQ),
               sizeof(uint32_t));
    }
    for (reg_no = 13; reg_no <= 14; reg_no++) {
        memcpy(((char*)dat_out) + ((reg_no - 13) + ARM7_REG_R13_UND) * sizeof(uint32_t),
               arm7_gen_reg_bank(arm7, reg_no, ARM7_MODE_UND),
               sizeof(uint32_t));
    }
}

static DEF_ERROR_U32_ATTR(arm7_reg_r0)
static DEF_ERROR_U32_ATTR(arm7_reg_r1)
static DEF_ERROR_U32_ATTR(arm7_reg_r2)
static DEF_ERROR_U32_ATTR(arm7_reg_r3)
static DEF_ERROR_U32_ATTR(arm7_reg_r4)
static DEF_ERROR_U32_ATTR(arm7_reg_r5)
static DEF_ERROR_U32_ATTR(arm7_reg_r6)
static DEF_ERROR_U32_ATTR(arm7_reg_r7)
static DEF_ERROR_U32_ATTR(arm7_reg_r8)
static DEF_ERROR_U32_ATTR(arm7_reg_r9)
static DEF_ERROR_U32_ATTR(arm7_reg_r10)
static DEF_ERROR_U32_ATTR(arm7_reg_r11)
static DEF_ERROR_U32_ATTR(arm7_reg_r12)
static DEF_ERROR_U32_ATTR(arm7_reg_r13)
static DEF_ERROR_U32_ATTR(arm7_reg_r14)
static DEF_ERROR_U32_ATTR(arm7_reg_r15)

// putthing this here even though it's just an alias for r15
static DEF_ERROR_U32_ATTR(arm7_reg_pc)

static DEF_ERROR_U32_ATTR(arm7_reg_r8_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r9_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r10_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r11_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r12_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r13_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r14_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_r13_svc)
static DEF_ERROR_U32_ATTR(arm7_reg_r14_svc)
static DEF_ERROR_U32_ATTR(arm7_reg_r13_abt)
static DEF_ERROR_U32_ATTR(arm7_reg_r14_abt)
static DEF_ERROR_U32_ATTR(arm7_reg_r13_irq)
static DEF_ERROR_U32_ATTR(arm7_reg_r14_irq)
static DEF_ERROR_U32_ATTR(arm7_reg_r13_und)
static DEF_ERROR_U32_ATTR(arm7_reg_r14_und)

static DEF_ERROR_U32_ATTR(arm7_reg_cpsr)

static DEF_ERROR_U32_ATTR(arm7_reg_spsr_fiq)
static DEF_ERROR_U32_ATTR(arm7_reg_spsr_svc)
static DEF_ERROR_U32_ATTR(arm7_reg_spsr_abt)
static DEF_ERROR_U32_ATTR(arm7_reg_spsr_irq)
static DEF_ERROR_U32_ATTR(arm7_reg_spsr_und)

void arm7_set_irq(struct arm7 *arm7, arm7_irq_fn handler, void *argp) {
    if (handler && arm7->irq_line) {
        LOG_ERROR("ERROR: attempt to overwirte arm7 irq handler!\n");
        CRITICAL_ERROR(ERROR_INTEGRITY);
    }
    arm7->irq_line = handler;
    arm7->irq_line_arg = argp;
}

void arm7_set_fiq(struct arm7 *arm7, arm7_irq_fn handler, void *argp) {
    if (handler && arm7->fiq_line) {
        LOG_ERROR("ERROR: attempt to overwirte arm7 fiq handler!\n");
        CRITICAL_ERROR(ERROR_INTEGRITY);
    }
    arm7->fiq_line = handler;
    arm7->fiq_line_arg = argp;
}

static bool arm7_excp_refresh_event_handler(struct SchedEvent *event) {
    struct arm7 *arm7 = event->arg_ptr;
    arm7_excp_refresh_outside_cpu_context(arm7);
    arm7->excp_refresh_event_scheduled = false;
    return false;
}

void arm7_excp_refresh(struct arm7 *arm7) {
    if (!arm7->excp_refresh_event_scheduled) {
        arm7->excp_refresh_event_scheduled = true;
        arm7->excp_refresh_event.when = clock_cycle_stamp(arm7->clk);
        sched_event(arm7->clk, &arm7->excp_refresh_event);
    }
}

void arm7_excp_refresh_outside_cpu_context(struct arm7 *arm7) {
    enum arm7_excp excp = arm7->excp;
    uint32_t cpsr = arm7->reg[ARM7_REG_CPSR];

    // TODO: maybe need to check for ARM7_EXCP_DATA_ABORT?

    if (arm7->fiq_line && arm7->fiq_line(arm7->fiq_line_arg))
        excp |= ARM7_EXCP_FIQ;
    else
        excp &= ~ARM7_EXCP_FIQ;

    if (arm7->irq_line && arm7->irq_line(arm7->irq_line_arg))
        excp |= ARM7_EXCP_IRQ;
    else
        excp &= ~ARM7_EXCP_IRQ;

    if (excp & ARM7_EXCP_RESET) {
        arm7_cpsr_mode_change(arm7, (cpsr & ~(ARM7_CPSR_M_MASK | ARM7_CPSR_T_MASK)) |
                              ARM7_MODE_SVC | ARM7_CPSR_I_MASK |
                              ARM7_CPSR_F_MASK);
        arm7->reg[ARM7_REG_SPSR_SVC] = cpsr;
        *arm7_gen_reg(arm7, 14) = arm7_pc_next(arm7) + 4;
        arm7->reg[ARM7_REG_PC] = 0;
        arm7_reset_pipeline(arm7);
        arm7->excp &= ~ARM7_EXCP_RESET;
    } else if ((excp & ARM7_EXCP_FIQ) && !(cpsr & ARM7_CPSR_F_MASK)) {
        LOG_DBG("FIQ jump to 0x1c\n");
        arm7_cpsr_mode_change(arm7, (cpsr & ~(ARM7_CPSR_M_MASK | ARM7_CPSR_T_MASK)) |
                              ARM7_MODE_FIQ | ARM7_CPSR_I_MASK |
                              ARM7_CPSR_F_MASK);
        arm7->reg[ARM7_REG_SPSR_FIQ] = cpsr;
        *arm7_gen_reg(arm7, 14) = arm7_pc_next(arm7) + 4;
        arm7->reg[ARM7_REG_PC] = 0x1c;
        arm7_reset_pipeline(arm7);
        arm7->excp &= ~ARM7_EXCP_FIQ;
    } else if ((excp & ARM7_EXCP_IRQ) && !(cpsr & ARM7_CPSR_I_MASK)) {
        LOG_DBG("IRQ jump to 0x18\n");
        arm7_cpsr_mode_change(arm7, (cpsr & ~(ARM7_CPSR_M_MASK | ARM7_CPSR_T_MASK)) |
                              ARM7_MODE_IRQ | ARM7_CPSR_I_MASK |
                              ARM7_CPSR_F_MASK);
        arm7->reg[ARM7_REG_SPSR_IRQ] = cpsr;
        *arm7_gen_reg(arm7, 14) = arm7_pc_next(arm7) + 4;
        arm7->reg[ARM7_REG_PC] = 0x18;
        arm7_reset_pipeline(arm7);
        arm7->excp &= ~ARM7_EXCP_IRQ;
    } else if (excp & ARM7_EXCP_SWI) {
        /*
         * This will be called *after* the SWI instruction has executed, when
         * the arm7 is about to execute the next instruction.  The spec says
         * that R14_svc needs to point to the instruction immediately after the
         * SWI.  I expect the SWI instruction to not increment the PC at the
         * end, so the instruction after the SWI will be pipeline[1].
         * ARM7_REG_R15 points to the next instruction to be fetched, which is
         * pipeline[0].  Therefore, the next instruction to be executed is at
         * ARM7_REG_R15 - 4.
         */
        arm7_cpsr_mode_change(arm7, (cpsr & ~(ARM7_CPSR_M_MASK | ARM7_CPSR_T_MASK)) |
                              ARM7_MODE_SVC | ARM7_CPSR_I_MASK |
                              ARM7_CPSR_F_MASK);
        arm7->reg[ARM7_REG_SPSR_SVC] = cpsr;
        *arm7_gen_reg(arm7, 14) = arm7_pc_next(arm7);
        arm7->reg[ARM7_REG_PC] = 8;
        arm7_reset_pipeline(arm7);
        arm7->excp &= ~ARM7_EXCP_SWI;
    }
}
