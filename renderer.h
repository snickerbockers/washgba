/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef RENDERER_H_
#define RENDERER_H_

#include "SDL_opengl.h"

#include "gba.h"

struct renderer {
    GLuint vs, fs, prog;

    GLuint vert_pos_slot, vert_tex_coord_slot,
        bound_tex_slot, trans_mat_slot, tex_mat_slot;

    GLuint quad_buf, quad_array, quad_elems;
};

void renderer_init(struct renderer *rend);
void renderer_cleanup(struct renderer *rend);

void renderer_draw_frame(struct renderer *rend,
                         struct gba *gba, GLfloat const proj[16]);

#endif
