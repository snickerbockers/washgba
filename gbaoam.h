/*******************************************************************************
 *
 *
 *    washGBA Game Boy Advance Emulator
 *    Copyright (C) 2021 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef GBAOAM_H_
#define GBAOAM_H_

#include <stdint.h>

#include "memory_map.h"

#define GBAOAM_FIRST 0x07000000
#define GBAOAM_LAST  0x070003ff

#define GBAOAM_LEN ((GBAOAM_LAST - GBAOAM_FIRST + 1) / 2)

#define GBAOAM_OBJ_COUNT 128

struct gbaoam {
    uint16_t mem[GBAOAM_LEN];
};

void gbaoam_init(struct gbaoam *oam);
void gbaoam_cleanup(struct gbaoam *oam);

// read through the OAM and dump everything to LOG_DBG for debugging purposes
void gbaoam_parse(struct gbaoam *oam);

extern struct memory_interface const gbaoam_memory_interface;

#endif
